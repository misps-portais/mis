
<head>
	<meta charset='utf-8'>
	<meta http-equiv='X-UA-Compatible' content='IE=Edge,chrome=1'>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>	
	<script type="text/javascript">
	
		google.load('visualization', '1', {'packages':['corechart']});
		google.load('visualization', '1', {packages: ['table']});

		//google.setOnLoadCallback(drawChart);

		function drawChart(visao) {
			var jsonData = $.ajax({
			  url: "demandas_graficos.php?visao="+visao,
			  dataType:"json",
			  async: false
			  }).responseText;
			  
			// Create our data table out of JSON data loaded from server.
			var data = new google.visualization.DataTable(jsonData);

			document.getElementById('div_seletor_mes').style.visibility = 'hidden';
			document.getElementById('div_exportar_mes').style.visibility = 'hidden';
			document.getElementById('div_exportar_dia').style.visibility = 'hidden';

			// Instantiate and draw our chart, passing in some options.
			switch (visao) {
				case 'evolucao_geral':
					var chart = new google.visualization.LineChart(document.getElementById('div_grafico'));
					chart.draw(data, {
										width: 900, 
										height: 500,
										lineWidth: 5
									});
					break;
				case 'evolucao_produto':
				case 'evolucao_solicitante':
				case 'evolucao_tipo_demandas':			
				case 'evolucao_solicitacao_corretor':
				case 'evolucao_solicitacao_segurado':
				case '2via_boleto':
				case '2via_boleto_corretor':					
					var chart = new google.visualization.LineChart(document.getElementById('div_grafico'));
					chart.draw(data, {
										width: 900, 
										height: 500,
									});	
					break;
				case 'participacao_solicitante':
					var chart = new google.visualization.PieChart(document.getElementById('div_grafico'));
					chart.draw(data, {
										width: 900, 
										height: 500,
										is3D: true,
									});	
					break;
				case 'tabela_mes':
					var chart = new google.visualization.Table(document.getElementById('div_grafico'));
					chart.draw(data, {
										width: 900, 
										height: 500,
									});	
					document.getElementById('div_exportar_mes').style.visibility = 'initial'
					break;
			}
		}
		function seletor_mes(id) {
			document.getElementById('div_seletor_mes').style.visibility = 'initial';
			document.getElementById('div_exportar_mes').style.visibility = 'hidden';
			document.getElementById('div_grafico').innerHTML = '';
		}
		function tabela_dia (data) {
			visao = 'tabela_dia';
			var jsonData = $.ajax({
			  url: "demandas_graficos.php?visao="+visao+"&&data="+data,
			  dataType:"json",
			  async: false
			  }).responseText;
			  
			document.getElementById('div_exportar_dia').style.visibility = 'initial';
			// Create our data table out of JSON data loaded from server.
			var data = new google.visualization.DataTable(jsonData);			
			var chart = new google.visualization.Table(document.getElementById('div_grafico'));
			chart.draw(data, {
								width: 900, 
								height: 500,
							});
		}
		function exportar(visao) {
			if (visao='exportar_tabela_mes') {
				url = "demandas_graficos.php?visao="+visao;
				window.open(url,'_blank');
			}
			else {
				data = document.getElementById('seletor_mes').value
				url = "demandas_graficos.php?visao="+visao+"&&data="+data;
				window.open(url,'_blank');				
			}
		}
	</script>
	<style type="text/css">
		.menu_graficos {
			background-color: rgb(79,129,189);
			color: white;
			width: 100px;
			height: 65px;
			float: left;
			margin: 3px;
			cursor: pointer;
		}
		.menu_graficos:hover {
			background: rgb(183, 222, 232);
		}
		.menu_graficos p {
			margin: 5px;
			text-align: center;
		}
		.grafico {
			position: inherit;
			width: 900px;
			height: 500px;
			clear: both;	
		}
	</style>
</head>

<body>
		<?php
			$menu = array(
							'evolucao_geral' => array('nomenclatura' => 'Evolução Geral', 'javascript' => "onclick='drawChart(this.id)'"), 
							'evolucao_produto' => array('nomenclatura' => 'Evolução por Produto', 'javascript' => "onclick='drawChart(this.id)'"), 
							'participacao_solicitante' => array('nomenclatura' => 'Participação Solicitante', 'javascript' => "onclick='drawChart(this.id)'"),
							'evolucao_solicitante' => array('nomenclatura' => 'Evolução Solicitante', 'javascript' => "onclick='drawChart(this.id)'"),
							'evolucao_tipo_demandas' => array('nomenclatura' => 'Evolução Tipo Demandas', 'javascript' => "onclick='drawChart(this.id)'"),
							'evolucao_solicitacao_corretor' => array('nomenclatura' => 'Evolução Solicitação Corretor', 'javascript' => "onclick='drawChart(this.id)'"),
							'evolucao_solicitacao_segurado' => array('nomenclatura' => 'Evolução Solicitação Segurado', 'javascript' => "onclick='drawChart(this.id)'"),
							'2via_boleto' => array('nomenclatura' => '2ª Via Boleto', 'javascript' => "onclick='drawChart(this.id)'"),
							'2via_boleto_corretor' => array('nomenclatura' => '2ª Via Boleto Corretor', 'javascript' => "onclick='drawChart(this.id)'"),
							'tabela_mes' => array('nomenclatura' => 'Evolução Mensal', 'javascript' => "onclick='drawChart(this.id)'"),
							'tabela_dia' => array('nomenclatura' => 'Evolução Diária', 'javascript' => "onclick='seletor_mes(this.id)'"),
							);
			echo "<div style='float:left;margin-bottom:10px'>";
			foreach ($menu as $chave => $valor) {
				echo "<div class='menu_graficos' id='".$chave."' ".$valor['javascript'].">
						<p>".$valor['nomenclatura']."</p>
					</div>";
				# code...
			}
			echo "</div>";
		?>
	<br>
	<div class='grafico'>
		<div id='div_grafico_controle'>
			<div id='div_seletor_mes' style="visibility: hidden;">
				<label for='seletor_mes'>Selecione o mês que deseja visualizar: </label><input id='seletor_mes' type='month' onChange='tabela_dia(this.value)'>
			</div>
			<div id='div_exportar_mes' style="visibility: hidden;">
				<input type='button' id='exportar_tabela_mes' value='exportar' onClick='exportar(this.id)'>
			</div>
			<div id='div_exportar_dia' style="visibility: hidden;">
				<input type='button' id='exportar_tabela_dia' value='exportar' onClick='exportar(this.id)'>
			</div>			
		</div>
		<div id='div_grafico'>
		</div>
	</div>
</body>



<?php

?>
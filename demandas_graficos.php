<?php
	$visao = $_REQUEST["visao"];

	/**
	* 
	*/
	class banco {
		function grafico_dados($strsql) {
			$tipo = array(
							'int4' => 'number',
							'text' => 'string',
							'varchar' => 'string',
							'int8' => 'number',
							'numeric' => 'number'
				);

			$rs=odbc_exec($this->conexao(),$strsql);

			//odbc_result_all($rs);
			for ($i=1; $i <= odbc_num_fields($rs) ; $i++) {
				$cols[] = array(
								'id' => '', 
								'label' => utf8_encode(odbc_field_name($rs, $i)),
								'type' => $tipo[odbc_field_type($rs, $i)]
								);
			}

			while(odbc_fetch_row($rs)) {
				$linha = array();
				for ($i=1; $i <= odbc_num_fields($rs) ; $i++) {
					$linha[] = array(
									'v' => utf8_encode(odbc_result($rs, $i)), 
									'f' => null,
									);
				}
				$rows[] = array(
								'c' => $linha
									);
			}
			$dados = array(
							'cols' => $cols,
							'rows' => $rows
				);

			$json = json_encode($dados, JSON_NUMERIC_CHECK );

			$json = strtr($json, "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ", "aaaaeeiooouucAAAAEEIOOOUUC");

			echo $json;
		}
		function exportar_dados($strsql) {
			$rs=odbc_exec($this->conexao(),$strsql);
			exportarxls(odbc_result_all($rs));
		}
		function conexao() {
			return odbc_connect('MISPG','','');
		}
		function colunas($strsql) {
			$rs=odbc_exec($this->conexao(),$strsql);
			$linha = array();
			while(odbc_fetch_row($rs)) {
				$linha[] = odbc_result($rs, 1);
			}
			return $linha;
		}
	}

	$banco = new banco();

	switch ($visao) {
		case 'evolucao_geral':
			$colunas = "select bo.grupo
						from extracao_bo.bo_cobranca_diario bo
						group by bo.grupo
						order by 1";
			
			$strsql ="SELECT 
						date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento) as data, ";
			foreach ($banco->colunas($colunas) as $chave => $valor) {
				$strsql = $strsql.chr(10)." SUM (CASE WHEN bo.grupo = '".$valor."' THEN bo.qtd ELSE 0 END) as ".chr(34).$valor.chr(34).",";

			}
				$strsql = substr($strsql,0,-1);
				$strsql = $strsql." FROM extracao_bo.bo_cobranca_diario as bo 
				GROUP BY 
					date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento)
				ORDER BY 1";
				$banco->grafico_dados($strsql);
				//echo utf8_encode($strsql);
			break;
		case 'evolucao_produto':
			$colunas = "select bo.produto
						from extracao_bo.bo_cobranca_diario bo
						group by bo.produto
						order by 1";
			
			$strsql ="SELECT 
						date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento) as data, ";
			foreach ($banco->colunas($colunas) as $chave => $valor) {
				$strsql = $strsql.chr(10)." SUM (CASE WHEN bo.produto = '".$valor."' THEN bo.qtd ELSE 0 END) as ".chr(34).$valor.chr(34).",";

			}
				$strsql = substr($strsql,0,-1);
				$strsql = $strsql." FROM extracao_bo.bo_cobranca_diario as bo 
									GROUP BY 
										date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento)
									ORDER BY 1";
				$banco->grafico_dados($strsql);
				//echo utf8_encode($strsql);

			break;
		case 'tabela_mes':
			$colunas = "select date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento)
						from extracao_bo.bo_cobranca_diario bo
						group by date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento)
						order by 1";		

			$strsql = "SELECT 
						bo.produto,
						bo.ramo,";
			foreach ($banco->colunas($colunas) as $chave => $valor) {
				$strsql = $strsql.chr(10)." SUM (CASE WHEN date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento) = '".$valor."' THEN bo.qtd ELSE 0 END) as ".chr(34).$valor.chr(34).",";

			}
			$strsql = $strsql." sum(bo.qtd) as Total";

			$strsql = $strsql."	from extracao_bo.bo_cobranca_diario bo
						group by bo.produto, bo.ramo
						order by 1, 2";
			//echo(utf8_encode($strsql));
			$strsql = utf8_decode($strsql);
			$banco->grafico_dados($strsql);
			break;
		case 'exportar_tabela_mes':
			$colunas = "select date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento)
						from extracao_bo.bo_cobranca_diario bo
						group by date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento)
						order by 1";		

			$strsql = "SELECT 
						bo.produto,
						bo.ramo,";
			foreach ($banco->colunas($colunas) as $chave => $valor) {
				$strsql = $strsql.chr(10)." SUM (CASE WHEN date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento) = '".$valor."' THEN bo.qtd ELSE 0 END) as ".chr(34).$valor.chr(34).",";

			}
			$strsql = $strsql." sum(bo.qtd) as Total";

			$strsql = $strsql."	from extracao_bo.bo_cobranca_diario bo
						group by bo.produto, bo.ramo
						order by 1, 2";
			//echo(utf8_encode($strsql));
			$banco->exportar_dados($strsql);
			break;
		case 'tabela_dia':
			$data = $_REQUEST["data"];
			$colunas = "SELECT date(bo.data_atendimento)
						FROM extracao_bo.bo_cobranca_diario bo
						WHERE date_part('year', data_atendimento)||'-'||to_char(data_atendimento,'MM')='$data'
						GROUP BY date(bo.data_atendimento)
						ORDER BY 1 ASC";

			$strsql = "SELECT 
						bo.produto,
						bo.ramo,";
			foreach ($banco->colunas($colunas) as $chave => $valor) {
				$strsql = $strsql.chr(10)." SUM (CASE WHEN data_atendimento = '".$valor."' THEN bo.qtd ELSE 0 END) as ".chr(34).$valor.chr(34).",";
			}
			$strsql = $strsql." sum(bo.qtd) as Total";

			$strsql = $strsql."	from extracao_bo.bo_cobranca_diario bo
						WHERE date_part('year', data_atendimento)||'-'||to_char(data_atendimento,'MM')='$data'
						group by bo.produto, bo.ramo
						order by 1, 2";
			//echo(utf8_encode($strsql));
			$banco->grafico_dados($strsql);
			break;
		case 'exportar_tabela_dia':
			$data = $_REQUEST["data"];
			$colunas = "SELECT date(bo.data_atendimento)
						FROM extracao_bo.bo_cobranca_diario bo
						WHERE date_part('year', data_atendimento)||'-'||to_char(data_atendimento,'MM')='$data'
						GROUP BY date(bo.data_atendimento)
						ORDER BY 1 ASC";

			$strsql = "SELECT 
						bo.produto,
						bo.ramo,";
			foreach ($banco->colunas($colunas) as $chave => $valor) {
				$strsql = $strsql.chr(10)." SUM (CASE WHEN data_atendimento = '".$valor."' THEN bo.qtd ELSE 0 END) as ".chr(34).$valor.chr(34).",";
			}
			$strsql = $strsql." sum(bo.qtd) as Total";

			$strsql = $strsql."	from extracao_bo.bo_cobranca_diario bo
						WHERE date_part('year', data_atendimento)||'-'||to_char(data_atendimento,'MM')='$data'
						group by bo.produto, bo.ramo
						order by 1, 2";
			//echo(utf8_encode($strsql));
			$banco->exportar_dados($strsql);
			break;
		case 'participacao_solicitante':
			$strsql = "SELECT 
							CASE WHEN ts.tipo_cliente_descricao ilike 'funcion%'
								THEN 
									CASE 
										WHEN bo.departamento ilike '%suc%' THEN 'Sucursal/Regional'
										WHEN bo.departamento ilike '%reg%' THEN 'Sucursal/Regional'
										WHEN bo.departamento ilike '%CLEO NEG%' THEN 'Nucleo de Negocios'
										WHEN bo.departamento ilike '%EMISS%' THEN 'Emissao'
										WHEN bo.departamento ilike '%CART%' THEN 'Cartao'
										ELSE 'Outras Areas'
									END 				
								ELSE
									ts.tipo_cliente_descricao
							END AS tipo_cliente,
							sum(bo.qtd) as qtd
						FROM extracao_bo.bo_cobranca_diario as bo
						INNER JOIN extracao_bo.tbl_tiposolicitante as ts
						ON bo.tipo_cliente = ts.tipo_cliente
						WHERE bo.grupo ilike '%Cobran%'
						GROUP BY 
							CASE WHEN ts.tipo_cliente_descricao ilike 'funcion%'
								THEN 
									CASE 
										WHEN bo.departamento ilike '%suc%' THEN 'Sucursal/Regional'
										WHEN bo.departamento ilike '%reg%' THEN 'Sucursal/Regional'
										WHEN bo.departamento ilike '%CLEO NEG%' THEN 'Nucleo de Negocios'
										WHEN bo.departamento ilike '%EMISS%' THEN 'Emissao'
										WHEN bo.departamento ilike '%CART%' THEN 'Cartao'				
										ELSE 'Outras Areas'
									END 				
								ELSE
									ts.tipo_cliente_descricao
							END
						ORDER BY 2 desc";
			//echo(utf8_encode($strsql));					
			$banco->grafico_dados($strsql);
			break;
		case 'evolucao_solicitante':
			$colunas = "SELECT 
						CASE WHEN ts.tipo_cliente_descricao ilike 'funcion%'
							THEN 
								CASE 
									WHEN bo.departamento ilike '%suc%' THEN 'Sucursal/Regional'
									WHEN bo.departamento ilike '%reg%' THEN 'Sucursal/Regional'
									WHEN bo.departamento ilike '%CLEO NEG%' THEN 'Nucleo de Negocios'
									WHEN bo.departamento ilike '%EMISS%' THEN 'Emissao'
									WHEN bo.departamento ilike '%CART%' THEN 'Cartao'
									ELSE 'Outras Areas'
								END 				
							ELSE
								ts.tipo_cliente_descricao
						END AS tipo_cliente
					FROM extracao_bo.bo_cobranca_diario as bo
					INNER JOIN extracao_bo.tbl_tiposolicitante as ts
					ON bo.tipo_cliente = ts.tipo_cliente
					WHERE bo.grupo ilike '%Cobran%'
					GROUP BY 
						CASE WHEN ts.tipo_cliente_descricao ilike 'funcion%'
							THEN 
								CASE 
									WHEN bo.departamento ilike '%suc%' THEN 'Sucursal/Regional'
									WHEN bo.departamento ilike '%reg%' THEN 'Sucursal/Regional'
									WHEN bo.departamento ilike '%CLEO NEG%' THEN 'Nucleo de Negocios'
									WHEN bo.departamento ilike '%EMISS%' THEN 'Emissao'
									WHEN bo.departamento ilike '%CART%' THEN 'Cartao'				
									ELSE 'Outras Areas'
								END 				
							ELSE
								ts.tipo_cliente_descricao
						END
					ORDER BY 1";

			$strsql ="SELECT 
						date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento) as data, ";
			foreach ($banco->colunas($colunas) as $chave => $valor) {
				$strsql = $strsql.chr(10)." SUM (CASE WHEN qr.tipo_cliente = '".$valor."' THEN qr.qtd ELSE 0 END) as ".chr(34).$valor.chr(34).",";

			}
				$strsql = substr($strsql,0,-1);
				$strsql = $strsql." FROM (SELECT
							bo.data_atendimento as data_atendimento,
							CASE WHEN ts.tipo_cliente_descricao ilike 'funcion%'
								THEN 
									CASE 
										WHEN bo.departamento ilike '%suc%' THEN 'Sucursal/Regional'
										WHEN bo.departamento ilike '%reg%' THEN 'Sucursal/Regional'
										WHEN bo.departamento ilike '%CLEO NEG%' THEN 'Nucleo de Negocios'
										WHEN bo.departamento ilike '%EMISS%' THEN 'Emissao'
										WHEN bo.departamento ilike '%CART%' THEN 'Cartao'
										ELSE 'Outras Areas'
									END 				
								ELSE
									ts.tipo_cliente_descricao
							END AS tipo_cliente,
							sum(bo.qtd) as qtd
						FROM extracao_bo.bo_cobranca_diario as bo
						INNER JOIN extracao_bo.tbl_tiposolicitante as ts
						ON bo.tipo_cliente = ts.tipo_cliente
						WHERE bo.grupo ilike '%Cobran%'
						GROUP BY
							bo.data_atendimento,
							CASE WHEN ts.tipo_cliente_descricao ilike 'funcion%'
								THEN 
									CASE
										WHEN bo.departamento ilike '%suc%' THEN 'Sucursal/Regional'
										WHEN bo.departamento ilike '%reg%' THEN 'Sucursal/Regional'
										WHEN bo.departamento ilike '%CLEO NEG%' THEN 'Nucleo de Negocios'
										WHEN bo.departamento ilike '%EMISS%' THEN 'Emissao'
										WHEN bo.departamento ilike '%CART%' THEN 'Cartao'
										ELSE 'Outras Areas'
									END
								ELSE
									ts.tipo_cliente_descricao
							END) as qr
					GROUP BY
						date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento)
					ORDER BY 1";
				//echo utf8_encode($strsql);
				$banco->grafico_dados($strsql);
			break;
		case 'evolucao_tipo_demandas':
			$colunas = "select 
							CASE WHEN bo.assunto IS NULL THEN 'Nao Informado'
								ELSE bo.assunto
							END as assunto,
							sum(bo.qtd)
						from extracao_bo.bo_cobranca_diario bo
						WHERE bo.grupo ilike '%Cobran%' AND bo.assunto IS NOT NULL
						group by bo.assunto
						order by 2 desc
						limit 10";
			
			$strsql ="SELECT 
						date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento) as data, ";
			foreach ($banco->colunas($colunas) as $chave => $valor) {
				if ($valor=='Nao Informado') {
					$strsql = $strsql.chr(10)." SUM (CASE WHEN bo.assunto is null THEN bo.qtd ELSE 0 END) as ".chr(34)."Nao Informado".chr(34).",";
				}
				else {
					$strsql = $strsql.chr(10)." SUM (CASE WHEN bo.assunto = '".$valor."' THEN bo.qtd ELSE 0 END) as ".chr(34).$valor.chr(34).",";
				}
			}
				$strsql = substr($strsql,0,-1);
				$strsql = $strsql." FROM extracao_bo.bo_cobranca_diario as bo 
									WHERE bo.grupo ilike '%Cobran%'
									GROUP BY 
										date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento)
									ORDER BY 1";
				//echo utf8_encode($strsql);
				$banco->grafico_dados($strsql);

			break;
		case 'evolucao_solicitacao_corretor':
			$colunas = "select 
							CASE WHEN bo.assunto IS NULL THEN 'Nao Informado'
								ELSE bo.assunto
							END as assunto,
						sum(bo.qtd)
						from extracao_bo.bo_cobranca_diario bo
						WHERE bo.grupo ilike '%Cobran%' and bo.tipo_cliente = 'Corretor'
						group by bo.assunto
						order by 2 desc
						limit 10";
			
			$strsql ="SELECT 
						date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento) as data, ";
			foreach ($banco->colunas($colunas) as $chave => $valor) {
				if ($valor=='Nao Informado') {
					$strsql = $strsql.chr(10)." SUM (CASE WHEN bo.assunto is null THEN bo.qtd ELSE 0 END) as ".chr(34)."Nao Informado".chr(34).",";
				}
				else {
					$strsql = $strsql.chr(10)." SUM (CASE WHEN bo.assunto = '".$valor."' THEN bo.qtd ELSE 0 END) as ".chr(34).$valor.chr(34).",";
				}
			}
				$strsql = substr($strsql,0,-1);
				$strsql = $strsql." FROM extracao_bo.bo_cobranca_diario as bo 
									WHERE bo.grupo ilike '%Cobran%' and bo.tipo_cliente = 'Corretor'
									GROUP BY 
										date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento)
									ORDER BY 1";
				$banco->grafico_dados($strsql);
				//echo utf8_encode($strsql);
			break;
		case 'evolucao_solicitacao_segurado':
			$colunas = "select 
							CASE WHEN bo.assunto IS NULL THEN 'Nao Informado'
								ELSE bo.assunto
							END as assunto,
						sum(bo.qtd)
						from extracao_bo.bo_cobranca_diario bo
						INNER JOIN extracao_bo.tbl_tiposolicitante as ts
						ON bo.tipo_cliente = ts.tipo_cliente
						WHERE bo.grupo ilike '%Cobran%' and ts.tipo_cliente_descricao = 'Cliente'
						group by bo.assunto
						order by 2 desc
						limit 10";
			
			$strsql ="SELECT 
						date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento) as data, ";
			foreach ($banco->colunas($colunas) as $chave => $valor) {
				if ($valor=='Nao Informado') {
					$strsql = $strsql.chr(10)." SUM (CASE WHEN bo.assunto is null THEN bo.qtd ELSE 0 END) as ".chr(34)."Nao Informado".chr(34).",";
				}
				else {
					$strsql = $strsql.chr(10)." SUM (CASE WHEN bo.assunto = '".$valor."' THEN bo.qtd ELSE 0 END) as ".chr(34).$valor.chr(34).",";
				}
			}
				$strsql = substr($strsql,0,-1);
				$strsql = $strsql." FROM extracao_bo.bo_cobranca_diario as bo 
									INNER JOIN extracao_bo.tbl_tiposolicitante as ts
									ON bo.tipo_cliente = ts.tipo_cliente										
									WHERE bo.grupo ilike '%Cobran%' and ts.tipo_cliente_descricao = 'Cliente'
									GROUP BY 
										date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento)
									ORDER BY 1";
				$banco->grafico_dados($strsql);
				//echo utf8_encode($strsql);
			break;
		case '2via_boleto':
			$colunas = "select bo.produto
						from extracao_bo.bo_cobranca_diario bo
						WHERE bo.grupo ilike '%Cobran%' and bo.assunto ilike '%via boleto%'
						group by bo.produto
						order by 1";
			
			$strsql ="SELECT 
						date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento) as data, ";
			foreach ($banco->colunas($colunas) as $chave => $valor) {
				$strsql = $strsql.chr(10)." SUM (CASE WHEN bo.produto = '".$valor."' THEN bo.qtd ELSE 0 END) as ".chr(34).$valor.chr(34).",";

			}
				$strsql = substr($strsql,0,-1);
				$strsql = $strsql." FROM extracao_bo.bo_cobranca_diario as bo 
									WHERE bo.grupo ilike '%Cobran%' and bo.assunto ilike '%via boleto%'
									GROUP BY 
										date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento)
									ORDER BY 1";
				$banco->grafico_dados($strsql);
				//echo utf8_encode($strsql);
			break;
		case '2via_boleto_corretor':
			$colunas = "select bo.produto
						from extracao_bo.bo_cobranca_diario bo
						WHERE bo.grupo ilike '%Cobran%' and bo.assunto ilike '%via boleto%'
						group by bo.produto
						order by 1";
			
			$strsql ="SELECT 
						date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento) as data, ";
			foreach ($banco->colunas($colunas) as $chave => $valor) {
				$strsql = $strsql.chr(10)." SUM (CASE WHEN bo.produto = '".$valor."' THEN bo.qtd ELSE 0 END) as ".chr(34).$valor.chr(34).",";

			}
				$strsql = substr($strsql,0,-1);
				$strsql = $strsql." FROM extracao_bo.bo_cobranca_diario as bo 
									INNER JOIN extracao_bo.tbl_tiposolicitante as ts
									ON bo.tipo_cliente = ts.tipo_cliente				
									WHERE bo.grupo ilike '%Cobran%' and bo.assunto ilike '%via boleto%' and ts.tipo_cliente_descricao = 'Corretor'
									GROUP BY 
										date_part('year', data_atendimento)||'-'||date_part('month', data_atendimento)
									ORDER BY 1";
				$banco->grafico_dados($strsql);
				//echo utf8_encode($strsql);
			break;

	}

	function exportarxls($tabela) {
		  $nome_arquivo = "CRM - Cobrança";
          header("Content-type: application/vnd.ms-excel");
          header("Content-type: application/force-download");
          header("Content-Disposition: attachment; filename=$nome_arquivo.xls");
          header("Pragma: no-cache");
          echo $tabela;
	}
?>
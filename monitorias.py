# -*- coding: cp1252 -*-
import pyodbc
from datetime import datetime, date
import string
import getpass
import sys

#lista dos bancos a serem acessados 
def lstbancos():
    # Este vetor tem validade at� 31/10/2013
    #bancos = ('n_azu','n_car')
    #bancos = ('n_azu','n_car','n_cob','n_ctt','n_gda','n_hdc','n_ita','n_orc','n_sau','n_ses','n_tmk','n_uni','n_vep','n_con')
    bancos = ('n_ccl', 'n_cre', 'n_ana', 'n_cco','n_pse', 'n_inf', 'n_csa', 'n_kob','n_com','n_sac', 'n_cda','n_cch','n_sin','n_esp','n_vid','n_caz','n_spa','n_spo','n_vep','n_hdk','n_hco','n_nap','n_n_1', 'n_n_2','n_n_3','n_n_4','n_n_5','n_n_6','n_n_7','n_n_8','n_ren','n_itt','n_itc','n_rau','n_sfu','n_per','n_med','n_pre','n_bac','n_gre','n_gau','n_cap','n_azu','n_ita','n_apo','n_con')
    return bancos

#captura �ltima de atualiza��o do banco local
def dtatualizacao(fonte, cnpg, tabela):
    strsql = "SELECT max(date(data_insert)) FROM " + tabela
    strsql = strsql + " WHERE fonte = '" + fonte +"';"
    cursorpg = cnpg.cursor()
    cursorpg.execute(strsql)
    rs = cursorpg.fetchall()

    dt = rs[0][0]

    if dt == None:
        dt = date(2013,1,1)
    return dt

def limpamatricula(mat):
    permitidos = string.digits
    mat =  ''.join(char if char in permitidos else '' for char in mat)
    return mat

def verifica_matricula(mat):
    try:
        if mat.isdigit() == False:
            mat = limpamatricula(mat)
        if mat[0:1] =='1':
            mat = '0' + mat
        if len(mat)==7:
            mat = digito(mat)
        return int(mat)
    except:
        return mat

def digito(mat):
    soma = 0
    cont = len(mat)+1
    for i in range(0,len(mat)):
        soma = soma + (int(mat[i:i+1]) * cont)
        cont = cont - 1
    dv = 11 - (soma % 11)
    if dv == 10 or dv == 11:
        dv = 0
    mat = mat + str(dv)
    return mat


#importa dados de fonte externa para banco local
def importar(fonte, cnpg, tabela):

    #estabele conex�o com fonte externa
    strconect = "DRIVER={MySQL ODBC 5.2a Driver};SERVER=192.167.227.59;DATABASE=" + fonte + ";UID=mis;PWD=mis*2013"
    cnms = pyodbc.connect(strconect)
    print 'banco conectado'

    strsql = "SELECT matricula, STR_TO_DATE(datainicio,'%d/%m/%Y') as data, count(*) as contagem, avg(nota)"
    strsql = strsql + " FROM monitoria_2013"
    strsql = strsql + " GROUP BY matricula, datainicio"
    cursorms = cnms.cursor()
    cursorms.execute(strsql)

    strinsert =''
    for row in cursorms:
        if len(row[0])>=6:
            strvalores = ' ('
            mat = str(row[0])
            mat = verifica_matricula(mat)
            strvalores = strvalores + "'" + str(mat) + "', "
            strvalores = strvalores + row[1].strftime("'%Y-%m-%d'") + ', '
            strvalores = strvalores + str(row[2]) + ', '
            strvalores = strvalores + str(row[3]) + ', '

            strvalores = strvalores + "'" + fonte + "'), "
            strinsert = strinsert + strvalores

    strinsert = strinsert[0:len(strinsert)-2]
    if not strinsert == '': 
        strsql = "INSERT INTO qualidade.tbl_qualidade_temp (matricula, data, qtd, nota, fonte) VALUES " + strinsert
        #strsql = strsql + " VALUES (" + strinsert + ")"
        cnpg.execute(strsql)
        cnpg.commit()

    strsql = "UPDATE qualidade.tbl_qualidade_temp SET (data_insert) = (current_timestamp) WHERE data_insert is null"
    cnpg.execute(strsql)
    cnpg.commit()

    cnms.close()
def atualizar_base():
    TempoIni = datetime.now()
    #update em registros j� existentes
    strsql = "UPDATE qualidade.tbl_qualidade_2013 AS tq"
    strsql = strsql + " SET "
    strsql = strsql + "     (qtd, nota, data_insert)"
    strsql = strsql + "     ="
    strsql = strsql + "     (qr1.qtd, qr1.nota, qr1.data_insert)"
    strsql = strsql + "     FROM (    SELECT *"
    strsql = strsql + "         FROM qualidade.tbl_qualidade_temp"
    strsql = strsql + "         ) qr1"
    strsql = strsql + "     WHERE tq.matricula = qr1.matricula AND tq.data = qr1.data AND tq.fonte = qr1.fonte"
    strsql = strsql + "     RETURNING *"

    cursorpg.execute(strsql)
    cursorpg.commit()
    TempoFim = datetime.now()
    registros = cursorpg.rowcount

    TempoIni = datetime.now()
    #Insere novos registros
    strsql = "INSERT INTO qualidade.tbl_qualidade_2013 (matricula, data, qtd, nota, fonte, data_insert)"
    strsql = strsql + " SELECT             "
    strsql = strsql + "     tqt.matricula,"
    strsql = strsql + "     tqt.data,"
    strsql = strsql + "     tqt.qtd,"
    strsql = strsql + "     tqt.nota,"
    strsql = strsql + "     tqt.fonte,"
    strsql = strsql + "     tqt.data_insert"
    strsql = strsql + " FROM tbl_qualidade_temp AS tqt"
    strsql = strsql + " WHERE NOT EXISTS "
    strsql = strsql + "     (SELECT tq.matricula,"
    strsql = strsql + "         tq.data"
    strsql = strsql + "     FROM qualidade.tbl_qualidade_2013 as tq"
    strsql = strsql + "     WHERE tq.matricula = tqt.matricula AND tq.data = tqt.data)"
    strsql = strsql + " RETURNING *"

    cursorpg.execute(strsql)
    cnpg.commit()

    TempoFim = datetime.now()
    registros = cursorpg.rowcount

    strsql = "VACUUM FULL qualidade.tbl_qualidade_2013"
    cnpg.execute(strsql)
    cnpg.commit()

#cnms = pyodbc.connect('DRIVER={MySQL ODBC 5.2a Driver};SERVER=192.167.227.59;DATABASE=n_hdc;UID=smis;PWD=pcmis')
#cursor = cn.cursor()

#conex�o com banco
cnpg = pyodbc.connect('DRIVER={PostgreSQL ANSI};SERVER=172.23.14.155;DATABASE=MIS;UID=postgres;PWD=mis*2013')
cursorpg = cnpg.cursor()
#tabela que recebera os dados
tabela = "qualidade.tbl_qualidade_2013"

#criar tabela tempor�ria
strsql = "CREATE TABLE IF NOT EXISTS qualidade.tbl_qualidade_temp"
strsql = strsql + " ("
strsql = strsql + "   id serial NOT NULL,"
strsql = strsql + "   matricula character varying(10),"
strsql = strsql + "   data date,"
strsql = strsql + "   qtd integer,"
strsql = strsql + "   nota numeric,"
strsql = strsql + "   fonte character varying(10),"
strsql = strsql + "   data_insert timestamp without time zone,"
strsql = strsql + "   PRIMARY KEY (id)"
strsql = strsql + " )"
cnpg.execute(strsql)

bancos = lstbancos()
for i in range(0, len(bancos)):
    print bancos[i]
    importar(bancos[i], cnpg, tabela) #importa registros para tabela tempor�ria

atualizar_base() #Atualiza base de dados permanente

#cursorpg.execute('select ppr_2013.proc_atualiza_qualidade()')
#cursorpg.commit()
#cursorpg.execute('select ppr_2013.proc_atualiza_qualidade_telemark()')
#cursorpg.commit()


cnpg.close()

print 'Finalizado'

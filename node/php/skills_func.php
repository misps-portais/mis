<?php

  function inicializaCampos($algoritmo,$id,$label,$tabela){

    $conn=odbc_connect('MISPG','','');

    $validaSkill = '';
    if($id == 'skill' || $id == 'dac' || $id == 'servidor'){
      $validaSkill = "onchange = 'validaSkill()'";
    }

    $strsql = "SELECT *
          FROM skills.$tabela ORDER BY 2";
    
    switch ($algoritmo) {
      case 1:
        $rs=odbc_exec($conn,$strsql);
        echo "<select name='$id' id='$id' $validaSkill class='liseletor' required>";
        echo "<option value=''>$label</option>";
        while (odbc_fetch_row($rs)) {
          echo utf8_encode("<option value=".odbc_result($rs,1).">".odbc_result($rs,2)." - ".odbc_result($rs,3)."</option>");
        }
        echo "</select>";
        echo "<input type='text' id='novo_$id' disabled required name='novo_$id' class='liseletor' style='display:none;margin-top:5px;margin-bottom:5px;'>";
      break;
      case 2:
        $rs=odbc_exec($conn,$strsql);
        echo "<select name='$id' id='$id' $validaSkill class='liseletor' required>";
        echo "<option value=''>$label</option>";
        while (odbc_fetch_row($rs)) {
          echo utf8_encode("<option value=".odbc_result($rs,1).">".odbc_result($rs,2)."</option>");
        }
        echo "<option value=-1>Novo</option>";
        echo "</select><img name='off' id='img_$id' style='width:20px;margin-left:5px;cursor:pointer;' onclick='novo(".chr(34).$id.chr(34).",this.name)' src='node/img/mais.png'></img>";
        echo "<input type='text' id='novo_$id' disabled required name='novo_$id' class='liseletor' style='display:none;margin-top:5px;margin-bottom:5px;'>";
      break;
      case 3:
        echo "<input type='text' name = '$id' id='novo_$id' class='liseletor' value='' required='required'>";
      break;
      case 4:
        $rs=odbc_exec($conn,$strsql);
        echo "<select multiple name='$id' $validaSkill id='$id' class='liseletor2' required>";
        echo "<option value=''>$label</option>";
        while (odbc_fetch_row($rs)) {
          echo utf8_encode("<option value=".odbc_result($rs,1).">".odbc_result($rs,2)."</option>");
        }
        echo "<option value=-1>Novo</option>";
        echo "</select><img name='off' id='img_$id' style='width:20px;margin-left:5px;cursor:pointer;' onclick='novo(".chr(34).$id.chr(34).",this.name)' src='node/img/mais.png'></img>";
        echo "<input type='text' id='novo_$id' disabled required name='novo_$id' value = -1 class='liseletor' style='display:none;margin-top:5px;margin-bottom:5px;'>";
      break;
      case 5:
        echo "<div id='div$id'>
            <input type='radio' name='$id' value='t' >Sim</input>
            <input type='radio' name='$id' checked value='f' >Não</input><br>   
          </div>";
      break;
      case 6:
        echo "<input type='text' $validaSkill name = '$id' onkeypress='validate(event)'  id='novo_$id' class='liseletor' value='' required='required'><img src='node/img/ajuda.png' style='cursor:pointer;width: 14px;margin-left:2%;' onclick='consultaAjuda(5)'><div id='ajuda5' style='
    display: none;
    position: absolute;
    background-color: black;box-shadow: -2px 2px 6px #888888;
margin-left: 1%;
width: 350px;
'></div>";
      break;
      case 7:
        $rs=odbc_exec($conn,$strsql);
        echo "<select name='$id' id='$id' $validaSkill class='liseletor' required>";
        echo "<option value=''>$label</option>";
        while (odbc_fetch_row($rs)) {
          echo utf8_encode("<option value=".odbc_result($rs,1).">".odbc_result($rs,2)."</option>");
        }
        echo "</select>";
      break;
      case 8:
        $servidor = array('172.27.203.111 - cmsr14','10.221.236.101 - tivit_101','192.167.235.76 - conexão');
        $valor = array(3,1,4);
        echo "<select name='$id' id='$id' onchange='buscaDac()' class='liseletor' required>";
        echo "<option value=''>$label</option>";
        for ($i=0; $i < sizeof($servidor); $i++) { 
          echo "<option value=".$valor[$i].">".$servidor[$i]."</option>";
        }
      break;
      case 9:
        echo "<select name='$id' id='$id' $validaSkill class='liseletor' required disabled required>";
        echo "<option value=''>$label</option>";
        echo "</select>";
      break;
      case 10:
        $rs=odbc_exec($conn,$strsql);
        echo "<select multiple name='$id' $validaSkill id='$id' class='liseletor2' required>";
        echo "<option value=''>$label</option>";
        while (odbc_fetch_row($rs)) {
          echo utf8_encode("<option value=".odbc_result($rs,1).">".odbc_result($rs,2)."</option>");
        }
        echo "<option value=-1>Novo</option>";
        echo "</select><img name='off' id='img_$id' style='width:20px;margin-left:5px;cursor:pointer;' onclick='novo(".chr(34).$id.chr(34).",this.name)' src='node/img/mais.png'></img>";
        echo "<input type='text' id='novo_$id' disabled required name='novo_$id' value = -1 class='liseletor' style='display:none;margin-top:5px;margin-bottom:5px;'>";
      break;
      }

      odbc_close($conn);
    }

  function inicializaAjuda(){

    echo '
      <div id="help" style="position:absolute;background-color:#F7F9FA;;z-index:10;box-shadow: -4px 2px 10px #888888;">
        <h1 onclick="fechaAjuda('.$_GET['campo'].')" style="cursor:pointer;float:right">X</h1>';

    switch ($_GET['campo']) {
      case 1:
        echo " Utilizar quando houver a necessidade de alteração de qualquer informação relacionada ao skill. Tais como: Coordenação, Consultor Responsável, etc.";
      break;
      case 2:
        echo "Utilizar quando for necessária a exclusão de um ou mais skills da composição de resultados.";
      break;
      case 3:
        echo "Utilizar quando houver a criação de um ou mais skill, onde este irá compor os resultados.";
      break;
      case 5:
        echo "Dever ser informado o número do skill referente à solicitação. É possível incluir mais de um skill por vez, desde que Servidor e DAC sejam os mesmos.";
      break;
      case 6:
        echo "Inserir a partir de qual data os dados devem ser computados para atualização dos relatórios.";
      break;
      case 7:
        echo "Campo para a inserção de informações adicionais.";
      break;
      case 8:
        echo "Campo de busca por número da solicitação";
      break;
    }

    echo "</div>";
  }

  function inicializaFiltros(){

    echo' Informe os campos abaixo: </br>

      <table style="list-style:none;position:relative;float:left;width: 100%;">';

          header("X-XSS-Protection: 0");
          //declaracao das tabelas a serem consultadas, bem como as labels e ids correspondentes
          $mtabela = array(   
                    array('tbl_servidor', 'Servidor', 'servidor', 8),
                    array('tbl_dac', 'Dac', 'dac', 9),
                    array('skill', 'Skill', 'skill', 6),
                    array('tbl_assunto_1', 'Produto', 'assunto_1', 2),
                    array('tbl_assunto_2', 'Operação', 'assunto_2', 2),
                    array('tbl_assunto_3', 'Assunto', 'assunto_3', 2),
                    array('tbl_diretoria', 'Diretoria', 'diretoria', 2),
                    array('tbl_diretor', 'Diretor', 'diretor', 2),
                    array('tbl_gerente', 'Gerência', 'gerencia', 2),
                    array('tbl_consultor', 'Consultor', 'consultor', 2),
                    array('tbl_marca', 'Marca', 'marca[]', 10),
                    array('tbl_npc', 'NPC', 'npc', 2),
                    array('tbl_canal', 'Canal', 'canal', 7),
                    array('tbl_midia', 'Tipo de midia', 'midia', 7),
                    array('tbl_publico', 'Público', 'publico[]', 4),
                    array('tbl_tipo', 'Tipo de skill', 'tipo', 7),
                    array('tbl_calchc', 'Cálculo para hc', 'calchc', 7),
                    array('tbl_ccusto', 'Centro de custo', 'ccusto', 2),
                    array('tbl_empresa', 'Empresa', 'empresa', 2),
                    array('tbl_atividadecustos', 'Atividade', 'atividadecustos', 2),
                    array('Sac', 'Sac Decreto', 'sacdec', 5),
                    array('SE', 'Serviço Emergencial', 'emergencial', 5),
                    array('VIP', 'Fila VIP', 'vip', 5),
                    array('24', '24 horas', '"24horas"', 5)
            );
          
          //Varre as arrays e incrementa a tabela
          echo "<tr>";

          for ($i=0; $i < count($mtabela);$i++) {
            $tabela = $mtabela[$i][0];
            $label = $mtabela[$i][1];
            $id = $mtabela[$i][2];
            $algoritmo = $mtabela[$i][3];

            echo "<td class='li' style='width:25%'>";
            echo "<label for='lst_$id'>$label:</label></br>";
            
            inicializaCampos($algoritmo,$id,$label,$tabela);

            echo "</td>";
            if(($i+1)%3==0){
              echo "</tr><tr>";
            }   
          }
          

      echo '</table>';

  }

  function listaNomenclatura($tabela){

    echo "<input type='text' id='campoBusca' value='BUSCA' onkeyup='filtraNomenclatura()' style='margin-left: 40px;'></input>";
    $conn=odbc_connect('MISPG','','');

    $strsql = "SELECT *
              FROM skills.$tabela ORDER BY 2";

    $rs=odbc_exec($conn,$strsql);
    echo "<center style='height:50%;overflow:auto'>";

    echo "<ul id='busca' style='width:70%;overflow:auto;list-style:none'><tr style='height:40px'>";  
    while (odbc_fetch_row($rs)) {
      echo utf8_encode("<li style='margin:2px;float:left;width:30%;text-align: left;'>".odbc_result($rs,2)."</li>");
    }
    echo "</ul>";
    echo "</center></br></br>";

  }

  function verificaSkill($skill,$dac,$servidor){
    $conn=odbc_connect('MISPG','','');

    $strsql ="SELECT skill
              FROM skills.tbl_skill 
              WHERE data = CURRENT_DATE and skill = $skill and servidor = $servidor and dac = $dac";

    $rs=odbc_exec($conn,$strsql);

    $retorno = odbc_result($rs,'skill');

    if($retorno <> ''){
      return 1;
    }
    else{
      return 0;
    }

  }

  if(isset($_GET['acao'])){
    $acao = $_GET['acao'];
    switch ($acao) {
      case 'form':
        inicializaFiltros();
      break;
      case 'nome':
        inicializaNomenclatura();
      break;
      case 'nome_cat':
        listaNomenclatura($_GET['cat']);
      break;
      case 'ajuda':
        inicializaAjuda();
      break;
      case 'valida_skill':
        echo verificaSkill($_GET['skill'],$_GET['dac'],$_GET['servidor']);
      break;
    }
  }

?>
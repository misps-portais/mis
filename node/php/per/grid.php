<link href="js/tablecloth.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="js/tablecloth.js"></script>

<style>
body{
	margin:0;
	padding:0;
	background:#f1f1f1;
	font:70% Arial, Helvetica, sans-serif; 
	color:#555;
	line-height:100%;
	text-align:left;
}
a{
	text-decoration:none;
	color:#057fac;
}
a:hover{
	text-decoration:none;
	color:#999;
}

#container{
	margin:0 auto;
	width:100%;
	background:#fff;
	padding-bottom:20px;
}

#content{
	margin:0 20px;
}

p.sig{	
	margin:0 auto;
	width:100%;
	padding:1em 0;
}
form{
	margin:1em 0;
	padding:.2em 20px;
	background:#eee;
}
</style>
<?
	$conn = odbc_connect('MISPG','','');
	
	error_reporting(0);
	$tela = $_GET["tela"];
	$id_skill = $_GET["id_skill"];
	$mes = $_GET["mes"];
	$dia = $_GET["dia"];
	
	$blevisky = array("Jan"=>1,"Fev"=>2,"Mar"=>3,"Abr"=>4,"Mai"=>5,"Jun"=>6,"Jul"=>7,"Ago"=>8,"Set"=>9,"Out"=>10,"Nov"=>11,"Dez"=>12);
	$mes = $blevisky[$mes];

		$sql = "SELECT * FROM performance.proc_performance_tela1(" . $tela . ",$id_skill,$mes,'$dia')";
	
	$rs=odbc_exec($conn,$sql);
	
		echo "<table id=\"container\"><thead><tr>";
		echo "<th>Data</th>";
		echo "<th>REC</th>";
		echo "<th>REC_DIM</th>";
		echo "<th>REC_DESV</th>";
		echo "<th>ABD_PERC</th>";
		echo "<th>ABD_DIM</th>";
		echo "<th>ABD_DESV</th>";
		echo "<th>TMO</th>";
		echo "<th>TMO_DIM</th>";
		echo "<th>TMO_DESV</th>";
		echo "<th>NS60</th>";
		echo "<th>HC</th>";
		echo "<th>HC_DIM</th>";
		echo "<th>HC_DESV</th>";
		echo "</tr></thead>";
		
			$st = odbc_result($rs,'periodo');
			
		while(odbc_fetch_row($rs)){
			$periodo = odbc_result($rs,'periodo');
			$rec = odbc_result($rs,'rec');
			$rec_dim = odbc_result($rs,'rec_dim');
			$rec_desv = odbc_result($rs,'rec_desv');
			$abd_perc = odbc_result($rs,'abd_perc');
			$abd_dim = odbc_result($rs,'abd_dim');
			$abd_desv = odbc_result($rs,'abd_desv');
			$tmo = odbc_result($rs,'tmo');
			$tmo_dim = odbc_result($rs,'tmo_dim');
			$tmo_desv = odbc_result($rs,'tmo_desv');
			$ns = odbc_result($rs,'ns60');
			$hc = odbc_result($rs,'hc');
			$hc_dim = odbc_result($rs,'hc_dim');
			$hc_desv = odbc_result($rs,'hc_desv');
		
				echo "<tr>";
				echo "<td>" . $periodo . "</td>";
				echo "<td>" . $rec . "</td>";
				echo "<td>" . $rec_dim . "</td>";
				echo "<td>" . $rec_desv . "</td>";
				echo "<td>" . $abd_perc . "</td>";
				echo "<td>" . $abd_dim . "</td>";
				echo "<td>" . $abd_desv . "</td>";
				echo "<td>" . $tmo . "</td>";
				echo "<td>" . $tmo_dim . "</td>";
				echo "<td>" . $tmo_desv . "</td>";
				echo "<td>" . $ns . "</td>";
				echo "<td>" . $hc . "</td>";
				echo "<td>" . $hc_dim . "</td>";
				echo "<td>" . $hc_desv . "</td>";
				echo "</tr>";
		}
	echo "</table>";
?>
	
	<?php
	
	header ('Content-type: text/html; charset=iso-8859-1');
	
		$d = date('Y-m-d');
		function graph($div,$titulo,$label,$dir,$dem,$cor){

			$dir2 = number_format($dir,0,',','.');
			echo "
				$('#$div').highcharts({
					chart: {
					    plotBackgroundColor: null,
					    plotBorderWidth: 0,
					    plotShadow: false
					},
					title: {
					    text: '$titulo<br\>$dir2',
					    align: 'center',
					    verticalAlign: 'middle',
					    y: -20
					},
					tooltip: {
						enabled: false,
					    pointFormat: '{series.name}: <b>{point.y:f}</b>'
					},
					plotOptions: {
					    pie: {
						  dataLabels: {
	                        enabled: false
	                    },
						startAngle: -90,
						endAngle: 270,
						center: ['50%', '50%']
					    }
					},
					series: [{
					    type: 'pie',
					    name: '$label',
					    innerSize: '98%',
					    data: [
						{name:'Diretoria<br>Atd',   y: $dir,color:'$cor'},
						{name:'',   y: $dem,color:'#ffffff'}
					    ]
					}],
				    });
			";
		};

		function graph_por($div,$titulo,$label,$dir,$dem,$cor){

			$dir_com = 100 - $dir;
			$dem_com = 100 - $dem;
			$dir2 = number_format($dir,2,',','.');
			echo "
			    $('#$div').highcharts({
				chart: {
				    plotBackgroundColor: null,
				    plotBorderWidth: 0,
				    plotShadow: false
				},
				title: {
				    text: '$titulo<br\>$dir2%',
				    align: 'center',
				    verticalAlign: 'middle',
				    y: -20
				},
				tooltip: {
					enabled: false,
				    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				},
				plotOptions: {
				    pie: {
					  dataLabels: {
                        enabled: false
                    },
					startAngle: -90,
					endAngle: 270,
					center: ['50%', '50%']
				    }
				},
				series: [{
				    type: 'pie',
				    name: '$label',
				    innerSize: '98%',
				    data: [
					{name:'Diretoria<br>Atd',   y: $dir,color:'$cor'},
					{name:'',   y: $dir_com,color:'#ffffff'}
				    ]
				}]
			    });
			";
		};

		$conn = odbc_connect('MISPG','','');
			$sql = "SELECT 
			tdir.diretoria as diretoria,
			SUM(ta.callsoffered)::text AS recebidas,
			CASE SUM(ta.callsoffered)
					WHEN 0 THEN
						0::text
					ELSE
						ROUND(CAST((SUM(ta.abncalls)-SUM(ta.abncalls1)) AS numeric)/CAST(SUM(ta.callsoffered) AS numeric)*100,2)::text
				END AS abd_per,
				CASE sum(ta.callsoffered)
					WHEN 0 THEN 
						0::text
					ELSE 
						ROUND(CAST(sum(ta.acdcalls-ta.acdcalls10-ta.acdcalls9) AS numeric) / CAST(sum(ta.callsoffered) as numeric)*100,2)::text
				END as ns60
			FROM performance.tbl_performance_dia_2013 as ta
				INNER JOIN skills.tbl_skill tsk ON
				tsk.dac = ta.dac and tsk.servidor = ta.servidor and tsk.skill = ta.skill and tsk.data = ta.row_date
				INNER JOIN skills.tbl_diretoria tdir ON
				tdir.id = tsk.diretoria
			WHERE row_date = current_date
			GROUP BY ta.row_date, tdir.diretoria,tsk.diretoria
			ORDER BY tsk.diretoria,tdir.diretoria,ta.row_date;";
		$rs = odbc_exec($conn, $sql);

		$i = 0;

		while(odbc_fetch_row($rs)){

			$resultado[$i][0] = odbc_result($rs,'recebidas');
			$resultado[$i][1] = odbc_result($rs,'abd_per');
			$resultado[$i][2] = odbc_result($rs,'ns60');
			$i++;	

		}

		$sql = "SELECT to_char(max(data_atualiza) + '00:30:00','DD-MM-YYYY HH24:MI:ss') as data_atualiza FROM avaya.tbl_avaya_extracao_info";
		$rs = odbc_exec($conn, $sql);

		$data_atualiza = odbc_result($rs, 'data_atualiza'); 

	?>
	<meta http-equiv="refresh" content="600" >
<title>Performance</title>
<html>
	<head>
		<link href='http://fonts.googleapis.com/css?family=Share+Tech|Noto+Sans|Open+Sans|Sintony' rel='stylesheet' type='text/css'><link href='http://fonts.googleapis.com/css?family=Share+Tech|Noto+Sans|Open+Sans|Sintony' rel='stylesheet' type='text/css'>
<script src="js/jquery-1.10.1.min.js"></script>
		<script src="highcharts.js"></script>
		<script src="js/performance.js"></script>
		<script>
		
			$(function () {<?php graph('rec_gra_dir','Liga��es<br>Recebidas','Recebidas',$resultado[0][0],$resultado[1][0],'#46b4af') ?>});

			$(function () {<?php graph_por('abd_gra_dir','Liga��es<br>Abandonadas','% Abandono',$resultado[0][1],$resultado[1][1],'#46b4af') ?>});

			$(function () {<?php graph_por('ns_gra_dir','N�vel de<br>Servi�o','NS 60',$resultado[0][2],$resultado[1][2],'#46b4af') ?>});

			$(function () {<?php graph('rec_gra_dem','Liga��es<br>Recebidas','Recebidas',$resultado[1][0],$resultado[0][0],'#3498db') ?>});

			$(function () {<?php graph_por('abd_gra_dem','Liga��es<br>Abandonadas','% Abandono',$resultado[1][1],$resultado[0][1],'#3498db') ?>});

			$(function () {<?php graph_por('ns_gra_dem','N�vel de<br>Servi�o','NS 60',$resultado[1][2],$resultado[0][2],'#3498db') ?>});

		</script>
	</head>
	<body style="height:100%;width:100%;background-color:#ffffff;overflow: hidden;">
		<div id="filtros" style="width:96%;height:15%;margin-left:2%;background-color:#F8FBFC">
			<p style="font-family: 'Sintony';color:#274b6d;font-size:2em;margin:0% 0% 0% 4%;padding-top:0.75%">Relat�rio de Performance</p>
			<p style="font-family: 'Sintony';color:#274b6d;font-size:.8em;margin:0% 0% 0% 4%;">Atualizado at� <?php echo $data_atualiza; ?></p>
					<form id="filtro" method="get" id="form" action="index.php" onchange="filtro_troca2();" style="margin-top:0%;">
					<meta charset="utf-8">

<center>

	<?php

		$where = '';
		$ind = array('diretoria','marca','npc','gerente','consultor','assunto_1','assunto_2');
		$in = array('diretoria','marca','npc','gerente','consultor','as1','as2');

			for ($i=0; $i < sizeof($ind) ; $i++) { 
				echo "<select name='$ind[$i]' id='$ind[$i]' style='width: 110px;margin-bottom: 3%;margin-right: 1%;''><option value='0'>$ind[$i]</option>";

						$d = date('Y-m-d');
						$conn = odbc_connect('MISPG','','');
						$sql = "SELECT DISTINCT $ind[$i], id_$in[$i] FROM performance.proc_performance_filtro('$where',0,'$d') ORDER BY $ind[$i]";
						$rs = odbc_exec($conn, $sql);
						if ($rs)
						  {
						while (odbc_fetch_row($rs)) 
						{
						  $valor = odbc_result ($rs,"id_$in[$i]");
						  $nome = odbc_result($rs,"$ind[$i]");
							echo "<option value='$valor'>$nome</option>\n";
						  }
						}

				echo "</select>";
			}

	?>

<input type='submit' value='Consulta'>
</center>
					</form>
		</div>

		<div	id="indicadores" style="width:96%;height:75%;margin-left:2%;margin-top:1%;background-color:#F8FBFC;position:absolute">
			<p style="font-family: 'Sintony';color:#274b6d;font-size:1em;margin:2% 0% 0% 4%;">Diretoria de Atendimento</p>
			<div id="rec" style="height:150%;width:30%;position:relative;float:left;margin-top:1%;margin-left:2.5%">

				<div id="rec_gra_dir" style="width:100%;height:30%;margin:5%;box-shadow: 0px 4px 10px #888888;">
				</div>
				<p style="font-family: 'Sintony';color:#274b6d;font-size:1em;margin:0% 0% 0% 4%;">Demais diretorias</p>
				<div id="rec_gra_dem" style="width:100%;height:30%;margin:5%;box-shadow: 0px 4px 10px #888888;">
				</div>
			</div>
			<div id="abd" style="height:150%;width:30%;position:relative;float:left;margin-top:1%;margin-left:2.5%">
				<div id="abd_gra_dir" style="width:100%;height:30%;margin:5%;box-shadow: 0px 4px 10px #888888;">
				</div>
				<div id="abd_gra_dem" style="width:100%;height:30%;margin:5%;margin-top:16%;box-shadow: 0px 4px 10px #888888;">
				</div>
			</div>
			<div id="ns" style="height:150%;width:30%;position:relative;float:left;margin-top:1%;margin-left:2.5%">
				<div id="ns_gra_dir" style="width:100%;height:30%;margin:5%;box-shadow: 0px 4px 10px #888888;">
				</div>
				<div id="ns_gra_dem" style="width:100%;height:30%;margin:5%;margin-top:16%;box-shadow: 0px 4px 10px #888888;">
				</div>			
			</div>
		</div>
	</body>
</html>

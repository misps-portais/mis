  <title>Performance - Atendidas vs Abandonadas</title>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  
  <script src="js/highcharts.js"></script>
  <link href="perf.css" rel="stylesheet" type="text/css">
  <style>
	 #menu{
		position:absolute;
		width:800px;
		height:10px;
		z-index:999;
		left: 8px;
		top: 0px;
	}
	
	#tag{
		position: relative;
		z-index:2;
		-webkit-transform: rotate(270deg);
		float:left;
		top: 80;
	}
	
  </style> 
 
 <?php
 
	error_reporting(0);
	$conn=odbc_connect('MISPG','','');
	
		$id_skill = $_GET["id_skill"]; 
		$mes = $_GET["mes"];
		$dia = $_GET["dia"];
		$assunto = $_GET["assunto_2"];
		
	$blevisky = array(
		"Jan"=>1,
		"Fev"=>2,
		"Mar"=>3,
		"Abr"=>4,
		"Mai"=>5,
		"Jun"=>6,
		"Jul"=>7,
		"Ago"=>8,
		"Set"=>9,
		"Out"=>10,
		"Nov"=>11,
		"Dez"=>12
	);

	
	
	$gra = $_GET['graf'];

	if (empty($gra)){ 
	  $gra = "area";
	}

	$mes = $blevisky[$mes];
	
			$sqlm = "
					SELECT * FROM proc_performance_tela2(1,".$_GET['id_skill'].",01,'2013-01-01')";

				
			//La�o Abandono mes
			
			$rs=odbc_exec($conn,$sqlm);
			
			while(odbc_fetch_row($rs)){

					$recm = odbc_result($rs,'atd');
					$abdm = odbc_result($rs,'abd');
					$recd = odbc_result($rs,'periodo');
					
			$seriestabdm_str = $seriestabdm_str.$recm.",";
			$seriesabdm_str = $seriesabdm_str.$abdm.",";
			$seriestabdmd_str = $seriestabdmd_str."'".$recd."'".",";
			}	
			$seriestabdm_str = substr($seriestabdm_str,0,strlen($seriestabdm_str)-1)."]";
			$seriesabdm_str = substr($seriesabdm_str,0,strlen($seriesabdm_str)-1)."]";
			$seriestabdmd_str = substr($seriestabdmd_str,0,strlen($seriestabdmd_str)-1)."]";
			
			$sql = "
					SELECT * FROM proc_performance_tela2(2,".$_GET['id_skill'].",".$mes.",'2013-01-01')";

			//La�o abandono diario
			$rs=odbc_exec($conn,$sql);
			
			while(odbc_fetch_row($rs)){

					$rec = odbc_result($rs,'atd');
					$abd = odbc_result($rs,'abd');
					$recd = odbc_result($rs,'periodo');
					
							
				$seriesd_str = $seriesd_str.$rec.",";
				$seriesabda_str = $seriesabda_str.$abd.",";
				$seriestreced_str = $seriestreced_str."'".$recd."'".",";
			
			}

			$seriesd_str = substr($seriesd_str,0,strlen($seriesd_str)-1)."]";
			$seriesabda_str = substr($seriesabda_str,0,strlen($seriesabda_str)-1)."]";
			$seriestreced_str = substr($seriestreced_str,0,strlen($seriestreced_str)-1)."]";
			
			
			$sqlh = "
					SELECT * FROM proc_performance_tela2(3,".$_GET['id_skill'].",".$mes.",'$dia')";

			//La�o Abandono intra hora
			$rs=odbc_exec($conn,$sqlh);
			
			while(odbc_fetch_row($rs)){

					$rech = odbc_result($rs,'atd');
					$abd = odbc_result($rs,'abd');
					$recd = odbc_result($rs,'periodo');
							
				$seriestabdh_str = $seriestabdh_str.$rech.",";
				$seriesabdh_str = $seriesabdh_str.$abd.",";
				$seriestabdhd_str = $seriestabdhd_str."'".$recd."'".",";
			
			}
			
			$seriestabdh_str = substr($seriestabdh_str,0,strlen($seriestabdh_str)-1)."]";
			$seriesabdh_str = substr($seriesabdh_str,0,strlen($seriesabdh_str)-1)."]";
			$seriestabdhd_str = substr($seriestabdhd_str,0,strlen($seriestabdhd_str)-1)."]";
$temp = $mes;
?>
<script type="text/javascript">
$(function () {
        $('#abd').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: [<?php echo $seriestabdmd_str; ?>,
				labels: {
                rotation: 300
            }
            }],
            yAxis: [{ // Primary yAxis
				lineWidth: 1,
				min:0,
				gridLineDashStyle: 'dot',
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: ''                    
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' ';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' ';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
			plotOptions: {
                area: {
                    stacking: 'normal',
                    lineColor: '',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#AA4643'
                    }
                }
            },
				series:[{
                name: 'Abd',
                color: '#CD2626',
				type: '<?php echo $gra ?>',
				data:[<?php echo $seriesabdm_str; ?>,
				marker: {
                    enabled: false
                },
                dashStyle: 'Solid',
                tooltip: {
                    valueSuffix: ' '
                }
				},{
				
				name :'Ate',
				color: '#4682B4',
				type: '<?php echo $gra ?>',
				data:[<?php echo $seriestabdm_str; ?>,
				marker: {
						enabled: false
					},
					dashStyle: 'Solid',
					tooltip: {
						valueSuffix: ' '
					}
					
        }]
});
});

</script>

<script type="text/javascript">
$(function () {
        $('#ns').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: [<?php echo $seriestreced_str; ?>,
				labels: {
                rotation: 300
            }
            }],
            yAxis: [{ // Primary yAxis
				lineWidth: 1,
				min:0 ,
				gridLineDashStyle: 'dot',
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: ''                    
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' ';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' ';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
			plotOptions: {
                area: {
                    stacking: 'normal',
                    lineColor: '',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#AA4643'
                    }
                }
            },
				series:[{
                name: 'Abd',
                color: '#CD2626',
				type: '<?php echo $gra ?>',
				data:[<?php echo $seriesabda_str; ?>,
				marker: {
                    enabled: false
                },
                dashStyle: 'Solid',
                tooltip: {
                    valueSuffix: ' '
                }
				},{
				
				name :'Ate',
				color: '#4682B4',
				type: '<?php echo $gra ?>',
				data:[<?php echo $seriesd_str; ?>,
				marker: {
						enabled: false
					},
					dashStyle: 'Solid',
					tooltip: {
						valueSuffix: ' '
					}
					
        }]
});
});

</script>

<script type="text/javascript">
$(function () {
        $('#rece').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: [<?php echo $seriestabdhd_str; ?>,
				labels: {
                rotation: 300
            }
            }],
            yAxis: [{ // Primary yAxis
				lineWidth: 1,
				min:0 ,
				gridLineDashStyle: 'dot',
                labels: {
                    formatter: function() {
                        return this.value;
                    },
                    style: {
                        color: '#89A54E'
                    }
                },
                title: {
                    text: ''                    
                },
                opposite: true
    
            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#4572A7'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' ';
                    },
                    style: {
                        color: '#4572A7'
                    }
                }
    
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: '',
                    style: {
                        color: '#AA4643'
                    }
                },
                labels: {
                    formatter: function() {
                        return this.value +' ';
                    },
                    style: {
                        color: '#AA4643'
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
			plotOptions: {
                area: {
                    stacking: 'normal',
                    lineColor: '',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#AA4643'
                    }
                }
            },
            series:[{
            name :'Abd',
			color: '#CD2626',
			type: '<?php echo $gra ?>',
            
			data:[<?php echo $seriesabdh_str; ?>,
			marker: {
                    enabled: false
                },
                dashStyle: 'Solid',
                tooltip: {
                    valueSuffix: ' '
                }
         },{    
                
                name: 'Ate',
                color: '#4682B4',
                type: '<?php echo $gra ?>',
                data:[<?php echo $seriestabdh_str; ?>,
				marker: {
                    enabled: false
                },
                dashStyle: 'Solid',
                tooltip: {
                    valueSuffix: ' '
                }
        }]
});
});

</script>


<form id="menu" action="" method="get">
		  <select id="forme" name="id_skill" onchange="form.submit(this.value);">
		  <option selected="selected" value="">Selecione a Fila Desejada</option>
		  <?
							$sql = "SELECT DISTINCT npc.id as npcid, tsa.assunto_2 as assunto_2, tsa.id as id
							FROM skills.tbl_skill tsk 
							INNER JOIN skills.tbl_npc npc ON tsk.npc = npc.id
							INNER JOIN skills.tbl_assunto_2 tsa ON tsa.id = tsk.assunto_2
							WHERE npc.id > 0 and npc.id < 4
							ORDER BY npc.id,tsa.assunto_2";
							
							$rs = odbc_exec($conn, $sql);
							if (!$rs)
							  {exit("Error in SQL");}
							while (odbc_fetch_row($rs)) 
							{
							  $id_skill = odbc_result ($rs,"id");
							  $assunto = odbc_result($rs,"assunto_2");
							  if($id_skill == $_GET['id_skill']){
								echo "<option value='$id_skill' selected>$assunto</option>\n";
							  }
							  else{
								echo "<option value='$id_skill'>$assunto</option>\n";
							  }				  
							}
					?>
		  </select>
		  
		  <form action="" method="get">
		  <select id="forme1" name="mes" onchange="form.submit(this.value);">
		  <option selected="selected" value="">M�s</option>
				<?
						$sqlm = "SELECT * FROM proc_performance_tela2(1,".$_GET['id_skill'].",01,'2013-01-01')";
						$rs = odbc_exec($conn, $sqlm);
						if (!$rs)
						  {exit("Error in SQL");}
						while (odbc_fetch_row($rs)) 
						{
						  $mes = odbc_result ($rs,"periodo");
						if($mes == $_GET['mes']){
							echo "<option value='$mes' selected>$mes</option>\n";
						  }
						  else{
							echo "<option value='$mes'>$mes</option>\n";
						  }				  
						}

					?>
			</select>
			
			<select name="dia" id="dia" onchange="form.submit(this.value);">
			<option selected="selected" value="">Dia</option>
				  <?php
						$sqld = "SELECT * FROM proc_performance_tela1(2,".$_GET['id_skill'].",".$temp.",'2013-01-01')";
						$rs = odbc_exec($conn, $sqld);
						if (!$rs)
						  {exit("Error in SQL");}
						while (odbc_fetch_row($rs)) 
						{
						  $dia = odbc_result ($rs,"periodo");
						  $dia2 = "2013-".$temp."-".explode("-",$dia)[0];
						if($dia2 == $_GET['dia']){
							echo "<option value='$dia2' selected>$dia</option>\n";
						  }
						  else{
							echo "<option value='$dia2'>$dia</option>\n";
						  }				  
						}

				  ?>

			  </select>
		  
			<select name="graf" id="valor" class="selct" >
				<option selected="selected" value="">Tipo</option>
				<option value="area">Area</option>
				<option value="column">Coluna</option>
				<option value="line">Linha</option>
				<option value="spline">Tendencia</option>
			</select>

</form>


<?php

	$ind = array("abd","ns","rece");
	$tag = array("Mensal","Di�rio","Intra-di�rio");
	$vis = array("","","");

	for($i = 0; $i<sizeof($ind);$i++){
		$local = $ind[$i];
		$local2 = $tag[$i];
		echo "<div id='res$local' style='width:100%;position:relative;z-index: 1;'>
				<div class='bt' id='tag' style='width:5%;'>$local2</div>";
				for($j = 0 ; $j<sizeof($vis);$j++){
					$v = $vis[$j];
					echo "<div id='$local$v' class='graf'></div>";
				}
		echo "</div></br>";
	}
?>
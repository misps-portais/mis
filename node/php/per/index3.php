<script src="js/performance.js"></script>
<style type="text/css">
	#menu{
		position:relative;
		width:783px;
		height:57px;
		z-index:2;
		left: 8px;
		top: 0px;
	}
	#tag{
		position: relative;
		z-index:2;
		-webkit-transform: rotate(270deg);
		float:left;
		top: 80;
	}
	.graf{
		width: 30%;
		height: 250px;
		margin: 0 auto;
		position: relative;
		float: left;
	}
</style>

    <script src="js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="js/highcharts.js"></script>
	<script src="js/jquery.modal.js" type="text/javascript" charset="utf-8"></script>
	<link href="perf.css" rel="stylesheet" media="screen">
	<link rel="stylesheet" href="js/jquery.modal.css" type="text/css" media="screen" />
	

<?php
//colors: ['$cor'],
	function graf_wr($cor,$desv,$categorias,$real,$plan,$tipo,$caixa){

		echo "
	colors: ['$cor'],
    chart: {
      renderTo: '$caixa'
    },
       xAxis: [{
                categories: [$categorias,
				labels: {
                rotation: 300
            }
            }],

		series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '$tipo',
            data:[$real,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
				valueSuffix: ''
			}
			},{    
			name: 'Planejado',
			color: '#CD2626',
			type: 'line',
			data:[$plan,
			marker: {
			enabled: false
			},
				dashStyle: 'Solid',
		},{ 
			name: '$desv',
			type: 'pie',
			data:[37],
            borderWidth: 0,
						center: [305, 195],
                size: 45,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
					fontSize: '12px',
					fontWeight:'bold',
					formatter: function() {
                        return this.series.name + '%';
                    },
                    color: 'black',
                    distance: -22
				}
        }]";
	}
//colors: ['$cor'],
	function graf_r($cor,$desv,$categorias,$real,$plan,$replan,$tipo,$caixa){

		echo "
		chart: {
      renderTo: '$caixa'
    },
       xAxis: [{
                categories: [$categorias,
				labels: {
                rotation: 300
            }
            }],

		series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '$tipo',
            data:[$real,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
				valueSuffix: ''
			}
			},{    
			name: 'Re-Forecast',
			color: '#46b4af',
			type: 'area',
			data:[$replan,
			visible: false,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
			valueSuffix: ''
			}
			},{    
			name: 'Planejado',
			color: '#CD2626',
			type: 'line',
            data:[$plan,
			marker: {
			enabled: false
			},
				dashStyle: 'Solid',
},{ 
			name: '$desv',
			type: 'pie',
			data:[100],
            borderWidth: 0,
						center: [310, 195],
                size: 45,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
					fontSize: '12px',
					fontWeight:'bold',
					formatter: function() {
                        return this.series.name + '%';
                    },
                    color: 'black',
                    distance: -22
				}
        }]";
	}

	error_reporting(0);
	$gra = $_GET["graf"];
	$conn = odbc_connect('MISPG','','');
	$cor = array('#61ffb3','#ffff61','#ff6161');

		if (empty($gra)){ 
		  $gra = "area";
		}
		
		$mes = $_GET['mes'];
		$dia = $_GET['dia'];
		$id_skill = $_GET['id_skill'];
		$assunto = $_GET['assunto_2'];

		$blevisky = array("Jan"=>1,"Fev"=>2,"Mar"=>3,"Abr"=>4,"Mai"=>5,"Jun"=>6,"Jul"=>7,"Ago"=>8,"Set"=>9,"Out"=>10,"Nov"=>11,"Dez"=>12);

		$mes = $blevisky[$mes];
			
			//La�o MENSAL de realizado e forecast com desvios
			//$sqlm = "SELECT * FROM performance.proc_performance_final(0,".$_GET['id_skill'].",11,'2013-11-07')";
			$sqlm = "SELECT * FROM performance.proc_performance_final(0, 1, '11', 10, '2013-10-01')";
			$rs = odbc_exec($conn, $sqlm);
				odbc_fetch_row($rs);
			
				$mrecd = odbc_result($rs,'rec_desv');
				$mabdd = odbc_result($rs,'abd_desv');
				$mtmod = odbc_result($rs,'tmo_desv');
				$mhcd = odbc_result($rs,'hc_desv');
				$mabdpp = odbc_result($rs,'abd_perc');
			
				$mesteira = array('rec'=>(float)$mrecd,'tmo'=>(float)$mtmod,'hc'=>(float)$mhcd);
				asort($mesteira); 
				$i = 0;

				while(current($mesteira)){

					$mesteira_cor[key($mesteira)][0] = current($mesteira);
					$mesteira_cor[key($mesteira)][1] = $cor[$i]; 
					next($mesteira);
					$i++;

				}
	
			while(odbc_fetch_row($rs)){
			
				$mperiodo = odbc_result($rs,'periodo');
				$mabdr = odbc_result($rs,'abd_perc');
				$mabdp = odbc_result($rs,'abd_dim');
				$mns60r = odbc_result($rs,'ns60');
				$mrecr = odbc_result($rs,'rec');
				$mrecp = odbc_result($rs,'rec_dim');
				$mtmor = odbc_result($rs,'tmo');
				$mtmop = odbc_result($rs,'tmo_dim');
				$mhcr = odbc_result($rs,'hc');
				$mhcp = odbc_result($rs,'hc_dim');
				
				
				$mperiodo_str = $mperiodo_str."'".$mperiodo."'".",";
				$mabd_str = $mabd_str.$mabdr.",";
				$mabdp_str = $mabdp_str.$mabdp.",";
				$mns60r_str = $mns60r_str.$mns60r.",";
				$mrecr_str = $mrecr_str.$mrecr.",";
				$mrecp_str = $mrecp_str.$mrecp.",";
				$mtmor_str = $mtmor_str.$mtmor.",";
				$mtmop_str = $mtmop_str.$mtmop.",";
				$mhcr_str = $mhcr_str.$mhcr.",";
				$mhcp_str = $mhcp_str.$mhcp.",";			
			}
			
			$mperiodo_str = substr($mperiodo_str,0,strlen($mperiodo_str)-1)."]";
			$mabd_str = substr($mabd_str,0,strlen($mabd_str)-1)."]";
			$mabdp_str = substr($mabdp_str,0,strlen($mabdp_str)-1)."]";
			$mns60r_str = substr($mns60r_str,0,strlen($mns60r_str)-1)."]";
			$mrecr_str = substr($mrecr_str,0,strlen($mrecr_str)-1)."]";
			$mrecp_str = substr($mrecp_str,0,strlen($mrecp_str)-1)."]";
			$mtmor_str = substr($mtmor_str,0,strlen($mtmor_str)-1)."]";
			$mtmop_str = substr($mtmop_str,0,strlen($mtmop_str)-1)."]";
			$mhcr_str = substr($mhcr_str,0,strlen($mhcr_str)-1)."]";
			$mhcp_str = substr($mhcp_str,0,strlen($mhcp_str)-1)."]";
			
			//POG para coletar %desv
			//$sqlmdes = "SELECT * FROM performance.proc_performance_tela1(1,".$_GET['id_skill'].",".$mes.",'2013-11-07')";
			$sqlmdes = "SELECT * FROM performance.proc_performance_final(0, 1, '11', 10, '2013-10-01')";
			$rs = odbc_exec($conn, $sqlmdes);
				odbc_fetch_row($rs);
			
				$hrechh = odbc_result($rs,'abd_desv');
			
			//$sqlmdes = "SELECT * FROM performance.proc_performance_tela1(2,".$_GET['id_skill'].",".$mes.",'2013-11-07')";
			$sqlmdes = "SELECT * FROM performance.proc_performance_final(0, 1, '11', 10, '2013-10-01')";
			$rs = odbc_exec($conn, $sqlmdes);
				odbc_fetch_row($rs);
			
				$mrecdd = odbc_result($rs,'abd_desv');
				
			//POG para coletar %desv
			//$sqlmdes = "SELECT * FROM performance.proc_performance_tela1(3,".$_GET['id_skill'].",".$mes.",'".$dia."')";
			$sqlmdes = "SELECT * FROM performance.proc_performance_final(0, 1, '11', 10, '2013-10-01')";
			$rs = odbc_exec($conn, $sqlmdes);
				odbc_fetch_row($rs);
			
				$drecdd = odbc_result($rs,'abd_desv');
			
			//La�o DIARIO de realizado e forecast com desvios
			//$sqld = "SELECT * FROM performance.proc_performance_tela1(2,".$_GET['id_skill'].",".$mes.",'2013-10-20')";
			$sqld = "SELECT * FROM performance.proc_performance_final(0, 1, '11', 10, '2013-10-01')";
			$rs = odbc_exec($conn, $sqld);

				odbc_fetch_row($rs);
			
				$drecd = odbc_result($rs,'rec_desv');
				$dabdd = odbc_result($rs,'abd_desv');
				$dtmod = odbc_result($rs,'tmo_desv');
				$dhcd = odbc_result($rs,'hc_desv');

				$desteira = array('rec'=>$drecd,'tmo'=>$dtmod,'hc'=>$dhcd);
				asort($desteira); 
				$i = 0;

				while(current($desteira)){

					$desteira_cor[key($desteira)][0] = current($desteira);
					$desteira_cor[key($desteira)][1] = $cor[$i];
					next($desteira);
					$i++;

				}

			while(odbc_fetch_row($rs)){
			
				$dperiodo = odbc_result($rs,'periodo');
				$dabdr = odbc_result($rs,'abd_perc');
				$dabdp = odbc_result($rs,'abd_dim');
				$dns60r = odbc_result($rs,'ns60');
				$drecr = odbc_result($rs,'rec');
				$drecp = odbc_result($rs,'rec_dim');
				$dtmor = odbc_result($rs,'tmo');
				$dtmop = odbc_result($rs,'tmo_dim');
				$dhcr = odbc_result($rs,'hc');
				$dhcp = odbc_result($rs,'hc_dim');
				
				$dperiodo_str = $dperiodo_str."'".$dperiodo."'".",";
				$dabd_str = $dabd_str.$dabdr.",";
				$dabdp_str = $dabdp_str.$dabdp.",";
				$dabdd_str = $dabdd_str.$dabdd.",";
				$dns60r_str = $dns60r_str.$dns60r.",";
				$drecr_str = $drecr_str.$drecr.",";
				$drecp_str = $drecp_str.$drecp.",";
				$drecd_str = $drecd_str.$drecd.",";
				$dtmor_str = $dtmor_str.$dtmor.",";
				$dtmop_str = $dtmop_str.$dtmop.",";
				$dtmod_str = $dtmod_str.$dtmod.",";
				$dhcr_str = $dhcr_str.$dhcr.",";
				$dhcp_str = $dhcp_str.$dhcp.",";
				$dhcd_str = $dhcd_str.$dhcd.",";
			}
			
			$dperiodo_str = substr($dperiodo_str,0,strlen($dperiodo_str)-1)."]";
			$dabd_str = substr($dabd_str,0,strlen($dabd_str)-1)."]";
			$dabdp_str = substr($dabdp_str,0,strlen($dabdp_str)-1)."]";
			$dabdd_str = substr($dabdd_str,0,strlen($dabdd_str)-1)."]";
			$dns60r_str = substr($dns60r_str,0,strlen($dns60r_str)-1)."]";
			$drecr_str = substr($drecr_str,0,strlen($drecr_str)-1)."]";
			$drecp_str = substr($drecp_str,0,strlen($drecp_str)-1)."]";
			$drecd_str = substr($drecd_str,0,strlen($drecd_str)-1)."]";
			$dtmor_str = substr($dtmor_str,0,strlen($dtmor_str)-1)."]";
			$dtmop_str = substr($dtmop_str,0,strlen($dtmop_str)-1)."]";
			$dtmod_str = substr($dtmod_str,0,strlen($dtmod_str)-1)."]";
			$dhcr_str = substr($dhcr_str,0,strlen($dhcr_str)-1)."]";
			$dhcp_str = substr($dhcp_str,0,strlen($dhcp_str)-1)."]";
			$dhcd_str = substr($dhcd_str,0,strlen($dhcd_str)-1)."]";

			
			//La�o INTRA-DIA de realizado e forecast com desvios
			//$sqlh = "SELECT * FROM performance.proc_performance_tela1(3,".$_GET['id_skill'].",".$mes.",'".$dia."')";
			$sqlh = "SELECT * FROM performance.proc_performance_final(0, 1, '11', 10, '2013-10-01')";
			$rs = odbc_exec($conn, $sqlh);
			odbc_fetch_row($rs);
			
				$hrecd = odbc_result($rs,'rec_desv');
				$habdd = odbc_result($rs,'abd_desv');
				$htmod = odbc_result($rs,'tmo_desv');
				$hhcd = odbc_result($rs,'hc_desv');
				
				$hesteira = array('rec'=>$hrecd,'tmo'=>$htmod,'hc'=>$hhcd);
				
				asort($hesteira); 
				$i = 0;

				while(current($hesteira)){

					$hesteira_cor[key($hesteira)][0] = current($hesteira);
					$hesteira_cor[key($hesteira)][1] = $cor[$i]; 
					next($hesteira);
					$i++;

				}
	

			while(odbc_fetch_row($rs)){
			
				$hperiodo = odbc_result($rs,'periodo');
				$habdr = odbc_result($rs,'abd_perc');
				$habdp = odbc_result($rs,'abd_dim');
				$hns60r = odbc_result($rs,'ns60');
				$hrecr = odbc_result($rs,'rec');
				$hrecp = odbc_result($rs,'rec_dim');
				$htmor = odbc_result($rs,'tmo');
				$htmop = odbc_result($rs,'tmo_dim');
				$hhcr = odbc_result($rs,'hc');
				$hhcp = odbc_result($rs,'hc_dim');
				
				$hperiodo_str = $hperiodo_str."'".$hperiodo."'".",";
				$habd_str = $habd_str.$habdr.",";
				$habdp_str = $habdp_str.$habdp.",";
				$habdd_str = $habdd_str.$habdd.",";
				$hns60r_str = $hns60r_str.$hns60r.",";
				$hrecr_str = $hrecr_str.$hrecr.",";
				$hrecp_str = $hrecp_str.$hrecp.",";
				$hrecd_str = $hrecd_str.$hrecd.",";
				$htmor_str = $htmor_str.$htmor.",";
				$htmop_str = $htmop_str.$htmop.",";
				$htmod_str = $htmod_str.$htmod.",";
				$hhcr_str = $hhcr_str.$hhcr.",";
				$hhcp_str = $hhcp_str.$hhcp.",";
				$hhcd_str = $hhcd_str.$hhcd.",";
			}
			
			$hperiodo_str = substr($hperiodo_str,0,strlen($hperiodo_str)-1)."]";
			$habd_str = substr($habd_str,0,strlen($habd_str)-1)."]";
			$habdp_str = substr($habdp_str,0,strlen($habdp_str)-1)."]";
			$habdd_str = substr($habdd_str,0,strlen($habdd_str)-1)."]";
			$hns60r_str = substr($hns60r_str,0,strlen($hns60r_str)-1)."]";
			$hrecr_str = substr($hrecr_str,0,strlen($hrecr_str)-1)."]";
			$hrecp_str = substr($hrecp_str,0,strlen($hrecp_str)-1)."]";
			$hrecd_str = substr($hrecd_str,0,strlen($hrecd_str)-1)."]";
			$htmor_str = substr($htmor_str,0,strlen($htmor_str)-1)."]";
			$htmop_str = substr($htmop_str,0,strlen($htmop_str)-1)."]";
			$htmod_str = substr($htmod_str,0,strlen($htmod_str)-1)."]";
			$hhcr_str = substr($hhcr_str,0,strlen($hhcr_str)-1)."]";
			$hhcp_str = substr($hhcp_str,0,strlen($hhcp_str)-1)."]";
			$hhcd_str = substr($hhcd_str,0,strlen($hhcd_str)-1)."]";
				
			// ReforeCast - se est� lendo isso a responsa agora � sua!
			//$sqlrefm = "SELECT * FROM forecast.proc_performance_tela1_reforecast(2,".$_GET['id_skill'].",".$mes.",'2013-10-01')";
			$sqlrefm = "SELECT * FROM performance.proc_performance_final(0, 1, '11', 10, '2013-10-01')";
			$rs = odbc_exec($conn, $sqlrefm);
			while(odbc_fetch_row($rs)){
			
				$drecr = odbc_result($rs,'rec_redim');
				$dtmor = odbc_result($rs,'tmo_redim');
				$dhcr = odbc_result($rs,'hc_redim');
				
				$drecref_str = $drecref_str.$drecr.",";
				$dtmoref_srt = $dtmoref_srt.$dtmor.",";
				$dhcref_str = $dhcref_str.$dhcr.",";
			}
			$drecref_str = substr($drecref_str,0,strlen($drecref_str)-1)."]";
			$dtmoref_srt = substr($dtmoref_srt,0,strlen($dtmoref_srt)-1)."]";
			$dhcref_str = substr($dhcref_str,0,strlen($dhcref_str)-1)."]";
			
			//REFORECAST INTRA HORA - ACHAMOS UM NOVO CULPADO...VOC�!!!
			$sqlrefh = "SELECT * FROM forecast.proc_performance_tela1_reforecast(3,".$_GET['id_skill'].",".$mes.",'".$dia."')";	
			$rs = odbc_exec($conn, $sqlrefh);
			odbc_fetch_row($rs);
			while(odbc_fetch_row($rs)){
			
				$hrecr = odbc_result($rs,'rec_redim');
				$htmor = odbc_result($rs,'tmo_redim');
				$hhcr = odbc_result($rs,'hc_redim');
				
				$hrecref_str = $hrecref_str.$hrecr.",";
				$htmoref_srt = $htmoref_srt.$htmor.",";
				$hhcref_str = $hhcref_str.$hhcr.",";
			}
			$hrecref_str = substr($hrecref_str,0,strlen($hrecref_str)-1)."]";
			$htmoref_srt = substr($htmoref_srt,0,strlen($htmoref_srt)-1)."]";
			$hhcref_str = substr($hhcref_str,0,strlen($hhcref_str)-1)."]";
			
				$hccolor = "#ff6161";
				$tmocolor = "#ffff61";
				$reccolor = "#61ffb3";
			
			//matriz de gravidade // A = Gm|r� 
			
			$matriz1 = "SELECT abd, color FROM performance.proc_gravidade(".$_GET['id_skill'].") WHERE abd >= '$mrecdd' "; //aqui temos que inserir o valor
			
			$rs = odbc_exec($conn,$matriz1);
			odbc_fetch_row($rs);

			$gmabdcor = odbc_result($rs,'color');
			
			$matriz2 = "SELECT abd, color FROM performance.proc_gravidade(".$_GET['id_skill'].") WHERE abd >= '$drecdd' "; //aqui temos que inserir o valor
			
			$rs = odbc_exec($conn,$matriz2);
			odbc_fetch_row($rs);
			$gdabdcor = odbc_result($rs,'color');
			
			
			$matriz3 = "SELECT abd, color FROM performance.proc_gravidade(".$_GET['id_skill'].") WHERE abd >= '$hrechh' "; //aqui temos que inserir o valor
			
			$rs = odbc_exec($conn,$matriz3);
			odbc_fetch_row($rs);

			$ghabdcor = odbc_result($rs,'color');

?>
<script type="text/javascript">
$(function () {

  Highcharts.setOptions({
            colors:['<? echo $cor ?>']       
    });
   // configura o tema, gradiente e linhas do grid

	legend: {
			itemStyle: {
			   fontSize: '10px'
			}
	    }
      title: {
          text: ''
		}

    xAxis: {
    
    }
		plotOptions: {
		series: {
			animation: {
				duration: 2000
			}
		}
	}
    yAxis: {
		lineWidth: 1,
		gridLineDashStyle: 'dot'
		  title: {
		  text: '',
			style: {
			  color: '#333',
			}
		}
    }
	
    labels: {
		style: {
			color: '#99b'
		}
    }
	
	});
  var options = {
  
    chart: {
      zoomType: 'x'
	},
    xAxis: {
      type: ''
    }
  };

  // cria os graficos, aqui comeca a magia (metal espadinha)
		var chart1Options = { <?php graf_wr($gmabdcor,$mabdd,$mperiodo_str,$mabd_str,$mabdp_str,$gra,'abdm') ?> };
		chart1Options = jQuery.extend(true, {}, options, chart1Options);
		var chart1 = new Highcharts.Chart(chart1Options);

		var chart2Options = { <?php graf_wr('#3f75a2',0,$mperiodo_str,$mns60r_str,$mns60r_str,$gra,'nsm') ?>};
		chart2Options = jQuery.extend(true, {}, options, chart2Options);
		var chart2 = new Highcharts.Chart(chart2Options);

		var chart3Options = {<?php graf_wr($mesteira_cor["rec"][1],$mesteira_cor["rec"][0],$mperiodo_str,$mrecr_str,$mrecp_str,$gra,'recem') ?>};
		chart3Options = jQuery.extend(true, {}, options, chart3Options);
		var chart3 = new Highcharts.Chart(chart3Options);

		var chart4Options = {<?php graf_wr($mesteira_cor["tmo"][1],$mesteira_cor["tmo"][0],$mperiodo_str,$mtmor_str,$mtmop_str,$gra,'tmom') ?>};
		chart4Options = jQuery.extend(true, {}, options, chart4Options);
		var chart4 = new Highcharts.Chart(chart4Options);

		var chart5Options = {<?php graf_wr($mesteira_cor["hc"][1],$mesteira_cor["hc"][0],$mperiodo_str,$mhcr_str,$mhcp_str,$gra,'hcm') ?>};
		chart5Options = jQuery.extend(true, {}, options, chart5Options);
		var chart5 = new Highcharts.Chart(chart5Options);

		var chart6Options = {<?php graf_wr($gdabdcor,$dabdd,$dperiodo_str,$dabd_str,$dabdp_str,$gra,'abd') ?>};
		chart6Options = jQuery.extend(true, {}, options, chart6Options);
		var chart6 = new Highcharts.Chart(chart6Options);
		  
		var chart7Options = {<?php graf_wr('#3f75a2',0,$dperiodo_str,$dns60r_str,$dns60r_str,$gra,'ns') ?>};
		chart7Options = jQuery.extend(true, {}, options, chart7Options);
		var chart7 = new Highcharts.Chart(chart7Options);
		  
		var chart8Options = {<?php graf_r($desteira_cor["rec"][1],$desteira_cor["rec"][0],$dperiodo_str,$drecr_str,$drecp_str,$drecref_str,$gra,'rece')?> };
		chart8Options = jQuery.extend(true, {}, options, chart8Options);
		var chart8 = new Highcharts.Chart(chart8Options);

		var chart9Options = {<?php graf_r($desteira_cor["tmo"][1],$desteira_cor["tmo"][0],$dperiodo_str,$dtmor_str,$dtmop_str,$dtmoref_srt,$gra,'tmo')?>};
		chart9Options = jQuery.extend(true, {}, options, chart9Options);
		var chart9 = new Highcharts.Chart(chart9Options);

		var chart10Options = {<?php graf_r($desteira_cor["hc"][1],$desteira_cor["hc"][0],$dperiodo_str,$dhcr_str,$dhcp_str,$dhcref_str,$gra,'hc')?>};
		chart10Options = jQuery.extend(true, {}, options, chart10Options);
		var chart10 = new Highcharts.Chart(chart10Options);
		  
		var chart11Options = {<?php graf_wr($ghabdcor,$habdd,$hperiodo_str,$habd_str,$habdp_str,$gra,'abdh') ?>};
		chart11Options = jQuery.extend(true, {}, options, chart11Options);
		var chart11 = new Highcharts.Chart(chart11Options);
		  
		var chart12Options = {<?php graf_wr('#3f75a2',0,$hperiodo_str,$hns60r_str,$hns60r_str,$gra,'nsh') ?>};
		chart12Options = jQuery.extend(true, {}, options, chart12Options);
		var chart12 = new Highcharts.Chart(chart12Options);
		  
		var chart13Options = {<?php graf_r($hesteira_cor["rec"][1],$hesteira_cor["rec"][0],$hperiodo_str,$hrecr_str,$hrecp_str,$hrecref_str,$gra,'receh')?>};
		chart13Options = jQuery.extend(true, {}, options, chart13Options);
		var chart13 = new Highcharts.Chart(chart13Options);

		var chart14Options = {<?php graf_r($hesteira_cor["tmo"][1],$hesteira_cor["tmo"][0],$hperiodo_str,$htmor_str,$htmop_str,$htmoref_srt,$gra,'tmoh')?>};
		chart14Options = jQuery.extend(true, {}, options, chart14Options);
		var chart14 = new Highcharts.Chart(chart14Options);

		var chart15Options = {<?php graf_r($hesteira_cor["hc"][1],$hesteira_cor["hc"][0],$hperiodo_str,$hhcr_str,$hhcp_str,$hhcref_str,$gra,'hch')?>};
		chart15Options = jQuery.extend(true, {}, options, chart15Options);
		var chart15 = new Highcharts.Chart(chart15Options);
});

</script>
<form id="filtro" method="get" id="form" action="index3.php" onchange="filtro_troca2();" style="margin-top:0%;">
					<meta charset="utf-8">

	<center>
	<?php

			$where = '';
			$ind = array('diretoria','marca','npc','gerente','consultor','assunto_1','assunto_2');
			$in = array('diretoria','marca','npc','gerente','consultor','as1','as2');

				for ($i=0; $i < sizeof($ind) ; $i++) { 
					echo "<select name='$ind[$i]' id='$ind[$i]' style='width: 110px;margin-bottom: 3%;margin-right: 1%;''><option value='0'>$ind[$i]</option>";

							$d = date('Y-m-d');
							$conn = odbc_connect('MISPG','','');
							$sql = "SELECT DISTINCT $ind[$i], id_$in[$i] FROM performance.proc_performance_filtro('$where',0,'$d') ORDER BY $ind[$i]";
							$rs = odbc_exec($conn, $sql);
							if ($rs)
							  {
							while (odbc_fetch_row($rs)) 
							{
							  $valor = odbc_result ($rs,"id_$in[$i]");
							  $nome = odbc_result($rs,"$ind[$i]");
								echo "<option value='$valor'>$nome</option>\n";
							  }
							}

					echo "</select>";
				}

		?><input type='submit' value='Consulta'>
	</center>
</form>

<div style="
    position: relative;
    width: 100%;
    height: 9%;
">						
<div class="bt" id="tag" style="width: 20%;-webkit-transform: rotate(0deg);position: relative;  z-index: 1;  /* -webkit-transform: rotate(270deg); */    float: left;top: 0;left: 8%;margin-right: 19%;"><a href="#a1">Mensal</a></div><div class="bt" id="tag" style="width: 20%;-webkit-transform: rotate(0deg);position: relative;  z-index: 1;  /* -webkit-transform: rotate(270deg); */    float: left;top: 0;left: 0%;margin-right: 9%;" ><a href="#a2">Di�rio</a></div><div class="bt" id="tag" style="width: 20%;-webkit-transform: rotate(0deg);position: relative;  z-index: 1;  /* -webkit-transform: rotate(270deg); */    float: left;top: 0;margin-right: 0;"><a href="#a3">Intra-di�rio</a></div></div>
	<?php

		$ind = array("abd","ns","rece","tmo","hc");
		$est = array ("eabd","ens","erece","etmo","ehc");
		$tag = array("Abandono","NS60","Recebidas","TMO","HC");
		$vis = array("m","","h");

		for($i = 0; $i<sizeof($ind);$i++){
			$local = $ind[$i];
			$local2 = $tag[$i];
			$local3 = $est[$i];
			echo "<div id='res$local' style='width:100%;position:relative;z-index: 1;'>
					<div id='res$local3' style='width:100%;position:relative;z-index: 1;'>
						<div class='bt' id='tag' style='width:5%;'>$local2</div>";
						for($j = 0 ; $j<sizeof($vis);$j++){
							$v = $vis[$j];
							echo "<div id='$local$v' class='graf'></div>";
						}
			echo "</div></br>";
		}
	?>
<script type="text/javascript" charset="utf-8">
  $(function() {

    function log_modal_event(event, modal) {
      if(typeof console != 'undefined' && console.log) console.log("[event] " + event.type);
    };
	  $('#more').click(function() {
      $(this).parent().after($(this).parent().next().clone());
      $.modal.resize();
      return false;
    });
    $('a[href="#a1"]').click(function(event) {
      event.preventDefault();
      $(this).modal({
        escapeClose: false,
        clickClose: false,
        showClose: true,
		fadeDuration: 250,
		fadeDelay: 1.5
      });
    });
	$('a[href="#a2"]').click(function(event) {
      event.preventDefault();
      $(this).modal({
        escapeClose: false,
        clickClose: false,
        showClose: true,
		fadeDuration: 250,
		fadeDelay: 1.5
      });
    });
	$('a[href="#a3"]').click(function(event) {
      event.preventDefault();
      $(this).modal({
        escapeClose: false,
        clickClose: false,
        showClose: true,
		fadeDuration: 250,
		fadeDelay: 1.5
      });
    });
  });
</script>
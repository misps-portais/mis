	<?php
		function graph($div,$titulo,$label,$dir,$dem){

			echo "
				$('#$div').highcharts({
					chart: {
					    plotBackgroundColor: null,
					    plotBorderWidth: 0,
					    plotShadow: false
					},
					title: {
					    text: '$titulo',
					    align: 'center',
					    verticalAlign: 'middle',
					    y: -20
					},
					tooltip: {
					    pointFormat: '{series.name}: <b>{point.y:f}</b>'
					},
					plotOptions: {
					    pie: {
						  dataLabels: {
	                        enabled: false
	                    },
	                    showInLegend: true,
						startAngle: -90,
						endAngle: 90,
						center: ['50%', '50%']
					    }
					},
					series: [{
					    type: 'pie',
					    name: '$label',
					    innerSize: '85%',
					    data: [
						{name:'Diretoria<br>Atd',   y: $dir,color:'#46b4af'},
						{name:'',   y: $dem,color:'#ffffff'}
					    ]
					},{
					    type: 'pie',
					    name: '$label',
					    innerSize: '95%',
					    data: [
						{name:'Demais<br>Diretorias',  y: $dem,color:'#3498db'},
						{name:'',   y: $dir,color:'#ffffff'}
					    ]
					}],
				    });
			";
		};

		function graph_por($div,$titulo,$label,$dir,$dem){

			$dir_com = 100 - $dir;
			$dem_com = 100 - $dem;
			echo "
			    $('#$div').highcharts({
				chart: {
				    plotBackgroundColor: null,
				    plotBorderWidth: 0,
				    plotShadow: false
				},
				title: {
				    text: '$titulo',
				    align: 'center',
				    verticalAlign: 'middle',
				    y: -20
				},
				tooltip: {
				    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
				},
				plotOptions: {
				    pie: {
					  dataLabels: {
                        enabled: false
                    },
                    showInLegend: true,
					startAngle: -90,
					endAngle: 90,
					center: ['50%', '50%']
				    }
				},
				series: [{
				    type: 'pie',
				    name: '$label',
				    innerSize: '85%',
				    data: [
					{name:'Diretoria<br>Atd',   y: $dir,color:'#46b4af'},
					{name:'',   y: $dir_com,color:'#ffffff'}
				    ]
				},{
				    type: 'pie',
				    name: '$label',
				    innerSize: '95%',
				    data: [
					{name:'Demais<br>Diretorias',  y: $dem,color:'#3498db'},
					{name:'',   y: $dem_com,color:'#ffffff'}
				    ]
				}]
			    });
			";
		};

		$conn = odbc_connect('MISPG','','');
			$sql = "SELECT 
			tdir.diretoria as diretoria,
			SUM(ta.callsoffered)::text AS recebidas,
			CASE SUM(ta.callsoffered)
					WHEN 0 THEN
						0::text
					ELSE
						ROUND(CAST((SUM(ta.abncalls)-SUM(ta.abncalls1)) AS numeric)/CAST(SUM(ta.callsoffered) AS numeric)*100,2)::text
				END AS abd_per,
				CASE sum(ta.callsoffered)
					WHEN 0 THEN 
						0::text
					ELSE 
						ROUND(CAST(sum(ta.acdcalls-ta.acdcalls10-ta.acdcalls9) AS numeric) / CAST(sum(ta.callsoffered) as numeric)*100,2)::text
				END as ns60
			FROM performance.tbl_performance_dia_2013 as ta
				INNER JOIN skills.tbl_skill tsk ON
				tsk.dac = ta.dac and tsk.servidor = ta.servidor and tsk.skill = ta.skill and tsk.data = ta.row_date
				INNER JOIN skills.tbl_diretoria tdir ON
				tdir.id = tsk.diretoria
			WHERE row_date = current_date
			GROUP BY ta.row_date, tdir.diretoria,tsk.diretoria
			ORDER BY tsk.diretoria,tdir.diretoria,ta.row_date;";
		$rs = odbc_exec($conn, $sql);

		$i = 0;

		while(odbc_fetch_row($rs)){

			$resultado[$i][0] = odbc_result($rs,'recebidas');
			$resultado[$i][1] = odbc_result($rs,'abd_per');
			$resultado[$i][2] = odbc_result($rs,'ns60');
			$i++;	

		}

		$sql = "SELECT to_char(max(data_atualiza) + '00:30:00','DD-MM-YYYY HH24:MI:ss') as data_atualiza FROM avaya.tbl_avaya_extracao_info";
		$rs = odbc_exec($conn, $sql);

		$data_atualiza = odbc_result($rs, 'data_atualiza'); 

	?>
	<meta charset="utf-8">
<title>Performance</title>
<html>
	<head>
		<link href='http://fonts.googleapis.com/css?family=Share+Tech|Noto+Sans|Open+Sans|Sintony' rel='stylesheet' type='text/css'><link href='http://fonts.googleapis.com/css?family=Share+Tech|Noto+Sans|Open+Sans|Sintony' rel='stylesheet' type='text/css'>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
		<script src="highcharts.js"></script>
		<script>
			$(function () {<?php graph('rec_gra','Ligações<br>Recebidas','Recebidas',$resultado[0][0],$resultado[1][0]) ?>});

			$(function () {<?php graph_por('abd_gra','Ligações<br>Abandonadas','% Abandono',$resultado[0][1],$resultado[1][1]) ?>});

			$(function () {<?php graph_por('ns_gra','Nível de<br>Serviço','NS 60',$resultado[0][2],$resultado[1][2]) ?>});

		</script>
	</head>
	<body style="height:100%;width:100%;background-color:#ffffff;overflow: hidden;">
		<div id="filtros" style="width:96%;height:15%;margin-left:2%;background-color:#F8FBFC">
			<p style="font-family: 'Sintony';color:#274b6d;font-size:2em;margin:0% 0% 0% 4%;padding-top:0.75%">Relatório de Performance</p>
			<p style="font-family: 'Sintony';color:#274b6d;font-size:.8em;margin:0% 0% 0% 4%;">Atualizado até <?php echo $data_atualiza; ?></p>
					<form method="get" id="form" onchange="form.submit(this.value);">
<center>
			<select name="diretoria" id="diretoria" style="width: 110px;
			margin-bottom: 3%;
			margin-right: 0%;">
			
                <option>Diretoria</option>
			  
				<?php
					$sql = "SELECT DISTINCT id, diretoria FROM skills.tbl_diretoria";
					$rs = odbc_exec($conn, $sql);
					if ($rs)
					  {
					while (odbc_fetch_row($rs)) 
					{
					  $diretoria = odbc_result ($rs,"id");
					  $assunto = odbc_result($rs,"diretoria");
					  if($diretoria == $_GET['diretoria']){
						echo "<option value='$diretoria' selected>$assunto</option>\n";
					  }
					  else{
						echo "<option value='$diretoria'>$assunto</option>\n";
					  }
					}}
					
				?>
			</select>

			<select name="marca" id="marca" style="width: 120px;
			margin-bottom: 2%;
			margin-right: 0%;">
                <option >Marca</option>
				<?php
					$sql = "SELECT DISTINCT marca, id_marca FROM performance.proc_performance_filtro('and diretoria = ".$_GET['diretoria']."',".$_GET['diretoria'].",'$d')";
					$rs = odbc_exec($conn, $sql);
					if ($rs)
					  {
					while (odbc_fetch_row($rs))
					{
					  $marca = odbc_result ($rs,"id_marca");
					  $assunto2 = odbc_result($rs,"marca");
					  if($marca == $_GET['marca']){
						echo "<option value='$marca' selected>$assunto2</option>\n";
					  }
					  else{
						echo "<option value='$marca'>$assunto2</option>\n";
					  }
					}
					odbc_close($conn); }
				?>
			</select>
			
			<select name="npc" id="npc" style="width: 120px;
			margin-bottom: 2%;
			margin-right: 0%;">
                <option >NPC</option>
			  
			  <?php
					$sql = "SELECT DISTINCT npc, id_npc FROM performance.proc_performance_filtro('marca',".$_GET['marca'].",'$d')";
					$rs = odbc_exec($conn, $sql);
					if ($rs)
					  {
					while (odbc_fetch_row($rs))
					{
					  $npc = odbc_result ($rs,"id_npc");
					  $assunto3 = odbc_result($rs,"npc");
					  if($npc == $_GET['npc']){
						echo "<option value='$npc' selected>$assunto3</option>\n";
					  }
					  else{
						echo "<option value='$npc'>$assunto3</option>\n";
					  }
					}
					odbc_close($conn); }
				?>
			</select>
			
			<select name="gerente" id="gerente"style="width: 120px;
			margin-bottom: 2%;
			margin-right: 0%;">
                <option >Gerente</option>
			  
			  <?php
					$sql = "SELECT DISTINCT gerente, id_gerente FROM performance.proc_performance_filtro('npc',".$_GET['npc'].",'$d')";
					$rs = odbc_exec($conn, $sql);
					if ($rs)
					  {
					while (odbc_fetch_row($rs))
					{
					  $gerente = odbc_result ($rs,"id_gerente");
					  $assunto4 = odbc_result($rs,"gerente");
					  if($gerente == $_GET['gerente']){
						echo "<option value='$gerente' selected>$assunto4</option>\n";
					  }
					  else{
						echo "<option value='$gerente'>$assunto4</option>\n";
					  }
					}
					odbc_close($conn); }
				?>
			</select>
			
			<select name="consultor" id="consultor" style="width: 120px;
			margin-bottom: 2%;
			margin-right: 0%;">
                <option >Consultor</option>
			  
			  <?php
					$sql = "SELECT DISTINCT consultor, id_consultor FROM performance.proc_performance_filtro('gerente',".$_GET['gerente'].",'$d')";
					$rs = odbc_exec($conn, $sql);
					if ($rs)
					  {
					while (odbc_fetch_row($rs))
					{
					  $consultor = odbc_result ($rs,"id_consultor");
					  $assunto5 = odbc_result($rs,"consultor");
					  if($consultor == $_GET['consultor']){
						echo "<option value='$consultor' selected>$assunto5</option>\n";
					  }
					  else{
						echo "<option value='$consultor'>$assunto5</option>\n";
					  }
					}
					odbc_close($conn); }
				?>
			</select>
			
			<select name="assunto_1" id="assunto_1" style="width: 120px;
			margin-bottom: 2%;
			margin-right: 0%;">
                <option >Assunto 1</option>
			  
			  <?php
					$sql = "SELECT DISTINCT assunto_1, id_as1 FROM performance.proc_performance_filtro('consultor',".$_GET['consultor'].",'$d')";
					$rs = odbc_exec($conn, $sql);
					if ($rs)
					  {
					while (odbc_fetch_row($rs))
					{
					  $assunto_1 = odbc_result ($rs,"id_as1");
					  $assunto6 = odbc_result($rs,"assunto_1");
					  if($assunto_1 == $_GET['assunto_1']){
						echo "<option value='$assunto_1' selected>$assunto6</option>\n";
					  }
					  else{
						echo "<option value='$assunto_1'>$assunto6</option>\n";
					  }
					}
					odbc_close($conn); }
				?>
			</select>
			
			<select name="assunto_2" id="assunto_2" style="width: 120px;
			margin-bottom: 2%;
			margin-right: 0%;">
                <option >Assunto 2</option>
			  
			  <?php
					$sql = "SELECT DISTINCT assunto_2, id_as2 FROM performance.proc_performance_filtro('as1',".$_GET['assunto_1'].",'$d')";
					$rs = odbc_exec($conn, $sql);
					if ($rs)
					  {
					while (odbc_fetch_row($rs))
					{
					  $assunto_2 = odbc_result ($rs,"id_as2");
					  $assunto7 = odbc_result($rs,"assunto_2");
					  if($assunto_2 == $_GET['assunto_2']){
						echo "<option value='$assunto_2' selected>$assunto7</option>\n";
					  }
					  else{
						echo "<option value='$assunto_2'>$assunto7</option>\n";
					  }
					}
										
					odbc_close($conn); }
				?>
			</select>
			
			
			<select name="skill" id="skill" style="width: 120px;
			margin-bottom: 2%;
			margin-right: 0%;">
                <option >Skill</option>
			  
			  <?php
					$sql = "SELECT DISTINCT skill FROM performance.proc_performance_filtro('as2',".$_GET['assunto_2'].",'$d')";
					$rs = odbc_exec($conn, $sql);
					if ($rs)
					  {
					while (odbc_fetch_row($rs))
					{
					  $skill = odbc_result ($rs,"skill");
					  $assunto9 = odbc_result($rs,"skill");
					  if($skill == $_GET['skill']){
						echo "<option value='$skill' selected>$skill</option>\n";
					  }
					  else{
						echo "<option value='$skill'>$skill</option>\n";
					  }
					}
					odbc_close($conn); }
				?>
			</select>

		</center>
		</form>
		</div>

		<div	id="indicadores" style="width:96%;height:75%;margin-left:2%;margin-top:1%;background-color:#F8FBFC;position:absolute">
			<div id="rec" style="height:150%;width:30%;position:relative;float:left;margin-top:1%;margin-left:2.5%">
				<div id="rec_gra" style="width:100%;height:60%;margin:5%;box-shadow: 0px 4px 10px #888888;">
				</div>

			</div>
			<div id="abd" style="height:150%;width:30%;position:relative;float:left;margin-top:1%;margin-left:2.5%">
				<div id="abd_gra" style="width:100%;height:60%;margin:5%;box-shadow: 0px 4px 10px #888888;">
				</div>
			</div>
			<div id="ns" style="height:150%;width:30%;position:relative;float:left;margin-top:1%;margin-left:2.5%">
				<div id="ns_gra" style="width:100%;height:60%;margin:5%;box-shadow: 0px 4px 10px #888888;">
				</div>		
			</div>
		</div>
	</body>
</html>

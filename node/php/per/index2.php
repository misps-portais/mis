<html>
<style type="text/css">
	#menu{
		position:relative;
		width:783px;
		height:57px;
		z-index:2;
		left: 8px;
		top: 0px;
	}
	#tag{
		position: relative;
		z-index:2;
		-webkit-transform: rotate(270deg);
		float:left;
		top: 80;
	}
	.graf{
		width: 30%;
		height: 250px;
		margin: 0 auto;
		position: relative;
		float: left;
	}
</style>
<body>
	<title>Performance - Filas</title>
    <script src="js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="js/highcharts.js"></script>
	<script src="js/jquery.modal.js" type="text/javascript" charset="utf-8"></script>
	<link href="perf.css" rel="stylesheet" media="screen">
	<link rel="stylesheet" href="js/jquery.modal.css" type="text/css" media="screen" />
	

<?php
$grad = "	Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function(color) {
		    return {
		        radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
		        stops: [
		            [0, color],
		            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
		        ]
		    };
		});";
		
	function sinalizador($cor,$desv){

		echo "
	colors: ['$cor'],
    chart: {
      renderTo: 'tmom'
    },
       xAxis: [{
                categories: [$mperiodo_str,
				labels: {
                rotation: 300
            }
            }],
			

		series:[{ 
			name: '$desv',
			type: 'pie',
			data:[100],
            borderWidth: 0,
						center: [310, 195],
                size: 45,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
					fontSize: '12px',
					fontWeight:'bold',
					formatter: function() {
                        return this.series.name + '%';
                    },
                    color: 'black',
                    distance: -22
				}
        }]";
	}

	error_reporting(0);
	$gra = $_GET["graf"];
	$conn = odbc_connect('MISPG','','');
	$cor = array('#61ffb3','#ffff61','#ff6161');
	
		if (empty($gra)){ 
		  $gra = "area";
		}
		
		$mes = $_GET['mes'];
		if(isset($_GET['dia'])){
			$dia = $_GET['dia'];
		}
		else{
			$dia = date('Y-m-d');
		}
		$id_skill = $_GET['id_skill']; 
		$assunto = $_GET['assunto_2'];

		$blevisky = array("Jan"=>1,"Fev"=>2,"Mar"=>3,"Abr"=>4,"Mai"=>5,"Jun"=>6,"Jul"=>7,"Ago"=>8,"Set"=>9,"Out"=>10,"Nov"=>11,"Dez"=>12);

		$mes = $blevisky[$mes];
			
			//La�o MENSAL de realizado e forecast com desvios
			$sqlm = "SELECT * FROM performance.proc_performance_tela1(1,".$_GET['id_skill'].",11,'2013-11-07')";
			$rs = odbc_exec($conn, $sqlm);
				odbc_fetch_row($rs);
			
				$mrecd = odbc_result($rs,'rec_desv');
				$mabdd = odbc_result($rs,'abd_desv');
				$mtmod = odbc_result($rs,'tmo_desv');
				$mhcd = odbc_result($rs,'hc_desv');
				$mabdpp = odbc_result($rs,'abd_perc');
			
				$mesteira = array('rec'=>(float)$mrecd,'tmo'=>(float)$mtmod,'hc'=>(float)$mhcd);
				asort($mesteira); 
				$i = 0;

				while(current($mesteira)){

					$mesteira_cor[key($mesteira)][0] = current($mesteira);
					$mesteira_cor[key($mesteira)][1] = $cor[$i]; 
					next($mesteira);
					$i++;

				}
				$branco = "#FFFFFF";
				if (empty($mesteira_cor)){ 
				  $mesteira_cor = $branco;
				}
	
			while(odbc_fetch_row($rs)){
			
				$mperiodo = odbc_result($rs,'periodo');
				$mabdr = odbc_result($rs,'abd_perc');
				$mabdp = odbc_result($rs,'abd_dim');
				$mns60r = odbc_result($rs,'ns60');
				$mrecr = odbc_result($rs,'rec');
				$mrecp = odbc_result($rs,'rec_dim');
				$mtmor = odbc_result($rs,'tmo');
				$mtmop = odbc_result($rs,'tmo_dim');
				$mhcr = odbc_result($rs,'hc');
				$mhcp = odbc_result($rs,'hc_dim');
				
				
				$mperiodo_str = $mperiodo_str."'".$mperiodo."'".",";
				$mabd_str = $mabd_str.$mabdr.",";
				$mabdp_str = $mabdp_str.$mabdp.",";
				$mns60r_str = $mns60r_str.$mns60r.",";
				$mrecr_str = $mrecr_str.$mrecr.",";
				$mrecp_str = $mrecp_str.$mrecp.",";
				$mtmor_str = $mtmor_str.$mtmor.",";
				$mtmop_str = $mtmop_str.$mtmop.",";
				$mhcr_str = $mhcr_str.$mhcr.",";
				$mhcp_str = $mhcp_str.$mhcp.",";		
			}
			
			$mperiodo_str = substr($mperiodo_str,0,strlen($mperiodo_str)-1)."]";
			$mabd_str = substr($mabd_str,0,strlen($mabd_str)-1)."]";
			$mabdp_str = substr($mabdp_str,0,strlen($mabdp_str)-1)."]";
			$mns60r_str = substr($mns60r_str,0,strlen($mns60r_str)-1)."]";
			$mrecr_str = substr($mrecr_str,0,strlen($mrecr_str)-1)."]";
			$mrecp_str = substr($mrecp_str,0,strlen($mrecp_str)-1)."]";
			$mtmor_str = substr($mtmor_str,0,strlen($mtmor_str)-1)."]";
			$mtmop_str = substr($mtmop_str,0,strlen($mtmop_str)-1)."]";
			$mhcr_str = substr($mhcr_str,0,strlen($mhcr_str)-1)."]";
			$mhcp_str = substr($mhcp_str,0,strlen($mhcp_str)-1)."]";
			
			//POG para coletar %desv
			$sqlmdes = "SELECT * FROM performance.proc_performance_tela1(1,".$_GET['id_skill'].",".$mes.",'2013-11-07')";
			$rs = odbc_exec($conn, $sqlmdes);
				odbc_fetch_row($rs);
			
				$hrechh = odbc_result($rs,'abd_desv');
			
			$sqlmdes = "SELECT * FROM performance.proc_performance_tela1(2,".$_GET['id_skill'].",".$mes.",'2013-11-07')";
			$rs = odbc_exec($conn, $sqlmdes);
				odbc_fetch_row($rs);
			
				$mrecdd = odbc_result($rs,'abd_desv');
				
			//POG para coletar %desv
			$sqlmdes = "SELECT * FROM performance.proc_performance_tela1(3,".$_GET['id_skill'].",".$mes.",'".$dia."')";
			$rs = odbc_exec($conn, $sqlmdes);
				odbc_fetch_row($rs);
			
				$drecdd = odbc_result($rs,'abd_desv');
			
			//La�o DIARIO de realizado e forecast com desvios
			$sqld = "SELECT * FROM performance.proc_performance_tela1(2,".$_GET['id_skill'].",".$mes.",'2013-10-20')";
			$rs = odbc_exec($conn, $sqld);

				odbc_fetch_row($rs);
			
				$drecd = odbc_result($rs,'rec_desv');
				$dabdd = odbc_result($rs,'abd_desv');
				$dtmod = odbc_result($rs,'tmo_desv');
				$dhcd = odbc_result($rs,'hc_desv');

				$desteira = array('rec'=>$drecd,'tmo'=>$dtmod,'hc'=>$dhcd);
				asort($desteira); 
				$i = 0;

				while(current($desteira)){

					$desteira_cor[key($desteira)][0] = current($desteira);
					$desteira_cor[key($desteira)][1] = $cor[$i]; 
					next($desteira);
					$i++;

				}

			while(odbc_fetch_row($rs)){
			
				$dperiodo = odbc_result($rs,'periodo');
				$dabdr = odbc_result($rs,'abd_perc');
				$dabdp = odbc_result($rs,'abd_dim');
				$dns60r = odbc_result($rs,'ns60');
				$drecr = odbc_result($rs,'rec');
				$drecp = odbc_result($rs,'rec_dim');
				$dtmor = odbc_result($rs,'tmo');
				$dtmop = odbc_result($rs,'tmo_dim');
				$dhcr = odbc_result($rs,'hc');
				$dhcp = odbc_result($rs,'hc_dim');
				
				$dperiodo_str = $dperiodo_str."'".$dperiodo."'".",";
				$dabd_str = $dabd_str.$dabdr.",";
				$dabdp_str = $dabdp_str.$dabdp.",";
				$dabdd_str = $dabdd_str.$dabdd.",";
				$dns60r_str = $dns60r_str.$dns60r.",";
				$drecr_str = $drecr_str.$drecr.",";
				$drecp_str = $drecp_str.$drecp.",";
				$drecd_str = $drecd_str.$drecd.",";
				$dtmor_str = $dtmor_str.$dtmor.",";
				$dtmop_str = $dtmop_str.$dtmop.",";
				$dtmod_str = $dtmod_str.$dtmod.",";
				$dhcr_str = $dhcr_str.$dhcr.",";
				$dhcp_str = $dhcp_str.$dhcp.",";
				$dhcd_str = $dhcd_str.$dhcd.",";
			}
			
			$dperiodo_str = substr($dperiodo_str,0,strlen($dperiodo_str)-1)."]";
			$dabd_str = substr($dabd_str,0,strlen($dabd_str)-1)."]";
			$dabdp_str = substr($dabdp_str,0,strlen($dabdp_str)-1)."]";
			$dabdd_str = substr($dabdd_str,0,strlen($dabdd_str)-1)."]";
			$dns60r_str = substr($dns60r_str,0,strlen($dns60r_str)-1)."]";
			$drecr_str = substr($drecr_str,0,strlen($drecr_str)-1)."]";
			$drecp_str = substr($drecp_str,0,strlen($drecp_str)-1)."]";
			$drecd_str = substr($drecd_str,0,strlen($drecd_str)-1)."]";
			$dtmor_str = substr($dtmor_str,0,strlen($dtmor_str)-1)."]";
			$dtmop_str = substr($dtmop_str,0,strlen($dtmop_str)-1)."]";
			$dtmod_str = substr($dtmod_str,0,strlen($dtmod_str)-1)."]";
			$dhcr_str = substr($dhcr_str,0,strlen($dhcr_str)-1)."]";
			$dhcp_str = substr($dhcp_str,0,strlen($dhcp_str)-1)."]";
			$dhcd_str = substr($dhcd_str,0,strlen($dhcd_str)-1)."]";

			
			//La�o INTRA-DIA de realizado e forecast com desvios
			$sqlh = "SELECT * FROM performance.proc_performance_tela1(3,".$_GET['id_skill'].",".$mes.",'".$dia."')";
			$rs = odbc_exec($conn, $sqlh);
			odbc_fetch_row($rs);
			
				$hrecd = odbc_result($rs,'rec_desv');
				$habdd = odbc_result($rs,'abd_desv');
				$htmod = odbc_result($rs,'tmo_desv');
				$hhcd = odbc_result($rs,'hc_desv');
				
				$hesteira = array('rec'=>$hrecd,'tmo'=>$htmod,'hc'=>$hhcd);
				
				asort($hesteira); 
				$i = 0;

				while(current($hesteira)){

					$hesteira_cor[key($hesteira)][0] = current($hesteira);
					$hesteira_cor[key($hesteira)][1] = $cor[$i]; 
					next($hesteira);
					$i++;

				}
	

			while(odbc_fetch_row($rs)){
			
				$hperiodo = odbc_result($rs,'periodo');
				$habdr = odbc_result($rs,'abd_perc');
				$habdp = odbc_result($rs,'abd_dim');
				$hns60r = odbc_result($rs,'ns60');
				$hrecr = odbc_result($rs,'rec');
				$hrecp = odbc_result($rs,'rec_dim');
				$htmor = odbc_result($rs,'tmo');
				$htmop = odbc_result($rs,'tmo_dim');
				$hhcr = odbc_result($rs,'hc');
				$hhcp = odbc_result($rs,'hc_dim');
				
				$hperiodo_str = $hperiodo_str."'".$hperiodo."'".",";
				$habd_str = $habd_str.$habdr.",";
				$habdp_str = $habdp_str.$habdp.",";
				$habdd_str = $habdd_str.$habdd.",";
				$hns60r_str = $hns60r_str.$hns60r.",";
				$hrecr_str = $hrecr_str.$hrecr.",";
				$hrecp_str = $hrecp_str.$hrecp.",";
				$hrecd_str = $hrecd_str.$hrecd.",";
				$htmor_str = $htmor_str.$htmor.",";
				$htmop_str = $htmop_str.$htmop.",";
				$htmod_str = $htmod_str.$htmod.",";
				$hhcr_str = $hhcr_str.$hhcr.",";
				$hhcp_str = $hhcp_str.$hhcp.",";
				$hhcd_str = $hhcd_str.$hhcd.",";
			}
			
			$hperiodo_str = substr($hperiodo_str,0,strlen($hperiodo_str)-1)."]";
			$habd_str = substr($habd_str,0,strlen($habd_str)-1)."]";
			$habdp_str = substr($habdp_str,0,strlen($habdp_str)-1)."]";
			$habdd_str = substr($habdd_str,0,strlen($habdd_str)-1)."]";
			$hns60r_str = substr($hns60r_str,0,strlen($hns60r_str)-1)."]";
			$hrecr_str = substr($hrecr_str,0,strlen($hrecr_str)-1)."]";
			$hrecp_str = substr($hrecp_str,0,strlen($hrecp_str)-1)."]";
			$hrecd_str = substr($hrecd_str,0,strlen($hrecd_str)-1)."]";
			$htmor_str = substr($htmor_str,0,strlen($htmor_str)-1)."]";
			$htmop_str = substr($htmop_str,0,strlen($htmop_str)-1)."]";
			$htmod_str = substr($htmod_str,0,strlen($htmod_str)-1)."]";
			$hhcr_str = substr($hhcr_str,0,strlen($hhcr_str)-1)."]";
			$hhcp_str = substr($hhcp_str,0,strlen($hhcp_str)-1)."]";
			$hhcd_str = substr($hhcd_str,0,strlen($hhcd_str)-1)."]";
				
			// ReforeCast - se est� lendo isso a responsa agora � sua!
			$sqlrefm = "SELECT * FROM forecast.proc_performance_tela1_reforecast(2,".$_GET['id_skill'].",".$mes.",'2013-10-01')";
			$rs = odbc_exec($conn, $sqlrefm);
			while(odbc_fetch_row($rs)){
			
				$drecr = odbc_result($rs,'rec_redim');
				$dtmor = odbc_result($rs,'tmo_redim');
				$dhcr = odbc_result($rs,'hc_redim');
				
				$drecref_str = $drecref_str.$drecr.",";
				$dtmoref_srt = $dtmoref_srt.$dtmor.",";
				$dhcref_str = $dhcref_str.$dhcr.",";
			}
			$drecref_str = substr($drecref_str,0,strlen($drecref_str)-1)."]";
			$dtmoref_srt = substr($dtmoref_srt,0,strlen($dtmoref_srt)-1)."]";
			$dhcref_str = substr($dhcref_str,0,strlen($dhcref_str)-1)."]";
			
			//REFORECAST INTRA HORA - ACHAMOS UM NOVO CULPADO...VOC�!!!
			$sqlrefh = "SELECT * FROM forecast.proc_performance_tela1_reforecast(3,".$_GET['id_skill'].",".$mes.",'".$dia."')";	
			$rs = odbc_exec($conn, $sqlrefh);
			odbc_fetch_row($rs);
			while(odbc_fetch_row($rs)){
			
				$hrecr = odbc_result($rs,'rec_redim');
				$htmor = odbc_result($rs,'tmo_redim');
				$hhcr = odbc_result($rs,'hc_redim');
				
				$hrecref_str = $hrecref_str.$hrecr.",";
				$htmoref_srt = $htmoref_srt.$htmor.",";
				$hhcref_str = $hhcref_str.$hhcr.",";
			}
			$hrecref_str = substr($hrecref_str,0,strlen($hrecref_str)-1)."]";
			$htmoref_srt = substr($htmoref_srt,0,strlen($htmoref_srt)-1)."]";
			$hhcref_str = substr($hhcref_str,0,strlen($hhcref_str)-1)."]";
			
				$hccolor = "#ff6161";
				$tmocolor = "#ffff61";
				$reccolor = "#61ffb3";
			
			//matriz de gravidade // A = Gm|r� 
			
			$matriz1 = "SELECT abd, color FROM performance.proc_gravidade(".$_GET['id_skill'].") WHERE abd >= '$mrecdd' "; //aqui temos que inserir o valor
			
			$rs = odbc_exec($conn,$matriz1);
			odbc_fetch_row($rs);

			$gmabdcor = odbc_result($rs,'color');
			
			$matriz2 = "SELECT abd, color FROM performance.proc_gravidade(".$_GET['id_skill'].") WHERE abd >= '$drecdd' "; //aqui temos que inserir o valor
			
			$rs = odbc_exec($conn,$matriz2);
			odbc_fetch_row($rs);

			$gdabdcor = odbc_result($rs,'color');
			
			
			$matriz3 = "SELECT abd, color FROM performance.proc_gravidade(".$_GET['id_skill'].") WHERE abd >= '$hrechh' "; //aqui temos que inserir o valor
			
			$rs = odbc_exec($conn,$matriz3);
			odbc_fetch_row($rs);

			$ghabdcor = odbc_result($rs,'color');

?>
<script type="text/javascript">
$(function () {
	
	
  // configura o tema, gradiente e linhas do grid
  Highcharts.setOptions({
	chart: {
	
      backgroundColor: {
        linearGradient: [0, 0, 500, 500],
        stops: [
          [0, 'rgb(255, 255, 255)'],
          [1, 'rgb(240, 240, 255)']
        ]
      },
	  
      borderWidth: 1,
      plotBackgroundColor: 'rgba(255, 255, 255, .9)'
    },
	legend: {
			itemStyle: {
			   fontSize: '10px'
			}
	    },
      title: {
          text: ''
		},
    yAxis: {
		lineWidth: 1,
		gridLineDashStyle: 'dot',
		  title: {
		  text: '',
			style: {
			  color: '#333',
			}
		}
    },
	});
  var options = {
  
    chart: {
      zoomType: 'x'
	},
    xAxis: {
      type: ''
    }
  };
  
     Highcharts.setOptions({

              colors: ['<? echo $gmabdcor ?>']
    });	
	    	// Radialize the colors
		<? echo $grad ?>
  // cria os graficos, aqui comeca a magia (metal espadinha)
  var chart1Options = {
 chart: {
      renderTo: 'abdm'
    },
	 tooltip: {
            crosshairs: [true, true],
			shared: true
        },
       xAxis: [{
				categories:
                [<?php echo $mperiodo_str; ?>,
				labels: {
                rotation: 300
            }
            }],
			series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $mabd_str; ?>,
			marker: {
				enabled: false
			},
			dashStyle: 'Solid',
			},{    
                
			name: 'Planejado',
			color: '#CD2626',
			type: 'line',
			data:[<?php echo $mabdp_str; ?>,
			marker: {
				enabled: false
			},
			dashStyle: 'Solid'
			},{
			name: '<? echo $mabdd; ?>',
			type: 'pie',
			data:[100],
            borderWidth: 0,
			center: [320, 195],
                size: 45,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
					fontSize: '12px',
					formatter: function() {
                        return this.series.name + '%';
                    },
                    color: 'white',
                    distance: -22
				}
			}]
		
 };
  chart1Options = jQuery.extend(true, {}, options, chart1Options);
  var chart1 = new Highcharts.Chart(chart1Options);

	var chart2Options = {
    chart: {
      renderTo: 'nsm'
    },
		 tooltip: {
            crosshairs: [true, true],
			shared: true
        },
      xAxis: [{
                categories: [<?php echo $mperiodo_str; ?>,
				labels: {
                rotation: 300
            }
            }],

    series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $mns60r_str; ?>,
			marker: {
                enabled: false
            },
        dashStyle: 'Solid',
         },{     
                name: 'Planejado',
				color: '#3f75a2',
                type: 'line',
                data:[<?php echo $mns60r_str; ?>,
             marker: {
                 enabled: false
             },
             dashStyle: 'Solid',
        },{ 
			name: '0',
			color: '#CD2626',
			type: 'pie',
			data:[100],
            borderWidth: 0,
				center: [310, 195],
                size: 45,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
					fontSize: '12px',
					fontWeight:'bold',
					formatter: function() {
                        return this.series.name + '%';
                    },
                    color: 'black',
                    distance: -22
				}
        }]
  };
  chart2Options = jQuery.extend(true, {}, options, chart2Options);
  var chart2 = new Highcharts.Chart(chart2Options);
    
    Highcharts.setOptions({
	
	colors: ['<? echo $mesteira_cor["rec"][1]; ?>']
	});
	<? echo $grad ?>
    var chart3Options = {
	
    chart: {
      renderTo: 'recem'
    },
		 tooltip: {
            crosshairs: [true, true],
			shared: true
        },
       xAxis: [{
                categories: [<?php echo $mperiodo_str; ?>,
				labels: {
                rotation: 300
            }
            }],

		series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $mrecr_str; ?>,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
				valueSuffix: ''
			}
			},{    
			name: 'Planejado',
			color: '#CD2626',
			type: 'line',
			data:[<?php echo $mrecp_str; ?>,
			marker: {
			enabled: false
			},
				dashStyle: 'Solid',
		},{ 
			name: '<? echo $mesteira_cor["rec"][0]; ?>',
			type: 'pie',
			data:[37],
            borderWidth: 0,
						center: [305, 195],
                size: 45,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
					fontSize: '12px',
					fontWeight:'bold',
					formatter: function() {
                        return this.series.name + '%';
                    },
                    color: 'black',
                    distance: -22
				}
        }]

  };
  chart3Options = jQuery.extend(true, {}, options, chart3Options);
  var chart3 = new Highcharts.Chart(chart3Options);
      Highcharts.setOptions({
	
	colors: ['<? echo $mesteira_cor["tmo"][1]; ?>'],
	});
  
  <? echo $grad ?>
var chart4Options = {

    chart: {
      renderTo: 'tmom'
    },
		 tooltip: {
            crosshairs: [true, true],
			shared: true
        },
       xAxis: [{
                categories: [<?php echo $mperiodo_str; ?>,
				labels: {
                rotation: 300
            }
            }],

		series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $mtmor_str; ?>,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
				valueSuffix: ''
			}
			},{    
			name: 'Planejado',
			color: '#CD2626',
			type: 'line',
                data:[<?php echo $mtmop_str; ?>,
			marker: {
			enabled: false
			},
				dashStyle: 'Solid',
},{ 
			name: '<? echo $mesteira_cor["tmo"][0]; ?>',
			color: '#CD2626',
			type: 'pie',
			data:[100],
            borderWidth: 0,
						center: [310, 195],
                size: 45,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
					fontSize: '12px',
					fontWeight:'bold',
					formatter: function() {
                        return this.series.name + '%';
                    },
                    color: 'black',
                    distance: -22
				}
        }]

  };
  chart4Options = jQuery.extend(true, {}, options, chart4Options);
  var chart4 = new Highcharts.Chart(chart4Options);
  
       Highcharts.setOptions({
	
	 colors: ['<? echo $mesteira_cor["hc"][1]; ?>'],
	});
  
  <? echo $grad ?>
  
 var chart5Options = {

    chart: {
      renderTo: 'hcm'
    },
		 tooltip: {
            crosshairs: [true, true],
			shared: true
        },
       xAxis: [{
                categories: [<?php echo $mperiodo_str; ?>,
				labels: {
                rotation: 300
            }
            }],

		series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $mhcr_str; ?>,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
				valueSuffix: ''
			}
			},{    
			name: 'Planejado',
			color: '#CD2626',
			type: 'line',
                data:[<?php echo $mhcp_str; ?>,
			marker: {
			enabled: false
			},
				dashStyle: 'Solid',
},{ 
			name: '<? echo $mesteira_cor["hc"][0]; ?>',
			type: 'pie',
			data:[100],
            borderWidth: 0,
						center: [320, 195],
                size: 45,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
					fontSize: '12px',
					fontWeight:'bold',
					formatter: function() {
                        return this.series.name + '%';
                    },
                    color: 'black',
                    distance: -22
				}
        }]

  };
  chart5Options = jQuery.extend(true, {}, options, chart5Options);
  var chart5 = new Highcharts.Chart(chart5Options);
        Highcharts.setOptions({
	
	 colors: ['<? echo $gdabdcor ?>'],
	});
  
  <? echo $grad ?>
 
var chart6Options = {
  
    chart: {
      renderTo: 'abd'
    },
		 tooltip: {
            crosshairs: [true, true],
			shared: true
        },
       xAxis: [{
                categories: [<?php echo $dperiodo_str; ?>,
				labels: {
                rotation: 300
            }
            }],

		series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $dabd_str; ?>,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
				valueSuffix: ''
			}
			},{    
			name: 'Planejado',
			color: '#CD2626',
			type: 'line',
                data:[<?php echo $dabdp_str ?>,
			marker: {
			enabled: false
			},
				dashStyle: 'Solid',
			tooltip: {
				valueSuffix: ''
			}
			},{
                name: '37%',
                color: '#FFFFFF',
                type: 'area',
				visible: false,
				data:[<?php echo $dabdp_str; ?>,
				marker: {
						enabled: false
						},
						dashStyle: 'Solid',
},{ 
			name: '<? echo $mrecdd ?>',
			type: 'pie',
			data:[<? echo $mrecdd ?>],
            borderWidth: 0,
						center: [320, 195],
                size: 45,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
					fontSize: '12px',
					fontWeight:'bold',
					formatter: function() {
                        return this.series.name + '%';
                    },
                    color: 'white',
                    distance: -22
				}
        }]

  };
  chart6Options = jQuery.extend(true, {}, options, chart6Options);
  var chart6 = new Highcharts.Chart(chart6Options);
  
  
  
var chart7Options = {
    chart: {
      renderTo: 'ns'
    },
		 tooltip: {
            crosshairs: [true, true],
			shared: true
        },
       xAxis: [{
                categories: [<?php echo $dperiodo_str; ?>,
				labels: {
                rotation: 300
            }
            }],

		series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $dns60r_str; ?>,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
				valueSuffix: ''
			}
			},{    
			name: 'Planejado',
			color: '#3f75a2',
			type: 'line',
            data:[<?php echo $dns60r_str; ?>,
			marker: {
			enabled: false
			},
				dashStyle: 'Solid',
},{ 
			name: '0',
			color: '#CD2626',
			type: 'pie',
			data:[37],
            borderWidth: 0,
						center: [310, 195],
                size: 45,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
					fontSize: '12px',
					fontWeight:'bold',
					formatter: function() {
                        return this.series.name + '%';
                    },
                    color: 'black',
                    distance: -22
				}
        }]

  };
  chart7Options = jQuery.extend(true, {}, options, chart7Options);
  var chart7 = new Highcharts.Chart(chart7Options);
  
         Highcharts.setOptions({
	
	 colors : ['<? echo $desteira_cor["rec"][1]; ?>'],
	});
  
  <? echo $grad ?>
 
var chart8Options = {
	
    chart: {
      renderTo: 'rece'
    },
		 tooltip: {
            crosshairs: [true, true],
			shared: true
        },
       xAxis: [{
                categories: [<?php echo $dperiodo_str; ?>,
				labels: {
                rotation: 300
            }
            }],

		series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $drecr_str; ?>,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
				valueSuffix: ''
			}
			},{    
			name: 'Re-Forecast',
			color: '#46b4af',
			type: 'area',
			data:[<?php echo $drecref_str ?>,
			visible: false,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
			valueSuffix: ''
			}
			},{    
			name: 'Planejado',
			color: '#CD2626',
			type: 'line',
			data:[<?php echo $drecp_str; ?>,
			marker: {
			enabled: false
			},
				dashStyle: 'Solid',
},{ 
			name: '<? echo $desteira_cor["rec"][0];  ?>',
			type: 'pie',
			data:[100],
            borderWidth: 0,
						center: [310, 195],
                size: 45,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
					fontSize: '12px',
					fontWeight:'bold',
					formatter: function() {
                        return this.series.name + '%';
                    },
                    color: 'black',
                    distance: -22
				}
        }]

  };
  chart8Options = jQuery.extend(true, {}, options, chart8Options);
  var chart8 = new Highcharts.Chart(chart8Options);
 
	 Highcharts.setOptions({
	
	 colors: ['<? echo $desteira_cor["tmo"][1]; ?>'],
	});
  
  <? echo $grad ?>
 
var chart9Options = {

    chart: {
      renderTo: 'tmo'
    },
		 tooltip: {
            crosshairs: [true, true],
			shared: true
        },
       xAxis: [{
                categories: [<?php echo $dperiodo_str; ?>,
				labels: {
                rotation: 300
            }
            }],

		series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $dtmor_str; ?>,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
				valueSuffix: ''
			}
			},{    
			name: 'Re-Forecast',
			color: '#46b4af',
			type: 'area',
			data:[<?php echo $dtmoref_srt ?>,
			visible: false,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
			valueSuffix: ''
			}
			},{    
			name: 'Planejado',
			color: '#CD2626',
			type: 'line',
            data:[<?php echo $dtmop_str; ?>,
			marker: {
			enabled: false
			},
				dashStyle: 'Solid',
},{ 
			name: '<? echo $desteira_cor["tmo"][0];  ?>',
			type: 'pie',
			data:[100],
            borderWidth: 0,
						center: [310, 195],
                size: 45,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
					fontSize: '12px',
					fontWeight:'bold',
					formatter: function() {
                        return this.series.name + '%';
                    },
                    color: 'black',
                    distance: -22
				}
        }]

  };
  chart9Options = jQuery.extend(true, {}, options, chart9Options);
  var chart9 = new Highcharts.Chart(chart9Options);
  
	Highcharts.setOptions({

	colors: ['<? echo $desteira_cor["hc"][1]; ?>'],
	});

	<? echo $grad ?>
  
var chart10Options = {

    chart: {
      renderTo: 'hc'
    },
		 tooltip: {
            crosshairs: [true, true],
			shared: true
        },
       xAxis: [{
                categories: [<?php echo $dperiodo_str; ?>,
				labels: {
                rotation: 300
            }
            }],

		series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $dhcr_str; ?>,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
				valueSuffix: ''
			}
			},{    
			name: 'Re-Forecast',
			color: '#46b4af',
			type: 'area',
            data:[<?php echo $dhcref_str; ?>,
			visible: false,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
			valueSuffix: ''
			}
			},{    
			name: 'Planejado',
			color: '#CD2626',
			type: 'line',
            data:[<?php echo $dhcp_str; ?>,
			marker: {
			enabled: false
			},
				dashStyle: 'Solid',
},{ 
			name: '<? echo $desteira_cor["hc"][0]; ?>',
			color: '#CD2626',
			type: 'pie',
			data:[100],
            borderWidth: 0,
						center: [320, 195],
                size: 45,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
					fontSize: '12px',
					fontWeight:'bold',
					formatter: function() {
                        return this.series.name + '%';
                    },
                    color: 'black',
                    distance: -22
				}
        }]

  };
  chart10Options = jQuery.extend(true, {}, options, chart10Options);
  var chart10 = new Highcharts.Chart(chart10Options);
  
  	Highcharts.setOptions({

	colors: ['<? echo $ghabdcor ?>'],
	});

	<? echo $grad ?>
  
var chart11Options = {
   
    chart: {
      renderTo: 'abdh'
    },
		 tooltip: {
            crosshairs: [true, true],
			shared: true
        },
       xAxis: [{
                categories: [<?php echo $hperiodo_str; ?>,
				labels: {
                rotation: 300
            }
            }],

		series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $habd_str; ?>,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
				valueSuffix: ''
			}
			},{   
			name: 'Planejado',
			color: '#CD2626',
			type: 'line',
            data:[<?php echo $habdp_str; ?>,
			marker: {
			enabled: false
			},
				dashStyle: 'Solid',
},{ 
			name: '<? echo $drecdd ?>%',
			type: 'pie',
			data:[100],
            borderWidth: 0,
						center: [320, 195],
                size: 45,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
					fontSize: '12px',
					formatter: function() {
                        return this.series.name;
                    },
                    color: 'white',
                    distance: -22
				}
        }]

  };
  chart11Options = jQuery.extend(true, {}, options, chart11Options);
  var chart11 = new Highcharts.Chart(chart11Options);
  
var chart12Options = {
    chart: {
      renderTo: 'nsh'
    },
		 tooltip: {
            crosshairs: [true, true],
			shared: true
        },
       xAxis: [{
                categories: [<?php echo $hperiodo_str; ?>,
				labels: {
                rotation: 300
            }
            }],

		series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $hns60r_str; ?>,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
				valueSuffix: ''
			}
			},{   
			name: 'Planejado',
			color: '#3f75a2',
			type: 'line',
            data:[<?php echo $hns60r_str; ?>,
			marker: {
			enabled: false
			},
				dashStyle: 'Solid',
},{ 
			name: '0%',
			color: '#CD2626',
			type: 'pie',
			data:[100],
            borderWidth: 0,
						center: [310, 195],
                size: 45,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
					fontSize: '12px',
					fontWeight:'bold',
					formatter: function() {
                        return this.series.name;
                    },
                    color: 'black',
                    distance: -22
				}
        }]

  };
  chart12Options = jQuery.extend(true, {}, options, chart12Options);
  var chart12 = new Highcharts.Chart(chart12Options);
  
  	Highcharts.setOptions({

	colors : ['<? echo $hesteira_cor["rec"][1];  ?>'],
	});

	<? echo $grad ?>
  
var chart13Options = {
    chart: {
      renderTo: 'receh'
    },
		 tooltip: {
            crosshairs: [true, true],
			shared: true
        },
       xAxis: [{
                categories: [<?php echo $hperiodo_str; ?>,
				labels: {
                rotation: 300
            }
            }],

		series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $hrecr_str; ?>,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
				valueSuffix: ''
			}
			},{
			name: 'Re-Forecast',
			color: '#46b4af',
			type: 'area',
			data:[<?php echo $hrecref_str ?>,
			visible: false,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
			valueSuffix: ''
			}
			},{   
			name: 'Planejado',
			color: '#CD2626',
			type: 'line',
            data:[<?php echo $hrecp_str; ?>,
			marker: {
			enabled: false
			},
				dashStyle: 'Solid',
},{ 
			name: '<? echo $hesteira_cor["rec"][0]; ?>%',
			color: '#CD2626',
			type: 'pie',
			data:[<? echo abs($hesteira_cor["rec"][0]); ?>],
            borderWidth: 0,
						center: [310, 195],
                size: 45,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
					fontSize: '12px',
					fontWeight:'bold',
					formatter: function() {
                        return this.series.name;
                    },
                    color: 'black',
                    distance: -22
				}
        }]

  };
  chart13Options = jQuery.extend(true, {}, options, chart13Options);
  var chart13 = new Highcharts.Chart(chart13Options);
  
  	Highcharts.setOptions({

	colors: ['<? echo $hesteira_cor["tmo"][1];  ?>'],
	});

	<? echo $grad ?>
  
var chart14Options = {

    chart: {
      renderTo: 'tmoh'
    },
		 tooltip: {
            crosshairs: [true, true],
			shared: true
        },
       xAxis: [{
                categories: [<?php echo $hperiodo_str; ?>,
				labels: {
                rotation: 300
            }
            }],

		series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $htmor_str; ?>,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
				valueSuffix: ''
			}
			},{
			name: 'Re-Forecast',
			color: '#46b4af',
			type: 'area',
			data:[<?php echo $htmoref_srt ?>,
			visible: false,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
			valueSuffix: ''
			}
			},{   
			name: 'Planejado',
			color: '#CD2626',
			type: 'line',
            data:[<?php echo $htmop_str; ?>,
			marker: {
			enabled: false
			},
				dashStyle: 'Solid',
},{ 
			name: '<? echo $hesteira_cor["tmo"][0]; ?>%',
			color: '#CD2626',
			type: 'pie',
			data:[<? echo abs($hesteira_cor["tmo"][0]); ?>],
            borderWidth: 0,
						center: [310, 195],
                size: 45,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
					fontSize: '12px',
					fontWeight:'bold',
					formatter: function() {
                        return this.series.name;
                    },
                    color: 'black',
                    distance: -22
				}
        }]

  };
  chart14Options = jQuery.extend(true, {}, options, chart14Options);
  var chart14 = new Highcharts.Chart(chart14Options);
  
  	Highcharts.setOptions({

	colors: ['<? echo $hesteira_cor["hc"][1];  ?>'],
	});

	<? echo $grad ?>
var chart15Options = {
    chart: {
	
      renderTo: 'hch'
    },
		 tooltip: {
            crosshairs: [true, true],
			shared: true
        },
       xAxis: [{
                categories: [<?php echo $hperiodo_str; ?>,
				labels: {
                rotation: 300
            }
            }],

		series:[{
            name :'Realizado',
            color: '#4682B4',
            type: '<?php echo $gra ?>',
            data:[<?php echo $hhcr_str; ?>,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
				valueSuffix: ''
			}
			},{
			name: 'Re-Forecast',
			color: '#46b4af',
			type: 'area',
			data:[<?php echo $hhcref_str ?>,
			visible: false,
			marker: {
			enabled: false
			},
			dashStyle: 'Solid',
			tooltip: {
			valueSuffix: ''
			}
			},{   
			name: 'Planejado',
			color: '#CD2626',
			type: 'line',
            data:[<?php echo $hhcp_str; ?>,
			marker: {
			enabled: false
			},
				dashStyle: 'Solid',
},{ 
			name: '<? echo $hesteira_cor["hc"][0];  ?>%',
			type: 'pie',
			data:[<? echo abs($hesteira_cor["hc"][0]); ?>],
			color: '#2e00c4',
            borderWidth: 0,
				center: [320, 195],
                size: 45,
                showInLegend: false,
                dataLabels: {
                    enabled: true,
					fontSize: '12px',
					fontWeight:'bold',
					formatter: function() {
                        return this.series.name;
                    },
                    color: 'black',
                    distance: -22
				}
        }]

  };
  
  chart15Options = jQuery.extend(true, {}, options, chart15Options);
  var chart15 = new Highcharts.Chart(chart15Options);
});

</script>
<div id="menu">
  
<form action="" method="get" id="form" onchange="form.submit(this.value);">
			
			<select name="id_skill" id="id_skill">
                <option >Selecione a Fila Desejada</option>
			  
				<?php
			  
					$sql = "SELECT DISTINCT npc.id as npcid, tsa.assunto_2 as assunto_2, tsa.id as id
							FROM skills.tbl_skill tsk 
							INNER JOIN skills.tbl_npc npc ON tsk.npc = npc.id
							INNER JOIN skills.tbl_assunto_2 tsa ON tsa.id = tsk.assunto_2
							WHERE npc.id > 0 and npc.id < 4
							ORDER BY npc.id,tsa.assunto_2";
					$rs = odbc_exec($conn, $sql);
					if (!$rs)
					  {exit("Error in SQL");}
					while (odbc_fetch_row($rs)) 
					{
					  $tempid_skill = odbc_result ($rs,"id");
					  $assunto = odbc_result($rs,"assunto_2");
					  if($tempid_skill == $_GET['id_skill']){
						echo "<option value='$tempid_skill' selected>$assunto</option>\n";
					  }
					  else{
						echo "<option value='$tempid_skill'>$assunto</option>\n";
					  }
					}
				?>
			</select>
			<select name="mes" id="mes">
			 <option selected="selected" value="">M�s</option>
			  <?php
				$temp = $mes;
				$data = date('m');
				if (empty($temp)){
				$temp = $data;
				}
	
					$sqlm = "SELECT * FROM proc_performance_tela1(1,".$_GET['id_skill'].",01,'2013-01-01')";
					$rs = odbc_exec($conn, $sqlm);
					if (!$rs)
					  {exit("Error in SQL");}
					while (odbc_fetch_row($rs))
					{
					  $mes = odbc_result ($rs,"periodo");
					if($mes == $_GET['mes']){
						echo "<option value='$mes' selected>$mes</option>\n";
					  }
					  else{
						echo "<option value='$mes'>$mes</option>\n";
					  }
					}
					
				echo "</select>
				<select name=\"dia\" id=\"dia\">
				<option>Dia</option>";

					$sqld = "SELECT * FROM proc_performance_tela1(2,".$_GET['id_skill'].",".$temp.",'2013-09-01')";
					$rsda = odbc_exec($conn, $sqld);
					if (!$rsda)
					  {exit("Error in SQL");}
					  
					while (odbc_fetch_row($rsda))
					{
					  $dia = odbc_result ($rsda,"periodo");
					  $dia2 = "2013-".$temp."-".explode("-",$dia)[0];
					if($dia2 == $_GET['dia']){
						echo "<option value='$dia2' selected>$dia</option>\n";
					  }
					  else{
						echo "<option value='$dia2'>$dia</option>\n";
						}			  
					}
					echo "</select>";
				?>
		<select name="graf" id="valor" class="selct" >
		<option selected="selected" value="">Tipo</option>
		<option value="area">Area</option>
		<option value="column">Coluna</option>
		<option value="line">Linha</option>
		<option value="spline">Tendencia</option>
		</select>

<div id="a1" style="display:none;">
<a href="#" rel="modal:close"></a><iframe frameborder="0" height="350" width="100%" src="grid.php?tela=1&id_skill=<? echo $id_skill ?>&mes=Jan&dia=2013-10-21"></iframe>
</div>

<div id="a2" style="display:none;">
<a href="#" rel="modal:close"></a><iframe frameborder="0" height="500" width="100%" src="grid.php?tela=2&id_skill=<? echo $id_skill ?>&mes=<? echo $_GET['mes'] ?>&dia=2013-10-21"></iframe>
</div>

<div id="a3" style="display:none;">
<a href="#" rel="modal:close"></a><iframe frameborder="0" height="500" width="100%" src="grid.php?tela=3&id_skill=<? echo $id_skill ?>&mes=<? echo $_GET['mes'] ?>&dia=<? echo $_GET['dia'] ?>"></iframe>
</div>
</form>
</div>

<div style="
    position: relative;
    width: 100%;
    height: 9%;
"><div class="bt" id="tag" style="width: 20%;-webkit-transform: rotate(0deg);position: relative;  z-index: 1;  /* -webkit-transform: rotate(270deg); */    float: left;top: 0;left: 8%;margin-right: 19%;"><a href="#a1">Mensal</a></div><div class="bt" id="tag" style="width: 20%;-webkit-transform: rotate(0deg);position: relative;  z-index: 1;  /* -webkit-transform: rotate(270deg); */    float: left;top: 0;left: 0%;margin-right: 9%;" ><a href="#a2">Di�rio</a></div><div class="bt" id="tag" style="width: 20%;-webkit-transform: rotate(0deg);position: relative;  z-index: 1;  /* -webkit-transform: rotate(270deg); */    float: left;top: 0;margin-right: 0;"><a href="#a3">Intra-di�rio</a></div></div>
	<?php

		$ind = array("abd","ns","rece","tmo","hc");
		$est = array ("eabd","ens","erece","etmo","ehc");
		$tag = array("Abandono","NS60","Recebidas","TMO","HC");
		$vis = array("m","","h");

		for($i = 0; $i<sizeof($ind);$i++){
			$local = $ind[$i];
			$local2 = $tag[$i];
			$local3 = $est[$i];
			echo "<div id='res$local' style='width:100%;position:relative;z-index: 1;'>
					<div id='res$local3' style='width:100%;position:relative;z-index: 1;'>
						<div class='bt' id='tag' style='width:5%;'>$local2</div>";
						for($j = 0 ; $j<sizeof($vis);$j++){
							$v = $vis[$j];
							echo "<div id='$local$v' class='graf'></div>";
						}
			echo "</div></br>";
		}
	?>
<script type="text/javascript" charset="utf-8">
  $(function() {

    function log_modal_event(event, modal) {
      if(typeof console != 'undefined' && console.log) console.log("[event] " + event.type);
    };
	  $('#more').click(function() {
      $(this).parent().after($(this).parent().next().clone());
      $.modal.resize();
      return false;
    });
    $('a[href="#a1"]').click(function(event) {
      event.preventDefault();
      $(this).modal({
        escapeClose: false,
        clickClose: false,
        showClose: true,
		fadeDuration: 250,
		fadeDelay: 1.5
      });
    });
	$('a[href="#a2"]').click(function(event) {
      event.preventDefault();
      $(this).modal({
        escapeClose: false,
        clickClose: false,
        showClose: true,
		fadeDuration: 250,
		fadeDelay: 1.5
      });
    });
	$('a[href="#a3"]').click(function(event) {
      event.preventDefault();
      $(this).modal({
        escapeClose: false,
        clickClose: false,
        showClose: true,
		fadeDuration: 250,
		fadeDelay: 1.5
      });
    });
  });
</script>
</body>
</html>
﻿<link href="node/css/myTheme.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="node/css/comunicacao.css" />

<style type="text/css">

.fancyTable td, .fancyTable th {
	/* appearance */
	border: 1px solid #e7eaed;
	
	/* size */
	padding: 3px;
	}

</style>

<?php
	$acao = $_GET['acao'];
	//Funcao que efetua a seguinte conversao MIS -> Mis
	function LUCase($string){
		return(ucwords(strtolower($string)));
	};
	
	switch ($acao){
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		case 'busca':
			$offset = $_GET['offset'];
			$id = $_GET['id'];
			$id_sol = $_GET['id_sol'];
			$conn=odbc_connect('MISPG','','');
			
			$sql = "SELECT count(id) as contagem
			FROM skills.tbl_skill_solicitacao
			WHERE solicitacao = $id and id::text LIKE '$id_sol%'";
			
			$rs=odbc_exec($conn,$sql);

			$nmsg = odbc_result($rs,'contagem');
			
			$entrada = chr(34). "entrada" . chr(34);
			$avanca = $offset + 15;
			$volta = $offset - 15;
			
			if($avanca < $nmsg){
				
				$esquerda = 'node/img/seta_d1.png';
				$funcaoe = "onclick='consulta($id,$avanca);'";
			}
			else{
				$esquerda = 'node/img/seta_d0.png';
				$funcaoe = "";
			}
			if($volta < 0){
				$direita = 'node/img/seta_e0.png';
				$funcaod = "";
			}
			else{
				$direita = 'node/img/seta_e1.png';
				$funcaod = "onclick='consulta($id,$volta);'";
			}
					
			$sql = "	SELECT tsk.id,tro.nome as usuario,
			tsa1.assunto_1 as tipo_de_produto, 
			tsa2.assunto_2 as fila, 
			tsa3.assunto_3 as subassunto,
			tsf.tipo as tipo,
			data as data_ini, 
			skill as skill, 
			dac as dac,
			tserv.servidor as servidor, 
			tger.gerente as gerente,
			tdir.diretor as diretor, 
			tcons.consultor as consultor,
			tativ.atividadecustos as atividade,
			descricao as descricao,
			data_insert as data_insert,
			solicitacao
						FROM skills.tbl_skill_solicitacao tsk
							LEFT JOIN skills.tbl_assunto_3 tsa3 ON
							tsa3.id = tsk.assunto_3
							LEFT JOIN skills.tbl_assunto_1 tsa1 ON
							tsa1.id = tsk.assunto_1
							LEFT JOIN skills.tbl_assunto_2 tsa2 ON
							tsa2.id = tsk.assunto_2
							LEFT JOIN skills.tbl_servidor tserv ON
							tserv.id = tsk.servidor
							LEFT JOIN skills.tbl_gerente tger ON
							tger.id = tsk.gerencia
							LEFT JOIN skills.tbl_diretor tdir ON
							tdir.id = tsk.diretor
							LEFT JOIN skills.tbl_consultor tcons ON
							tcons.id = tsk.consultor
							LEFT JOIN skills.tbl_atividadecustos tativ ON
							tativ.id = tsk.atividadecustos
							LEFT JOIN skills.tbl_tipo tsf ON
							tsf.id = tsk.fila
							LEFT JOIN (SELECT DISTINCT nome,matricula FROM tbl_ronda_agentes_dia_2014) tro ON
							tro.matricula = usuario
						WHERE solicitacao = $id and tsk.id::text LIKE '$id_sol%'
						ORDER BY data_insert DESC
						LIMIT 15
						OFFSET $offset
			";
						$rs=odbc_exec($conn,$sql);
			
			$sup = $offset + odbc_num_rows($rs);
			if($sup <= 0){
				$offset = 0;
				$sup = 0;
			}
			else{
				$offset++;
			}
			echo "<table>
				<tbody>
					<tr>
						<th style='text-align:right;'>
							<b>$offset-$sup</b> de <b>$nmsg</b>
						</th>
						<th style='width: 30px;'>
							<img src='$direita' style='width: 25px;height: 25px;' $funcaod>
						</th>		
						<th style='width: 30px;'>
							<img src='$esquerda' style='width: 25px;height: 25px;' $funcaoe>
						</th>	
					</tr>
				</tbody>
			</table>";

			echo "<input type='text' id='campoBusca' onclick='vazio(this.id)'  onkeydown='if (event.keyCode == 13)buscaSolicitacao($id,0)' value='Busca' style='float:left;margin-right:2%'></input><img src='node/img/busca.png' style='width:20px;float:left;cursor:pointer' onclick='buscaSolicitacao($id,0)'></img>";

			if($id <> 4){		 
				echo "
				<a style='float:right'>Marcar todos:<input id = 'marcatudo' type='checkbox' onclick='marcaTodos()'></input></a>
				<input type='button' onclick='seletorAcao()' value='Ir' style='float:right;margin-right:2%'></input>
				<select id='acao' style='float:right;margin-right:2%'>
				<option>Escolha uma ação</option>
				<option value='$id'>Confirmar todos selecionados</option>
				<option value='0'>Negar solicitação de todos selecionados</option>
				</select>";
			}

			echo "
				</br>
				</br>";
			$leitura = chr(34). "leitura" . chr(34);
			echo "<center><div style='max-height:600px;overflow-y:auto;'><table class='fancyTable' id='myTable02' name='myTable02' style='font-size:10px;'>";

			echo "<thead><tr><td width='300' ><b>Número da solicitação</b>
						 </td><td width='300' ><b>Usuário</b></td>
						 </td><td width='300' ><b>Grp6</b></td>
						 </td><td width='300' ><b>Grp7</b></td>
						 </td><td width='300' ><b>Grp4</b></td>
						 </td><td width='300' ><b>Data inicial</b></td>
						 </td><td width='300' ><b>Tipo</b></td>
						 </td><td width='300' ><b>Skill</b></td>
						 </td><td width='300' ><b>Dac</b></td>
						 </td><td width='300' ><b>Servidor</b></td>
						 </td><td width='300' ><b>Gerente</b></td>
						 </td><td width='300' ><b>Diretor</b></td>
						 </td><td width='300' ><b>Consultor</b></td>
						 </td><td width='300' ><b>Atividade de custos</b></td>
						 </td><td width='300'  ><b>Descrição</b></td>
						 <td width='300' ><b>Data</b></td>";
			if($id <> 4){		 
				echo "<td width='300' ><b>Confirmar</b></td>";
			}
			echo "</tr></thead>";
			
			while(odbc_fetch_row($rs)){

					echo "<tr>";

						$indicador = array("id","usuario","tipo_de_produto","fila","subassunto","data_ini","tipo","skill","dac","servidor","gerente","diretor","consultor","atividade","descricao","data_insert");
						
						$id_conv = odbc_result($rs,"id");

						for($i = 0 ; $i < sizeof($indicador); $i++){

								if ($indicador[$i] == "data_insert"){
								$resultado = date("d/m/Y H:i:s", strtotime(odbc_result($rs,"$indicador[$i]")));
								}
								else{
								$resultado = str_replace("172.27.203.111","cmsr14",utf8_encode(LUCase(odbc_result($rs,"$indicador[$i]"))));
								}
								echo "<td style='text-align:center;cursor:pointer;'>$resultado</td>";

						}

					if($id <> 4){
						echo "<td style='text-align:center;cursor:pointer;'><input id='".odbc_result($rs,'id')."' name = '".odbc_result($rs,'solicitacao')."'type='checkbox'></input></td>";
					}
					echo "</tr>";
			}
			
			echo "</table></div></center>";
		break;	
		case 'consulta':
			$offset = $_GET['offset'];
			$id = $_GET['id'];
			$conn=odbc_connect('MISPG','','');
			
			$sql = "SELECT count(id) as contagem
			FROM skills.tbl_skill_solicitacao
			WHERE solicitacao = $id";
			
			$rs=odbc_exec($conn,$sql);

			$nmsg = odbc_result($rs,'contagem');
			
			$entrada = chr(34). "entrada" . chr(34);
			$avanca = $offset + 15;
			$volta = $offset - 15;
			
			if($avanca < $nmsg){
				
				$esquerda = 'node/img/seta_d1.png';
				$funcaoe = "onclick='consulta($id,$avanca);'";
			}
			else{
				$esquerda = 'node/img/seta_d0.png';
				$funcaoe = "";
			}
			if($volta < 0){
				$direita = 'node/img/seta_e0.png';
				$funcaod = "";
			}
			else{
				$direita = 'node/img/seta_e1.png';
				$funcaod = "onclick='consulta($id,$volta);'";
			}
					
			$sql = "	SELECT tsk.id,tro.nome as usuario,
			tsa1.assunto_1 as tipo_de_produto, 
			tsa2.assunto_2 as fila, 
			tsa3.assunto_3 as subassunto,
			data as data_ini,
			tsf.tipo as tipo, 
			skill as skill, 
			dac as dac,
			tserv.servidor as servidor, 
			tger.gerente as gerente,
			tdir.diretor as diretor, 
			tcons.consultor as consultor,
			tativ.atividadecustos as atividade,
			descricao as descricao,
			data_insert as data_insert,
			solicitacao
						FROM skills.tbl_skill_solicitacao tsk
							LEFT JOIN skills.tbl_assunto_3 tsa3 ON
							tsa3.id = tsk.assunto_3
							LEFT JOIN skills.tbl_assunto_1 tsa1 ON
							tsa1.id = tsk.assunto_1
							LEFT JOIN skills.tbl_assunto_2 tsa2 ON
							tsa2.id = tsk.assunto_2
							LEFT JOIN skills.tbl_servidor tserv ON
							tserv.id = tsk.servidor
							LEFT JOIN skills.tbl_gerente tger ON
							tger.id = tsk.gerencia
							LEFT JOIN skills.tbl_diretor tdir ON
							tdir.id = tsk.diretor
							LEFT JOIN skills.tbl_consultor tcons ON
							tcons.id = tsk.consultor
							LEFT JOIN skills.tbl_atividadecustos tativ ON
							tativ.id = tsk.atividadecustos
							LEFT JOIN skills.tbl_tipo tsf ON
							tsf.id = tsk.tipo
							LEFT JOIN (SELECT DISTINCT nome,matricula FROM tbl_ronda_agentes_dia_2014) tro ON
							tro.matricula = usuario
						WHERE solicitacao = $id
						ORDER BY data_insert DESC
						LIMIT 15
						OFFSET $offset
			";

			$rs=odbc_exec($conn,$sql);
			
			$sup = $offset + odbc_num_rows($rs);
			if($sup <= 0){
				$offset = 0;
				$sup = 0;
			}
			else{
				$offset++;
			}
			echo "<table>
				<tbody>
					<tr>
						<th style='text-align:right;'>
							<b>$offset-$sup</b> de <b>$nmsg</b>
						</th>
						<th style='width: 30px;'>
							<img src='$direita' style='width: 25px;height: 25px;' $funcaod>
						</th>		
						<th style='width: 30px;'>
							<img src='$esquerda' style='width: 25px;height: 25px;' $funcaoe>
						</th>	
					</tr>
				</tbody>
			</table>";

			echo "<input type='text' id='campoBusca' onclick='vazio(this.id)' onkeydown='if (event.keyCode == 13)buscaSolicitacao($id,0)' value='Busca' style='float:left;margin-right:2%'></input><img src='node/img/busca.png' style='width:20px;float:left;cursor:pointer' onclick='buscaSolicitacao($id,0)'></img>";

			if($id <> 4){		 
				echo "
				<a style='float:right'>Marcar todos:<input id = 'marcatudo' type='checkbox' onclick='marcaTodos()'></input></a>
				<input type='button' onclick='seletorAcao()' value='Ir' style='float:right;margin-right:2%'></input>
				<select id='acao' style='float:right;margin-right:2%'>
				<option>Escolha uma ação</option>
				<option value='$id'>Confirmar todos selecionados</option>
				<option value='0'>Negar solicitação de todos selecionados</option>
				</select>";
			}

			echo "
				</br>
				</br>";
			$leitura = chr(34). "leitura" . chr(34);
			echo "<center><div style='max-height:600px;overflow-y:auto;'><table class='fancyTable' id='myTable02' name='myTable02' style='font-size:10px;'>";

			echo "<thead><tr><td width='300' ><b>Número da solicitação</b>
						 </td><td width='300' ><b>Usuário</b></td>
						 </td><td width='300' ><b>Grp6</b></td>
						 </td><td width='300' ><b>Grp7</b></td>
						 </td><td width='300' ><b>Grp4</b></td>
						 </td><td width='300' ><b>Data inicial</b></td>
						 </td><td width='300' ><b>Tipo</b></td>
						 </td><td width='300' ><b>Skill</b></td>
						 </td><td width='300' ><b>Dac</b></td>
						 </td><td width='300' ><b>Servidor</b></td>
						 </td><td width='300' ><b>Gerente</b></td>
						 </td><td width='300' ><b>Diretor</b></td>
						 </td><td width='300' ><b>Consultor</b></td>
						 </td><td width='300' ><b>Atividade de custos</b></td>
						 </td><td width='300'  ><b>Descrição</b></td>
						 <td width='300' ><b>Data</b></td>";
			if($id <> 4){		 
				echo "<td width='300' ><b>Confirmar</b></td>";
			}
			echo "</tr></thead>";
			
			while(odbc_fetch_row($rs)){

					echo "<tr>";

						$indicador = array("id","usuario","tipo_de_produto","fila","subassunto","data_ini","tipo","skill","dac","servidor","gerente","diretor","consultor","atividade","descricao","data_insert");
						
						$id_conv = odbc_result($rs,"id");

						for($i = 0 ; $i < sizeof($indicador); $i++){

								if ($indicador[$i] == "data_insert"){
								$resultado = date("d/m/Y H:i:s", strtotime(odbc_result($rs,"$indicador[$i]")));
								}
								else{
								$resultado = str_replace("172.27.203.111","cmsr14",utf8_encode(LUCase(odbc_result($rs,"$indicador[$i]"))));
								}
								echo "<td style='text-align:center;cursor:pointer;'>$resultado</td>";

						}

					if($id <> 4){
						echo "<td style='text-align:center;cursor:pointer;'><input id='".odbc_result($rs,'id')."' name = '".odbc_result($rs,'solicitacao')."'type='checkbox'></input></td>";
					}
					echo "</tr>";
			}
			
			echo "</table></div></center>";
		break;	
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			case '2':
				$id = $_GET['id'];;
				$conn=odbc_connect('MISPG','','');
				
				//Bloco que aplica alteracao

				$sql = '
					UPDATE skills.tbl_skill tsk
				   	SET skill = qr1.skill, servidor= qr1.servidor, dac=qr1.dac, tipo=qr1.tipo, calchc=qr1.calchc, npc=qr1.npc, 
					canal=qr1.canal, midia=qr1.midia, sacdec=qr1.sacdec, emergencial=qr1.emergencial, 
				       vip=qr1.vip, slaabd=qr1.slaabd, slans=qr1.slans, acceptable=qr1.acceptable, "24horas"=qr1."24horas",
				       assunto=qr1.assunto, diretoria=qr1.diretoria, diretor=qr1.diretor, gerencia=qr1.gerencia, consultor=qr1.consultor, assunto_1=qr1.assunto_1, 
				       assunto_2=qr1.assunto_2, assunto_3=qr1.assunto_3, ativo=qr1.ativo, ccusto=qr1.ccusto, fila=qr1.fila, atividadecustos=qr1.atividadecustos, 
				       publico=qr1.publico, marca=qr1.marca, empresa=qr1.empresa
					FROM (SELECT skill,servidor, dac, 
					tipo, calchc, npc, canal, midia, sacdec, emergencial, vip, slaabd, 
					slans, acceptable, "24horas", data, assunto, diretoria, diretor, 
					gerencia, consultor, assunto_1, assunto_2, assunto_3, ativo, 
					ccusto, fila, atividadecustos, publico, marca, empresa FROM skills.tbl_skill_solicitacao WHERE id = '.$id.') qr1
					WHERE tsk.servidor = qr1.servidor and tsk.dac = qr1.dac and tsk.skill =  qr1.skill and tsk.data >=  qr1.data
				';
				$rs=odbc_exec($conn,$sql);
				//Bloco que confirma alteracao
				
				$sql = "UPDATE skills.tbl_skill_solicitacao SET solicitacao = 4
						WHERE id = $id";

				$rs=odbc_exec($conn,$sql);
			break;
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			case 1:

				$id = $_GET['id'];;
				$conn=odbc_connect('MISPG','','');

				//Bloco que aplica insercao
				$sql = 'INSERT INTO skills.tbl_skill(skill, grupoforecast, grupodim, grupofin, servidor, dac, 
				tipo, calchc, npc, canal, midia, sacdec, emergencial, vip, slaabd, 
				slans, acceptable, "24horas", data, assunto, diretoria, diretor, 
				gerencia, consultor, assunto_1, assunto_2, assunto_3, ativo, 
				ccusto, fila, atividadecustos, publico, marca, empresa) 
				(SELECT skill, grupoforecast, grupodim, grupofin, servidor, dac, 
				tipo, calchc, npc, canal, midia, sacdec, emergencial, vip, slaabd, 
				slans, acceptable, "24horas", date(generate_series(data::timestamp without time zone, CURRENT_DATE, '.chr(39).'1 day'.chr(39).')), assunto, diretoria, diretor, 
				gerencia, consultor, assunto_1, assunto_2, assunto_3, ativo, 
				ccusto, fila, atividadecustos, publico, marca, empresa FROM skills.tbl_skill_solicitacao WHERE id = '.$id.')';

				$rs=odbc_exec($conn,$sql);

				//Bloco que confirma insercao

				$sql = "UPDATE skills.tbl_skill_solicitacao SET solicitacao = 4
						WHERE id = $id";

				$rs=odbc_exec($conn,$sql);

			break;
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			case 3:
				$id = $_GET['id'];;
				$conn=odbc_connect('MISPG','','');
				
				//Bloco que aplica insercao

				$sql = "DELETE FROM skills.tbl_skill WHERE skill = 
						(SELECT skill FROM skills.tbl_skill_solicitacao WHERE id = $id) 
						and dac = (SELECT dac FROM skills.tbl_skill_solicitacao WHERE id = $id) and 
						servidor = (SELECT servidor FROM skills.tbl_skill_solicitacao WHERE id = $id) and 
						data >= (SELECT data FROM skills.tbl_skill_solicitacao WHERE id = $id)
						";
				$rs=odbc_exec($conn,$sql);

				//Bloco que confirma insercao

				$sql = "UPDATE skills.tbl_skill_solicitacao SET solicitacao = 4
						WHERE id = $id";

				$rs=odbc_exec($conn,$sql);

				echo "Exclusão realizada com sucesso!";
			break;
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			case 0:
				$id = $_GET['id'];;
				$conn=odbc_connect('MISPG','','');

				//Bloco que confirma deleta

				$sql = "UPDATE skills.tbl_skill_solicitacao SET solicitacao = 0
						WHERE id = $id";

				$rs=odbc_exec($conn,$sql);

			break;
		}

if(isset($conn)){
odbc_close($conn);
};	
?>
﻿<?php
	$acao = $_GET['acao'];
	             	
	switch ($acao){
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		case 'inicio':
			$turma = $_GET['turma'];
			$conn=odbc_connect('MISPG','','');
			
			$sql = "SELECT DISTINCT data_inicio FROM tbl_turmas_escola WHERE turma = '$turma';";
		//Constroi a lista das turmas cadastradas no sistema
			$rs=odbc_exec($conn,$sql);
			
			$data = date("d/m/Y", strtotime(odbc_result($rs,"data_inicio")));
		//Captura da de inicio da turmas (apenas para exibicao)
			echo "<input id='dt_inicio' value='$data' disabled='true'/>";
		break;
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		case 'consulta':
			$turma = $_GET['turma'];
			$inicio = $_GET['inicio'];
			$fim = $inicio - 1;
			
			$conn=odbc_connect('MISPG','','');
			
		//query principal para capturar informacoes da turma($turma) no intervalo de [inicio + $inicio ; inicio + $fim]
			$sql = "SELECT tbl_produtividade_agente_dia_2013.nome_ronda  as nome, tbl_produtividade_agente_dia_2013.matricula as matr,
							tbl_turmas_escola.turma as tur, 
							date_part('month' , data) as mes, 
							SUM(atd) as atendidas, 		
							CASE WHEN SUM(atd) > 0
								THEN SUM(atd*tmo)/SUM(atd)
							ELSE 0 END as tmo, 
							CASE WHEN SUM(faltas) IS NOT NULL
							THEN SUM(faltas)
							ELSE 0 END as faltas, 
							CASE WHEN SUM(qtd_monitorias) > 0 
								THEN ROUND(sum(qtd_monitorias*nota)/sum(qtd_monitorias),2)
							ELSE 0 END as nota
					FROM tbl_produtividade_agente_dia_2013
					LEFT JOIN tbl_turmas_escola
						ON tbl_turmas_escola.matricula = tbl_produtividade_agente_dia_2013.matricula
					WHERE tbl_produtividade_agente_dia_2013.data >= tbl_turmas_escola.data_inicio + interval '$fim month' AND 
						tbl_produtividade_agente_dia_2013.data < tbl_turmas_escola.data_inicio + interval '$inicio month' and 
						tbl_turmas_escola.turma = '$turma'
					GROUP BY matr, mes, tur, nome_ronda
					ORDER BY nome
					";
			
			$rs=odbc_exec($conn,$sql);
			
		//Array com os indicadores utilizados na tabela
			$indicador = array("nome","mes","atendidas","tmo","faltas","nota");
		
		//Array para associar numero -> nome do mes
			$mes = array("Total","Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro" , "Novembro", "Dezembro");
			
			echo "<table class='fancyTable' id='myTable02' name='myTable02' style='font-size: 10px;'>";
			
			echo "
				<thead>
					<td style='text-align:center;width: 300px;cursor:pointer;'>Nome do operador</td>
					<td class='sorttable_nosort' style='text-align:center;width: 50px;cursor:pointer;'>Mês</td>
					<td style='text-align:center;cursor:pointer;'>Atendidas</td>
					<td style='text-align:center;cursor:pointer;'>TMO</td>
					<td style='text-align:center;cursor:pointer;'>Faltas</td>
					<td style='text-align:center;cursor:pointer;'>Qualidade</td>
				</thead>
			";
		
		//rotina de impressao da tabela
			while(odbc_fetch_row($rs)){
				echo "<tr>";
					for($i = 0 ; $i < sizeof($indicador); $i++){
						If("$indicador[$i]" == "tmo"){
							$resultado = gmdate('H:i:s',odbc_result($rs,"$indicador[$i]"));
							echo "<td style='text-align:center;'>$resultado</td>";
						}
						elseif("$indicador[$i]" == "faltas"){
							$resultado = (int)odbc_result($rs,"$indicador[$i]");
						//Cada 'if' define as cores, baseado nos parametros informados por Aline Costa
							if((int)odbc_result($rs,"$indicador[$i]") == 0){
								echo "<td style='text-align:center;color:green'>$resultado</td>";
							}
							else{
								echo "<td style='text-align:center;color:red'>$resultado</td>";
							}

							}
							elseif("$indicador[$i]" == "nome"){
							$resultado = utf8_encode(odbc_result($rs,"$indicador[$i]"));
								echo "<td style='text-align:left;'>$resultado</td>";
						}
						elseif("$indicador[$i]" == "nota"){
							$resultado = odbc_result($rs,"$indicador[$i]");
							if(odbc_result($rs,"$indicador[$i]") >= 7){
								echo "<td style='text-align:center;color:green'>$resultado</td>";
							}
							else{
								echo "<td style='text-align:center;color:red'>$resultado</td>";
							}
						}
						elseif("$indicador[$i]" == "mes"){
							$resultado = $mes[odbc_result($rs,"$indicador[$i]")];
								echo "<td style='text-align:center;'>$resultado</td>";
					
						}
						else{
						//utf8_encode para exibicao dos acentos corretamente
							$resultado = utf8_encode(odbc_result($rs,"$indicador[$i]"));
							echo "<td style='text-align:center;'>$resultado</td>";
						}
						
					}
				echo "</tr>";
			}
			echo "</table>";
		break;
		case 'grafico':
			$turma = $_GET['turma'];
			$matricula = $_GET['matricula'];
			
			$conn = odbc_connect("MISPG", "", "");

			$sql = "SELECT eq_mes as mes, eq_atendidas, ag_atendidas, eq_tmo, ag_tmo, eq_faltas,ag_faltas,eq_nota,ag_nota 
			   FROM (SELECT                                                         
					   date_part('month' , data) as eq_mes, 
					   SUM(atd)/(SELECT count( q1.matricula) 
					   FROM (
							   SELECT DISTINCT tbl_produtividade_agente_dia_2013.matricula FROM tbl_produtividade_agente_dia_2013
							   INNER JOIN tbl_turmas_escola
									   ON tbl_turmas_escola.matricula = tbl_produtividade_agente_dia_2013.matricula
							   INNER JOIN tbl_ccm7_hierarquia
									   ON tbl_ccm7_hierarquia.cod_re_rh = tbl_produtividade_agente_dia_2013.matricula
					   ) as q1) as eq_atendidas,                
					   CASE WHEN SUM(atd) > 0
							   THEN SUM(atd*tmo)/SUM(atd)
					   ELSE 0 END as eq_tmo, 
					   CASE WHEN SUM(faltas) IS NOT NULL
					   THEN SUM(faltas)
					   ELSE 0 END as eq_faltas, 
					   CASE WHEN SUM(qtd_monitorias) > 0 
							   THEN ROUND(sum(qtd_monitorias*nota)/sum(qtd_monitorias),2)
					   ELSE 0 END as eq_nota
					   FROM tbl_produtividade_agente_dia_2013
					   INNER JOIN tbl_turmas_escola
							   ON tbl_turmas_escola.matricula = tbl_produtividade_agente_dia_2013.matricula
					   GROUP BY eq_mes
					   ORDER BY eq_mes) as query1 --QUERY QUE RETORNA INFORMACOES DA EQUIPE
			   INNER JOIN
					(SELECT                 
					   date_part('month' , data) as ag_mes, 
					   SUM(atd) as ag_atendidas,                 
					   CASE WHEN SUM(atd) > 0
							   THEN SUM(atd*tmo)/SUM(atd)
					   ELSE 0 END as ag_tmo, 
					   CASE WHEN SUM(faltas) IS NOT NULL
					   THEN SUM(faltas)
					   ELSE 0 END as ag_faltas, 
					   CASE WHEN SUM(qtd_monitorias) > 0 
							   THEN ROUND(sum(qtd_monitorias*nota)/sum(qtd_monitorias),2)
					   ELSE 0 END as ag_nota
					   FROM tbl_produtividade_agente_dia_2013
					   INNER JOIN tbl_turmas_escola
							   ON tbl_turmas_escola.matricula = tbl_produtividade_agente_dia_2013.matricula
					   WHERE  tbl_produtividade_agente_dia_2013.data >= tbl_turmas_escola.data_inicio + interval '0 month' AND 
							   tbl_produtividade_agente_dia_2013.data < tbl_turmas_escola.data_inicio + interval '6 month' and 
							   tbl_produtividade_agente_dia_2013.matricula = '$matricula'
					   GROUP BY ag_mes
					   ORDER BY ag_mes) as query2 --QUERY QUE RETORNA INFORMACOES DO AGENTE NO PERIODO DE 180 DIAS
			   ON query2.ag_mes = query1.eq_mes
			   WHERE eq_atendidas <> 0

			   ";
			   
			$rs = odbc_exec ($conn,$sql);
			
			$mes = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro" , "Novembro", "Dezembro");
						
		break;
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//FUNCAO NAO UTILIZADA
		case 'editar':
			$turma = $_GET['turma'];
			
			$conn=odbc_connect('MISPG','','');
			
			$sql = "SELECT id, turma, nome_turma, periodo, 
							atd[1] as infatd,atd[2] as supatd, 
							tmo[1] as inftmo,tmo[2] as suptmo, 
							faltas[1] as inffaltas,faltas[2] as supfaltas, 
							nota[1] as infnota, nota[2] as supnota
					FROM tbl_regras_escola
					WHERE turma = '$turma'";
					
			$rs=odbc_exec($conn,$sql);
  
				$nome_turma = odbc_result($rs,'nome_turma');
				$id = odbc_result($rs,'id');
				$periodo = odbc_result($rs,'periodo');
				$supa = odbc_result($rs,'supatd');
				$infa = odbc_result($rs,'infatd');
				$supt = odbc_result($rs,'suptmo');
				$inft = odbc_result($rs,'inftmo');
				$supf = odbc_result($rs,'supfaltas');
				$inff = odbc_result($rs,'inffaltas');
				$supn = odbc_result($rs,'supnota');
				$infn = odbc_result($rs,'infnota');
			
			$metas = array(array($infa,$supa),array($supt,$inft),array($supf,$inff),array($infn,$supn));
			$indicador = array("Atendidas","TMO","Faltas","Nota_de_monitorias");
			$cores = array(array("red","green"),array("green","red"),array("green","red"),array("red","green"));
			
			echo "<label for'nome_turma'>Nome da turma: <input id='nome_turma' value='$nome_turma'/></label>";
			echo "<center>echo<div style='max-height:600px;overflow-y:auto;'><table class='fancyTable' id='myTable02' name='myTable02' style='font-size: 10px;'>";
			for($i = 0; $i < sizeof($indicador); $i++){
			
				$cor =  $cores[$i][0];
				$cor2 = $cores[$i][1];
				
				$esq = $metas[$i][0];
				$dir = $metas[$i][1];
				
					echo   "<tr>
								<td rowspan='2' style='text-align:right;'>$indicador[$i]</td>
								<td style='text-align:right;color:$cor;'>menor que</td>
								<td style='text-align:center;color:orange;'>entre</td>
								<td style='text-align:left;color:$cor2;'>maior que</td>
							</tr>
					<tr>";
																						
					if($indicador[$i] == 'TMO'){
						echo "<td colspan='3'><input value='00' style='width:19px;margin-left:55px;' onkeypress='numeros()' id='H_inf_$indicador[$i]'/>:
									<input value='00' style='width:19px;' onkeypress='numeros()' id='m_inf_$indicador[$i]'/>:
									<input value='00' style='width:19px;' onkeypress='numeros()' id='s_inf_$indicador[$i]'/>
							";
						echo "<input value='00' style='width:19px;margin-left:20px;' onkeypress='numeros()' id='H_sup_$indicador[$i]'/>:
								<input value='00' style='width:19px;' onkeypress='numeros()' id='m_sup_$indicador[$i]'/>:
								<input value='00' style='width:19px;' onkeypress='numeros()' id='s_sup_$indicador[$i]'/>
							</td>";
					}
					else{
						echo "<td colspan='3'><input value='$esq' style='width:80px; margin-left:55px;' onkeypress='numeros()' id='inf_$indicador[$i]'/>";
						echo "<input value='$dir' style='width:80px;margin-left:20px;' onkeypress='numeros()' id='sup_$indicador[$i]'/></td>";
					}
					
				echo "</tr><tr style='height:5px;'></tr>";
			};
			echo "</table></div></center>";
			
			echo "<div style='float:right;padding-right:200px;'>";
				echo "<input type='button' value='Enviar' onclick='enviar();'>";
				echo "<input type='button' value='Cancelar'>";
			echo "</div>";
		break;
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//FUNCAO NAO UTILIZADA
		case 'enviar':
			$nome_turma = $_GET['nome'];
			$sup_atd = $_GET['supa'];
			$inf_atd  = $_GET['infa'];
			$sup_tmo = $_GET['supt'];
			$inf_tmo = $_GET['inft'];
			$sup_faltas = $_GET['supf'];
			$inf_faltas = $_GET['inff'];
			$sup_nota = $_GET['supn'];
			$inf_nota = $_GET['infn'];
		break;
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}
if(isset($conn)){
odbc_close($conn);
};	
?>
﻿<?php
error_reporting(0);
$pagina = $_GET['pagina'];

switch ($pagina){
	case 'graf':
		echo "<table><tr><td>Em construção!</td></tr></table>";
		break;
	case 'regras':
			$usuario = $_GET['usuario'];
			$indicadores = array(array("Maior","Menor","Menor","Menor"),
						   array("Menor","Maior","Maior","Maior"));
			
			$conn=odbc_connect('MISPG','','');	
			$sql = "
				SELECT 
					tr.indicador id, 
					initcap(replace(ti.indicador, ' tmkt','')) indicador, 
					to_char(peso*100,'90D00%') peso, 
					CAST((round(CAST (100*(peso) AS numeric),2)) as character varying)||'%' as peso, 
					CASE WHEN tr.indicador = 2 THEN 
						CAST(ROUND(CAST((1-faixav1[1])*referencia[1] AS NUMERIC),2) AS CHARACTER VARYING)
					ELSE 
						CAST(ROUND(CAST((1-faixav1[1])*referencia[1]*100 AS NUMERIC),2) AS CHARACTER VARYING)||'%' 
					END as melhor, 
					CASE WHEN tr.indicador = 2 THEN 
						CAST(ROUND(CAST((1-faixav1[2])*referencia[1] AS NUMERIC),2) as character varying) 
					ELSE CAST(ROUND(CAST((1-faixav1[2])*referencia[1]*100 AS NUMERIC),2) as character varying)||'%' 
					END as pior 
				FROM (SELECT DISTINCT LEFT(ccusto, 12) ccusto, indicador, peso, referencia, faixav1 FROM ppr_2013.tbl_regras) tr 
				INNER JOIN (SELECT DISTINCT LEFT(ccusto, 12) ccusto, matricula FROM ppr_2013.tbl_ronda_1hierarquia_mes_2013) th 
				ON th.ccusto = LEFT(tr.ccusto, 12)
				INNER JOIN ppr_2013.tbl_indicadores ti 
				ON ti.id = tr.indicador 
				WHERE matricula = '$usuario'
				ORDER BY tr.indicador;
			";
			/*$rs=odbc_exec($conn,$sql);
			
			$i = 0;
			echo "<p>";
			echo "<table>";
			echo "<tr>";
			//Metricas dos indicadores
			echo "<td>";
			while(odbc_fetch_row($rs)){
					$texto = $indicadores[0][$i];
					$peso = odbc_result($rs,"peso");
					$sinalm = $indicadores[1][$i];
					$sinalp = $indicadores[2][$i];
					$melhor = odbc_result($rs,"melhor");
					$pior = odbc_result($rs,"pior");
					
					echo "<b style='font:14px'>$texto </b>(Peso de $peso)";
					echo "<p style='margin-left:1em'>Resultado $sinalm ou igual que $melhor, seu ganho será de 100%</p>";
					echo "<p style='margin-left:1em'>Resultado $sinalp $melhor e $sinalm ou igual $pior, seu ganho será de 80%</p>";
					echo "<p style='margin-left:1em'>Resultado $sinalp que $pior, seu ganho será de 0%</p>";
					
					$i++;
				}
			echo "</td>";*/
			
	///////// Começo da Tabela, cabeçalho para as 3 tabelas /////////////////////
		echo "<table width='100%'>";
		echo "<tr><td width='50%'>";
			echo "<table width='50%'>";
			echo "<tr>";
			echo "<td style='font-weight:bold; background-color:#003366; color:#f5f5f5; border-bottom:3px solid #fff;'>PPR 2013 Versão 01 – Janeiro a Julho</td>";
			echo "</tr>";

			$rs=odbc_exec($conn,$sql);
			
			$i=0;			
			while ($linha=odbc_fetch_array($rs)){
			$id = $linha['id'];
			$texto = $linha['indicador'];
			$pesoh[$i] = $peso;
			$melhor = $linha['melhor'];
			$pior = $linha['pior'];
			$sinalm = $indicadores[1][$i];
			$sinalp = $indicadores[0][$i];
					
				echo "<tr>";
				echo "<td style='background:#F1F1F1; font-weight:bold;'>$texto(Peso de $peso)</td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td>Resultado $sinalm ou igual a $melhor, seu ganho será de 100%</td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td>Resultado $sinalp que $melhor e $sinalm ou igual a $pior, seu ganho será de 80%</td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td>Resultado $sinalp que $pior, seu ganho será de 0%</td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td height='5'> </td>";
				echo "</tr>";					
					
				$i++;
			}
			echo "</table>";
			echo "</td><td>";
			$sql = "					
					SELECT DISTINCT
	th.celula,
	tr.indicador id, 
	initcap(replace(ti.indicador, ' tmkt','')) indicador, 
	to_char(peso*100,'90D00%') peso, 
	--CAST((round(CAST (100*(peso) AS numeric),2)) as character varying)||'%' as peso, 
	CASE WHEN tr.indicador = 2 THEN 
		CAST(ROUND(CAST((1-faixav2[1])*referencia[1] AS NUMERIC),2) AS CHARACTER VARYING)
	ELSE 
		CAST(ROUND(CAST((1-faixav2[1])*referencia[1]*100 AS NUMERIC),2) AS CHARACTER VARYING)||'%' 
	END as melhor, 
	CASE WHEN tr.indicador = 2 THEN 
		CAST(ROUND(CAST((1-faixav2[2])*referencia[1] AS NUMERIC),2) as character varying) 
	ELSE 
		CAST(ROUND(CAST((1-faixav2[2])*referencia[1]*100 AS NUMERIC),2) as character varying)||'%' 
	END as pior 
FROM (SELECT DISTINCT ccusto, indicador, peso, referencia, faixav2 FROM ppr_2013.tbl_regras) tr 
INNER JOIN 
	(
		SELECT DISTINCT 
			ccusto,
			grupo,
			celula,
			matricula 
		FROM ppr_2013.tbl_ronda_1hierarquia_mes_2013 th
		INNER JOIN ppr_2013.tbl_cargos_elegiveis tcargos
		ON th.cargo = tcargos.cargo
		WHERE tcargos.ativosn = TRUE OR tcargos.cargo = 'AUX ADMINISTRATIVO'
	) th 
ON th.ccusto = tr.ccusto
INNER JOIN ppr_2013.tbl_indicadores ti 
ON ti.id = tr.indicador 
WHERE matricula = '$usuario'
ORDER BY th.celula, tr.indicador; 
				";
										
			echo "<table width='50%'>";
			echo "<tr>";
			echo "<td style='font-weight:bold; background-color:#003366; color:#f5f5f5; border-bottom:3px solid #fff;'>PPR 2013 Versão 02 – Agosto a Dezembro</td>";
			echo "</tr>";
			$rs=odbc_exec($conn,$sql);
			$i = 0;
			/*while(odbc_fetch_row($rs)){
					$texto = $indicadores[0][$i];
					$peso = odbc_result($rs,"peso");
					$sinalm = $indicadores[1][$i];
					$sinalp = $indicadores[2][$i];
					$melhor = odbc_result($rs,"melhor");
					$pior = odbc_result($rs,"pior");*/
			$i=0;			
			while ($linha=odbc_fetch_array($rs)){
					$id = $linha['id'];
					$texto  = $linha['indicador'];
					$head[$i] = $texto;
					$peso = $linha['peso'];
					$pesoh[$i] = $peso;
					$melhor = $linha['melhor'];
					$pior = $linha['pior'];
					$sinalm = $indicadores[1][$i];
					$sinalp = $indicadores[0][$i];
				echo "<tr>";
				echo "<td style='background:#F1F1F1; font-weight:bold;'>$texto(Peso de $peso)</td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td>Resultado $sinalm ou igual a $melhor, seu ganho será de 100%</td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td>Resultado $sinalp que $melhor e $sinalm ou igual a $pior, seu ganho será de 80%</td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td>Resultado $sinalp que $pior, seu ganho será de 0%</td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td height='5'> </td>";
				echo "</tr>";				
					
				$i++;
			}

			
			echo "</table>";
			echo "</td>";
			echo "</tr>";
			echo "</table>";
	////////////// Fim da Tabela ////////////////////////////////
			
			$sql = "SELECT tcc.premio_max as peso
					FROM ppr_2013.tbl_ccusto tcc
					WHERE tcc.ccusto = (SELECT th.centro_custo
								FROM tbl_ccm7_hierarquia th
								WHERE th.cod_re_rh = '$usuario')";
			
			$rs=odbc_exec($conn,$sql);
			
			$peso = odbc_result($rs,"peso");
			
			//Metrica do lucro liquido
			echo "<td valign='top'>";
			echo "<b style='font:14px'>Lucro líquido</b>(Peso de 20.00%)";
			echo "<p style='margin-left:1em'>Resultado de lucro líquido será apurado no fechamento do ano de 2013 pela Controladoria</p>";
	

			//Metrica do lucro liquido
			echo "<b style='font:14px'>Composição da PPR 2013:</b>";
				echo "<table align='center'>";
					echo "<thead>";
						echo "<tr>";
						for ($i=0; $i<4 ; $i++) { 							
							echo '<th style="text-align:center">'.$head[$i].'</th>';
							echo "<th></th>";							
						}		
							echo "<th style='text-align:center'>Lucro Liquido</th>";
							echo "<th></th>";
							echo "<th style='text-align:center'>Total</th>";
							echo "</tr>";
					echo "</thead>";
					echo "<tr>";
					for ($i=0; $i<4 ; $i++) { 
							echo '<td align="center">'.$pesoh[$i].'</td>';
							echo '<td>+</td>';
						}						
						
						echo '<td align="center">20%</td>';
						echo '<td align="center"></td>';
						echo "<td align='center'>100% do prêmio </td>";
					echo "</tr>";
				echo "</table>";
			echo "</td>";
			echo "</tr>";
			echo "</table>";
		break;
	case 'orientacoes':
		echo "<table><tr><td>Em construção!</td></tr></table>";
		break;
	case 'regras_ger':
	
			$usuario = $_GET['usuario'];
			$indicadores = array(array("Faltas","Qualidade", "Abandono","Abandono CHAT"),
						 array("Menor","Maior","Menor","Menor"),
						 array("Maior","Menor","Maior","Maior"));
						 
			$conn=odbc_connect('MISPG','','');
			
			$sql = "SELECT tc.ccusto, tc.grupo
					FROM ppr_2013.tbl_gerente tg
					INNER JOIN ppr_2013.tbl_ccusto tc
						ON tg.gerente = tc.gerente
					WHERE tg.matricula IN (
								SELECT gerente_matricula 
								FROM ppr_2013.tbl_gerente_coor as tbl_coor 
								WHERE tbl_coor.matricula = '$usuario'
								)
					ORDER BY tc.grupo";
			
			$rs=odbc_exec($conn,$sql);

			echo "<select id='ccusto' onchange='regras_gerencia(this.options[selectedIndex].value)'>";
				echo "<option value='%'>Selecione um centro de custo</option>";
					while(odbc_fetch_row($rs)){
						$ccusto = odbc_result($rs,"ccusto");
						$grupo = odbc_result($rs,"grupo");
						echo "<option value='$ccusto'>$grupo</option>";
					}
			echo "</select>";
			
			echo "<div style='width: 100%;
					height: 10px;
					border-bottom: 1px rgb(184, 184, 184);
					border-bottom-style: solid;
					border-bottom-width: thin;'></div>";
					
			echo "<div id='regras'></div>";
	break;
	case 'regras_ger_cons':
	
			$ccusto = $_GET['ccusto'];
			$indicadores = array(array("Menor","Maior","Menor","Menor"),
								 array("Maior","Menor","Maior","Maior"));
			
			$conn=odbc_connect('MISPG','','');	
			$sql = "
				SELECT indicador, CAST((round(CAST (100*(peso) AS numeric),2)) as character varying)||'%' as peso, CASE WHEN indicador = 2 THEN CAST(round(CAST (((1-faixav2[1])*referencia) AS numeric),2) as character varying) ELSE CAST((round(CAST (100*((1-faixav2[1])*referencia) AS numeric),2)) as character varying)||'%' END as melhor , 
				CASE WHEN indicador = 2 THEN CAST(round(CAST (((1-faixav2[2])*referencia) AS numeric),2) as character varying) ELSE CAST((round(CAST (100*((1-faixav2[2])*referencia) AS numeric),2)) as character varying)||'%' END as pior
				FROM ppr_2013.tbl_regras
				WHERE ccusto = '$ccusto' and indicador <= 3 ORDER BY indicador;
			";
			$rs=odbc_exec($conn,$sql);
			
			$i = 0;
			echo "<p>";
			echo "<table>";
			echo "<tr>";
			//Metricas dos indicadores
			echo "<td>";
			/*while(odbc_fetch_row($rs)){
					$texto = $indicadores[0][$i];
					$peso = odbc_result($rs,"peso");
					$sinalm = $indicadores[1][$i];
					$sinalp = $indicadores[2][$i];
					$melhor = odbc_result($rs,"melhor");
					$pior = odbc_result($rs,"pior");*/			
			while ($linha=odbc_fetch_array($rs)){
					$id = $linha['id'];
					$texto = $linha['indicador'];
					$peso = $linha['peso'];
					$melhor = $linha['melhor'];
					$pior = $linha['pior'];
					$sinalm = $indicadores[1][$i];
					$sinalp = $indicadores[0][$i];
					
					echo "<b style='font:14px'>$texto </b>(Peso de $peso)";
					echo "<p style='margin-left:1em'>Resultado $sinalm ou igual que $melhor, seu ganho será de 100%</p>";
					echo "<p style='margin-left:1em'>Resultado $sinalp $melhor e $sinalm ou igual $pior, seu ganho será de 80%</p>";
					echo "<p style='margin-left:1em'>Resultado $sinalp que $pior, seu ganho será de 0%</p>";
					
					$i++;
				}
			echo "</td>";
			
			
			$sql = "SELECT tcc.premio_max as peso
					FROM ppr_2013.tbl_ccusto tcc
					WHERE tcc.ccusto = '$ccusto'";
			
			$rs=odbc_exec($conn,$sql);
			
			$peso = odbc_result($rs,"peso");
			
			//Metrica do lucro liquido
			echo "<b style='font:14px'>Composição da PPR 2013:</b>";
				echo "<table align='center'>";
					echo "<thead>";
						echo "<tr>";
						for ($i=0; $i<4 ; $i++) { 							
							echo '<th style="text-align:center">'.$head[$i].'</th>';
							echo "<th></th>";							
						}		
							echo "<th style='text-align:center'>Lucro Liquido</th>";
							echo "<th></th>";
							echo "<th style='text-align:center'>Total</th>";
							echo "</tr>";
					echo "</thead>";
						echo "<tr>";
						for ($i=0; $i<4 ; $i++) { 
							echo '<td align="center">'.$pesoh[$i].'</td>';
							echo '<td>+</td>';
						}						
							echo '<td align="center">20%</td>';
							echo '<td align="center"></td>';
							echo "<td align='center'>100% do prêmio </td>";
							echo "</tr>";
				echo "</table>";
			echo "</td>";
			echo "</tr>";
			echo "</table>";
			echo "</td>";
			echo "</tr>";
			echo "</table>";
		break;
	break;
}
if(isset($conn)){
odbc_close($conn);};

?>
<?php
	$acao = $_GET['acao'];
	             	
	switch ($acao){
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		case 'filtro_cc':

			if(isset($_GET['gerente'])){
				$ger = $_GET['gerente'];
			}
			else{
				$ger = '';
			};

			if(isset($_GET['ccusto'])){
				$ccusto = $_GET['ccusto'];
			}
			else{
				$ccusto = '';
			};

			if(isset($_GET['mes'])){
				$mesc = $_GET['mes'];
			}
			else{
				$mesc = '';
			};

			$conn=odbc_connect('MISPG','','');

			$sql = 'SELECT Max(data_insert) as max FROM tbl_produtividade_agente_dia_2013;';
			  
			$rs=odbc_exec($conn,$sql);

			$atualiza= date("d/m/Y H:i:s", strtotime(odbc_result($rs,"max")));

				$sql = "  SELECT DISTINCT 
				CASE gerente WHEN 'CARLA MACIEIRA' 
				THEN 'SONIA RICA' ELSE gerente 
				END as gerente
				FROM tbl_ronda_hierarquia_2013
				ORDER BY gerente";
			$rs=odbc_exec($conn,$sql);

			echo "<table id=>";
			echo "<thead>";
				echo "<tr><td style='text-align:center;'>Gerência</td>";
				echo "<td style='text-align:center;'>Centro de custo</td>";
				echo "<td style='text-align:center;'>Mês</td></tr>";
			echo "</thead>";
			
			echo "<td style='text-align:center;'>
					<select name='gerente' id='gerente' type='text' onchange='atualiza();filtros();'>
					<option value='%'>Selecione uma gerência</option>
					";
				while(odbc_fetch_row($rs)){
					$gerente = odbc_result($rs,"gerente");
					if($gerente == $ger){
						echo "<option value='$gerente' selected>$gerente</option>";
					}
					else{
						echo "<option value='$gerente'>$gerente</option>";
					};
				}

			echo "</select>
				   </td>";
			
			$sql = "  SELECT DISTINCT 
					  grupo,ccusto
					  FROM tbl_ronda_hierarquia_2013
					  WHERE gerente = '$ger'
					  ORDER BY grupo";

			$rs=odbc_exec($conn,$sql);

			echo "<td style='text-align:center;'>
			<select name='ccusto' id='ccusto' type='text' onchange='atualiza();filtros();'>
			<option value='' >Selecione um centro de custo</option>";
				while(odbc_fetch_row($rs)){
					$cc = utf8_encode(odbc_result($rs,"grupo"));
					$numero = odbc_result($rs,"ccusto");
					if ($numero == $ccusto) {
						echo "<option value='$numero' selected>$cc</option>";
					}
					else{
						echo "<option value='$numero'>$cc</option>";
					};
				}
			echo "
			</select>
			</td>";

			$sql = "SELECT DISTINCT extract('month' from data) as mes
					FROM tbl_produtividade_agente_dia_2013
					INNER JOIN (SELECT matricula from tbl_ronda_hierarquia_2013 WHERE ccusto = '$ccusto') as tbl_ronda
					on tbl_produtividade_agente_dia_2013.matricula = tbl_ronda.matricula
					ORDER BY mes";

			$rs=odbc_exec($conn,$sql);

			$mes = $arrayName = array('Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');
			echo "<td style='text-align:center;'>
				  <select name='mes' id='mes' type='text' onchange='atualiza();filtros();'>
				  <option value='' >Selecione um mês</option>";
						for($i = 0; $i < 12;$i++){
						while(odbc_fetch_row($rs)){
					$i = odbc_result($rs,"mes");
						if($i == $mesc){
								echo "<option value='$i' selected>$mes[$i]</option>";
							}
							else{
								echo "<option value='$i'>$mes[$i]</option>";
							}
				}
	
						}			
			echo "</select> ";
			echo "</td></tr>
			<tr>
			<th colspan='3'><b style='float:right;font-size: 9px;padding-top: 10px;' id='Atualizacao'>Horário de atualização $atualiza</b></th>
			</tr>
			</table>";

		break;
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		case 'consulta':

			$conn=odbc_connect('MISPG','','');

			if(isset($_GET['gerente'])){
				$gerente = $_GET['gerente'];
			}
			else{
				$gerente = '';
			};

			if(isset($_GET['ccusto'])){
				$ccusto = $_GET['ccusto'];
			}
			else{
				$ccusto = '';
			};

			if(isset($_GET['mes'])){
				$mes = $_GET['mes'];
			}
			else{
				$mes = '';
			};

			$strSql = "
					SELECT 
					gerente,
					grupo,
					tbl_produtividade_agente_dia_2013.nome_ronda as nome,
					tbl_produtividade_agente_dia_2013.matricula as matricula,
					SUM(faltas) as faltas,
					SUM(escala) as escala, 
					CASE WHEN SUM(escala) <> 0 THEN ROUND(CAST(SUM(faltas) AS numeric)/CAST(SUM(escala) as numeric)*100,2)::text||'%' ELSE '0%' END as absenteismo,
					CASE WHEN SUM(qtd_monitorias) <> 0 THEN ROUND((CAST(SUM(nota*qtd_monitorias) AS numeric)/CAST(SUM(qtd_monitorias) as numeric)),2) else 0 END as qualidade 
					  FROM tbl_produtividade_agente_dia_2013
					INNER JOIN tbl_ccm7_hierarquia
						ON tbl_ccm7_hierarquia.cod_re_rh = tbl_produtividade_agente_dia_2013.matricula
					INNER JOIN tbl_ronda_hierarquia_2013
						on tbl_produtividade_agente_dia_2013.matricula = tbl_ronda_hierarquia_2013.matricula
					  WHERE  
					    escala IS NOT NULL
						and extract('year' from data) = 2013 ";

						if($mes != ''){ 
							$strSql = $strSql."and extract('month' from data) = $mes ";
						}

						if ($ccusto != ''){
							$strSql = $strSql."and ccusto = '$ccusto' ";
						}

						if ($gerente != '') { 
							$strSql = $strSql."and gerente = '$gerente' ";
						}

				$strSql = $strSql."
					  GROUP BY tbl_produtividade_agente_dia_2013.nome_ronda,tbl_produtividade_agente_dia_2013.matricula,grupo,gerente
			";

			$rs=odbc_exec($conn,$strSql);

		//Array com os indicadores utilizados na tabela
			$indicador = array("grupo","nome","matricula","faltas","escala","absenteismo","qualidade");

			echo "<table class='fancyTable' id='myTable02' name='myTable02' style='font-size: 10px;'>";
			
			echo "
				<thead>
					<td style='text-align:center;width: 300px;cursor:pointer;'>Centro de custo</td>
					<td style='text-align:center;width: 50px;cursor:pointer;'>Nome</td>
					<td style='text-align:center;cursor:pointer;'>Matrícula</td>
					<td style='text-align:center;cursor:pointer;'>Faltas</td>
					<td style='text-align:center;cursor:pointer;'>Escala</td>
					<td style='text-align:center;cursor:pointer;'>Absenteísmo</td>
					<td style='text-align:center;cursor:pointer;'>Qualidade</td>
				</thead>
			";

		//rotina de impressao da tabela
			while(odbc_fetch_row($rs)){
				echo "<tr>";
					for($i = 0 ; $i < sizeof($indicador); $i++){
						if("$indicador[$i]" == "faltas"){
							$resultado = odbc_result($rs,"$indicador[$i]");
						//Cada 'if' define as cores, baseado nos parametros informados por Aline Costa
							if(odbc_result($rs,"$indicador[$i]") == 0){
								echo "<td style='text-align:center;color:green'>$resultado</td>";
							}
							else{
								echo "<td style='text-align:center;color:red'>$resultado</td>";
							}

							}
							elseif("$indicador[$i]" == "nome"){
							$resultado = utf8_encode(odbc_result($rs,"$indicador[$i]"));
								echo "<td style='text-align:left;'>$resultado</td>";
						}
						elseif("$indicador[$i]" == "qualidade"){
							$resultado = odbc_result($rs,"$indicador[$i]");
							if($resultado >= 7){
								echo "<td style='text-align:center;color:green'>$resultado</td>";
							}
							else{
								echo "<td style='text-align:center;color:red'>$resultado</td>";
							}
						}
						else{
						//utf8_encode para exibicao dos acentos corretamente
							$resultado = utf8_encode(odbc_result($rs,"$indicador[$i]"));
							echo "<td style='text-align:center;'>$resultado</td>";
						}
						
					}
				echo "</tr>";
			}
			echo "</table>";

		break;
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
	
	}

	if(isset($conn)){
	odbc_close($conn);
	};	
?>
<link href="http://172.23.14.155/mis/node/css/myTheme.css" rel="stylesheet" media="screen">

<style>

	table{
		width: 100%;
		min-width:480px;
	}
	
	ul.legenda{
		list-style: none;
		float: left;
		width: 50%;
		-webkit-margin-before: 0;
		-webkit-margin-after: 0;
		-webkit-margin-start: 0px;
		-webkit-margin-end: 0px;
		-webkit-padding-start: 0;
		margin-right: 10px;
		color: #215867;
		margin-bottom: 20px;
	}
	
	div.aviso{
	
		width: 30%;
		float: right;
		padding: 5px;
		color: #215867;
		background-color: #F5D7CB;
		border: 1px solid #F3BCAB;
		
	}
	
</style>

<?php
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		function min_volume_bm($intervalo){
			
			$dicintervalo = array(
							'00:00:00'=>0.0994347996128309,'00:30:00'=>0.17631997116869,'01:00:00'=>0.237073098545398,
							'01:30:00'=>0.284361883276481,'02:00:00'=>0.323748231389384,'02:30:00'=>0.35764817446164,
							'03:00:00'=>0.387949237163196,'03:30:00'=>0.416362775194805,'04:00:00'=>0.442687452591672,
							'04:30:00'=>0.471428161566072,'05:00:00'=>0.507844804198258,'05:30:00'=>0.559990819080005,
							'06:00:00'=>0.647169291835978,'06:30:00'=>0.790797335721178,'07:00:00'=>1.06244988622001,
							'07:30:00'=>1.49159749517926,'08:00:00'=>2.38983273511385,'08:30:00'=>3.94912944845523,
							'09:00:00'=>6.07088334644534,'09:30:00'=>8.63545053200512,'10:00:00'=>11.3808929149371,
							'10:30:00'=>14.2009051058297,'11:00:00'=>16.9588554855745,'11:30:00'=>19.5739831652933,
							'12:00:00'=>21.7651224701507,'12:30:00'=>23.6287133272832,'13:00:00'=>25.4447637297286,
							'13:30:00'=>27.4091987382275,'14:00:00'=>29.5561196318169,'14:30:00'=>31.8473732451683,
							'15:00:00'=>34.0658690742421,'15:30:00'=>36.2551208544296,'16:00:00'=>38.4003807263094,
							'16:30:00'=>40.4758273523968,'17:00:00'=>42.3762375240911,'17:30:00'=>43.9762292693166,
							'18:00:00'=>45.1571653708181,'18:30:00'=>46.0022479160469,'19:00:00'=>46.704683930219,
							'19:30:00'=>47.3411320819297,'20:00:00'=>47.8956113289734,'20:30:00'=>48.3857134012742,
							'21:00:00'=>48.801950341491,'21:30:00'=>49.1464110102586,'22:00:00'=>49.4392290040564,
							'22:30:00'=>49.6718223773851,'23:00:00'=>49.8581588128024,'23:30:00'=>50
					);
						
			return $dicintervalo[$intervalo];
		}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		function abd_cor($numero){
			
			$numero = floatval(str_replace(",",".",str_replace("%","",$numero)));
			
			if($numero >= 10){
				return "red";
			}
			else{
				return "";
			}
			
		}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		function cabecalho($id, $titulo){
		
			echo "<table class='fancyTable' id='myTable02' name='myTable02' ><thead><tr><td style='font-size: 16px;
																									font-weight: bold;
																									color: #215867;'>
					$titulo
					</td></tr></thead></table>";
					
					echo "<table class='fancyTable' id='myTable02' name='myTable02' style='font-size: 10px;'>
								<thead>
									<tr>";
									if($id == 0){
										echo "	<th >�REA/FILA</th>
												<th >REC</th>
												<th > NS - 60''</th>
												<th > (%) ABD Aceit�vel</th>
												<th > (%) ABD</th>
												<th > TMAXESP</th>";
									}
									else{									
									echo"	<th > �REA/FILA</th>
											<th > REC</th>
											<th > ATD</th>
											<th > ABD > 5</th>
											<th > TMO</th>
											<th > NS 60''</th>
											<th > (%) ABD</th>";
									}
					echo"		</tr>
								</thead><tbody>";
								
		
		}
		
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		function tbl_1($titulo,$indicadores,$cores,$esp,$rs){
		
					cabecalho(0, $titulo);
					
					while(odbc_fetch_row($rs)){
					
						echo "<tr>";
						
						$resultado = utf8_encode(odbc_result($rs,$indicadores[0]));
						echo "<td style='width:30%;'>$resultado</td>";
						
						for ($i = 1; $i < sizeof($indicadores); $i++) {
							$resultado = utf8_encode(odbc_result($rs,$indicadores[$i]));
											
							if($indicadores[$i] == 'abd'){
								echo "<td style='color:red;text-align:center'>$resultado</td>";
							}
							else{
								echo "<td style='text-align:center'>$resultado</td>";
							}
						}
						
						echo "</tr>";

					};
					
					echo "</tbody></table>";
					
					echo "</br>";
					
		};
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		function tbl_2($titulo,$indicadores,$cores,$esp,$rs){
		
			cabecalho(1, $titulo);
			
			while(odbc_fetch_row($rs)){
			
				echo "<tr>";
				
				$cor = $cores[odbc_result($rs,$indicadores[0])-1];
				$pl = $esp[odbc_result($rs,$indicadores[0])-1];
				$resultado = utf8_encode(odbc_result($rs,$indicadores[1]));
				echo "<td style='width:30%;background-color:$cor;padding-left:$pl'>$resultado</td>";	
						
				for ($i = 2; $i < sizeof($indicadores); $i++) {
					$resultado = utf8_encode(odbc_result($rs,$indicadores[$i]));
					
					if($indicadores[$i] == "abd_perc"){
						$cortexto = abd_cor($resultado);
					}
					else{
						$cortexto = "";
					}
					
						echo "<td style='background-color:$cor;text-align:center;color:$cortexto;'>$resultado</td>";
				}
				
				echo "</tr>";

			};
			
			echo "</tbody></table>";
			echo "</br>";
					
		};
		
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		function tbl_3($titulo,$indicadores,$cores,$esp,$rs,$fonte){
		
			cabecalho(1, $titulo);
			
			while(odbc_fetch_row($rs)){
			
				echo "<tr>";
				
				$cor = $cores[odbc_result($rs,$indicadores[0])-1];
				$pl = $esp[odbc_result($rs,$indicadores[0])-1];
				$letra = $fonte[odbc_result($rs,$indicadores[0])-1];
				
				$resultado = utf8_encode(odbc_result($rs,$indicadores[1]));
				
				echo "<td style='width:30%;background-color:$cor;padding-left:$pl;$letra;'>$resultado</td>";	
						
				for ($i = 2; $i < sizeof($indicadores); $i++) {
					$resultado = utf8_encode(odbc_result($rs,$indicadores[$i]));
					
					if($indicadores[$i] == "abd_perc"){
						$cortexto = abd_cor($resultado);
					}
					else{
						$cortexto = "";
					}
					
						echo "<td style='background-color:$cor;text-align:center;$letra;color:$cortexto;'>$resultado</td>";
				}
				
				echo "</tr>";

			};
			
			echo "</tbody></table>";
					
		};
		
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		$conn=odbc_connect('MISPG','','');

		$sql = "SELECT servidor, dac, min(data_insert) 
			FROM tbl_avaya_extracao_info 
			WHERE relatorio = 'hsplit' 
			GROUP BY servidor, dac 
			ORDER BY servidor, dac ";
		$rs=odbc_exec($conn,$sql);
		
		$at_tempo = date('H:i:s',strtotime(odbc_result($rs,'min')));
		$hoje = date("d/m/y");
		$intervalo = min_volume_bm($at_tempo);
		
		$tab = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
		$tab2 = "&nbsp&nbsp&nbsp&nbsp&nbsp";
	
	//Imprime titulo e legenda
	
		echo "<table class='fancyTable' id='myTable02' name='myTable02' ><thead><tr><td style='font-size: 16px;
																							font-weight: bold;
																							color: #215867;'>
        Boletim Performance
		</td></tr></thead></table>";
		
		echo "
			</br>
			<div style='font-size:14px;'>
			<ul class='legenda'>
				<li><b>REC:</b>$tab Liga��es recebidas</li>
				<li><b>ATD:</b>$tab Liga��es atendidas</li>
				<li><b>ABD:</b>$tab Liga��es abandonadas ap�s aguardar por mais de 5 segundos na fila de espera</li>
				<li><b>NS 60:</b>$tab2% de liga��es atendidas ap�s aguadar por mais de 60 segundos na fila de espera</li>
				<li><b>(%) ABD:</b> %de liga��es abandonadas ap�s aguardar por mais de 5 segundos na fila de espera</li>
			</ul>
			<div class='aviso'>
			<u><b>Importante:</b></u> Acompanhe, anexo,diagn�stico dos resultados no <a href='node\xls\Boletim Performance (TM).xlsx'>link</a> An�lises.
			</div>
			</div>
		";
	
	//Consulta da tbl_1
	
		$sql = "SELECT * FROM proc_boletim_tbl1_2013($intervalo)";
		$rs=odbc_exec($conn,$sql);
		$indicadores = array('fila','rec','ns60','abd_ac','abd','tmax');
		$cores = array('#b3c8e2','#d8e3f0','','','');
		$esp = array('0%','2%','5%','7%','9%');
		
	//Ranking de abandono
	
		tbl_1("Telefone - Ranking de abandono maior que aceit�vel - $hoje - Atualizado at�: $at_tempo",$indicadores,$cores,$esp,$rs);
		
	//Consulta tbl_2
	
		$sql = "SELECT * FROM proc_boletim_tbl2_2013()";
		$rs=odbc_exec($conn,$sql);
		$indicadores = array('nivel','fila','rec','atd','abd','tmo','n60','abd_perc');
		
	//Quadro resumo
	
		tbl_2("Quadro Resumo - Acumulado do dia $hoje  - Atualizado at�: $at_tempo",$indicadores,$cores,$esp,$rs);
		
	//Consulta tbl_3
	
		$sql = "SELECT * FROM proc_boletim_tbl3_2013()";
		$rs=odbc_exec($conn,$sql);
		
		$cores = array('#b3c8e2','#b3c8e2','#d8e3f0','','#ffffff');
		$fonte = array('','','','font-weight: bold;','');
		$esp = array('0%','0%','0%','2%','4%');
		
	//Quadro detalhado
	
		tbl_3("Quadro Resumo - Acumulado do dia $hoje  - Atualizado at�: $at_tempo",$indicadores,$cores,$esp,$rs,$fonte);

	if(isset($conn)){
		odbc_close($conn);
	};
?>
<?PHP
require_once("pass/include/membersite_config.php");

if(!$fgmembersite->CheckLogin())
{
    $fgmembersite->RedirectToURL("index.php");
    exit;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
 <html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<title>Portal dos Núcleos</title>
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
    <script src="msg/js/editablegrid-2.0.1.js"></script>   
		<!-- I use jQuery for the Ajax methods -->
		<script src="msg/js/jquery-1.7.2.min.js" ></script>
		<script src="msg/js/demo.js" ></script>
		<script type="text/javascript">
			window.onload = function() { 
				datagrid = new DatabaseGrid();
			};        
  
</script>
	<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
	<script type="text/javascript" src="js/jquery.jcarousel.js"></script>
	<script src="js/cufon-yui.js" type="text/javascript" charset="utf-8"></script>
	<script src="js/Chaparral_Pro.font.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="js/jquery-func.js"></script>
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />

</head>
<body>

<!-- Featured -->
<div id="featured">
	<!-- Shell -->
	<div class="shell">
		<!-- Slider-carousel -->
		<div class="slider-carousel">
			<ul>
				<li>
					<div class="info">
			    		<h2>Sejam Bem Vindos</h2>
			    		<p>"Leve na sua memória para o resto de sua vida, as coisas boas que surgiram no meio das dificuldades. Elas serão uma prova de sua capacidade em vencer as provas e lhe darão confiança na presença divina, que nos auxilia em qualquer situação, em qualquer tempo, diante de qualquer obstáculo." - <i>Chico Xavier</i></p>
			    		<p>A Porto Seguro deseja Boas Vindas</p>
			    	<div class="leia-mais">
						<a class="leia-mais" href="#">Saiba Mais</a>
					</div>
					</div>
					<div class="image">
			    		<a href="#"><img src="images/bem_vindos.jpg" alt="Bem Vindo!" /></a>
			    	</div>
					<div class="cl">&nbsp;</div>
				</li>
				
				<li>
					<div class="info">
			    		<h2>Dicas de Postura</h2>
			    		<p>Quando você está à frente do computador, naturalmente você deixa de piscar os olhos e isso pode causar dores de cabeça, dor nos olhos, xeroftalmia (ressecamento dos olhos), e lacrimejamento. Para que isso não ocorra, a cada 15 minutos trabalhados, pare 15 segundos e pisque os olhos. Também olhe para locais mais longes para ter outro foco.</p>
			    	<div class="leia-mais">
						<a class="leia-mais" href="#">Leia Mais ..</a>
					</div>
					</div>
					<div class="image">
			    		<a href="#"><img src="images/ginastica.jpg" alt="slide image" /></a>
		    	  </div>
					<div class="cl">&nbsp;</div>
				</li>
				
				<li>
					<div class="info">
			    		<h2>Treinamento</h2>
			    		<p>Além do Treino básico exigido por um ofício, ocupação ou profissão, os avanços tecnológicos e a competitividade do mundo moderno exigem que os trabalhadores atualizem constantemente suas habilidades, ao longo de toda sua vida profissional.</p>
			    		<p>Vamos ao próximo treinamento?</p>
			    	<div class="leia-mais">
						<a class="leia-mais" href="#">Learn More</a>
					</div>
					</div>
					<div class="image">
			    		<a href="#"><img src="images/treinamento.jpg" alt="slide image" /></a>
		    	  </div>
					<div class="cl">&nbsp;</div>
				</li>
				
				
				
			</ul>
		</div>	
		<!-- FIM Slider-Carousel -->
	</div>
	<!-- FIM Shell -->	
</div>
<!-- FIM Featured -->

<!-- Principal -->
<div id="principal">
	<!-- Shell -->
	<div class="shell">
		
	<!-- Main-Boxes -->
	<div id="principal-boxes">		
			<!-- Box -->
			<div class="box">
				<span class="left-arrow">&nbsp;</span>
				<h3>Porto Seguro</h3>
				<div class="box-conteudo">
					<ul class="link-list">
					    
					     <li><a href="http://novaportonet/portal/site/SocialIntranet/template.PAGE/home/resolver.vcarp/?javax.portlet.ctx_vca=scope%3Dsite%26appmodes%3Dgroup%26group%3D1.112.122%26collectionOid%3D1.11.1039%26apptypes%3Dsocialgroup&javax.portlet.tpst=2a9f53ab2f771f16f0d80fa41382139c&javax.portlet.begCacheTok=com.vignette.cachetoken&javax.portlet.endCacheTok=com.vignette.cachetoken&vgnextoid=a678697f720e2310VgnVCM100000b80416acRCRD&vgnextfmt=default" target="_blank">Base do Conhecimento</a></li>
                         <li><a href="http://novaportonet/PortoNetIntranet/Manuais-e-Circulares" target="_blank">Manuais e Circulares</a></li>
                        <li><a href="http://www.portoseguro.com.br/" target="_blank">Site Porto Seguro</a></li>
                        <li><a href="http://www.portoseguroservicosavulsos.com.br/" target="_blank">Serviços Avulsos</a></li>
					    
					</ul>
				</div>
			</div>
			<!-- FIM Box -->
			
			<!-- Box -->
			<div class="box">
				<span class="left-arrow">&nbsp;</span>
				<h3>Azul Seguros</h3>
				<div class="box-conteudo">
					<ul class="link-list">
					    <li><a href="http://novaportonet/portal/site/SocialIntranet/template.PAGE/home/resolver.vcarp/?javax.portlet.ctx_vca=scope%3Dsite%26appmodes%3Dgroup%26group%3D1.112.123%26collectionOid%3D1.11.1039%26apptypes%3Dsocialgroup&javax.portlet.tpst=2a9f53ab2f771f16f0d80fa41382139c&javax.portlet.begCacheTok=com.vignette.cachetoken&javax.portlet.endCacheTok=com.vignette.cachetoken&vgnextoid=a678697f720e2310VgnVCM100000b80416acRCRD&vgnextfmt=default" target="_blank">Base de Conhecimento</a></li> <li><a href="http://www.azulseguros.com.br/voce/home.cfm" target="_blank">Site Azul Seguros</a></li>
					    <li><a href="http://intranet.azulseguros.com.br/" target="_blank">Intranet Azul Seguros</a></li>
					    <li><a href="http://32.94.155.68/acesso/" target="_blank">Sistema de Sinistro</a></li>					   
                        <li><a href="https://audatex.azulseguros.com.br:447/AudatexWeb/Audatex_LogIn.aspx?ReturnUrl=%2fAudatexWeb%2faudatex_home.aspx" target="_blank">Audatex</a></li>
					</ul>
				</div>
			</div>
			<!-- FIM Box -->
		
			<!-- Box -->
			<div class="box box-last">
				<span class="left-arrow">&nbsp;</span>
				<h3>Itau Seguros</h3>
				<div class="box-conteudo">
					<ul class="link-list">
					   
                        <li><a href="http://novaportonet/portal/site/SocialIntranet/template.PAGE/home/resolver.vcarp/?javax.portlet.ctx_vca=scope%3Dsite%26appmodes%3Dgroup%26group%3D1.112.126%26collectionOid%3D1.11.1039%26apptypes%3Dsocialgroup&javax.portlet.tpst=2a9f53ab2f771f16f0d80fa41382139c&javax.portlet.begCacheTok=com.vignette.cachetoken&javax.portlet.endCacheTok=com.vignette.cachetoken&vgnextoid=a678697f720e2310VgnVCM100000b80416acRCRD&vgnextfmt=default" target="_blank">Base de Conhecimento</a></li>
					    <li><a href="https://ww39.itau.com.br/j140/operacional/logintam.jsp" target="_blank">Portal Itaú Seguros</a></li>
					    <li><a href="http://www.itau.com.br/" target="_blank">Site Itaú</a></li>
					    
					</ul>
				</div>
      </div> 
			<!-- FIM Box -->
			<div class="cl">&nbsp;</div>

	</div>
	<!-- FIM Principal-boxes -->
	
	<!-- Conteúdo-->
	<div id="conteudo">
		
		<!-- Box -->
		<div class="box">
			<h4>Sobre a Porto Seguro</h4>
			<div class="box-conteudo">
				<img src="images/logo_porto_seguro.png" width="86" height="112"  alt="Porto Seguro" />
			  <p>A Porto Seguro atua em todos os ramos de Seguros, Patrimoniais e de Pessoas, que são complementados por outros negócios sinérgicos com a atividade principal: Automóvel, Saúde Empresarial, Patrimonial, Vida e Transportes, Previdência, Consórcio de Imóveis e Automóveis, Administração de Investimentos, Financiamento, Capitalização e Cartão de Crédito, Proteção e Monitoramento, Serviços a Condomínios e Residências e Telecomunicações.</p>
                <p>A sede da Porto Seguro está localizada em Campos Elísios, na região central da cidade de São Paulo.</p>
                
			  <p class="anchor"><a class="button-small" href="http://www.portoseguro.com.br/a-porto-seguro/conheca-a-porto-seguro/sobre-a-porto-seguro" target="_blank">Saiba mais</a></p>
			</div>
			
		</div>
		<!-- FIM Box -->
		
	</div>
	<!-- FIM Conteúdo-->
	
		<!-- Sidebar -->
			<div id="sidebar">
				<h4>Mensagens</h4>
				<div class="quote">
					<div id="wrap"><!-- Feedback message zone -->
                     <div id="message"></div><!-- Grid contents -->    
                     <br /><br />               
                     <div id="tablecontent"></div>
                   <!-- Paginator control -->                       
                     <div id="paginator"></div>
                  </div>
				   </div> <p class="author">Rui Pereira<span>Gerente de Desenvolvimento de Atendimento</span></p>
                   
			   </div>
		<!-- FIM Sidebar -->
        
	<!-- Shell -->
</div>

		<div class="cl">&nbsp;</div>
</div>
<!-- FIM Principal -->
</body>
</html>
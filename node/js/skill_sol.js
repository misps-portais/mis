var resposta //Variavel global
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
 function validate(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|\;/;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  function objXml (){
    var objXml;
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        var objXml =new XMLHttpRequest();
      }
    else {
        // code for IE6, IE5
        var objXml =new ActiveXObject("Microsoft.XMLHTTP");
      }
    return objXml
  }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  function radioSkill(stracao) {
    if (stracao==4) {
        consultaNomenclatura();
    }
    else {
        consultaFormulario();
    }
  }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  function novo(strcampo, strvalue) {
    if (strvalue=="off") {
      document.getElementById("novo_" + strcampo).style.display = "inline";
      //document.getElementById(strcampo).disabled = true;
      document.getElementById("novo_" + strcampo).disabled = false;
      document.getElementById("img_"+strcampo).name = "on";
      document.getElementById(strcampo).getElementsByTagName('option')[ document.getElementById(strcampo).options.length-1].selected = 'selected'
    }
    else {
      document.getElementById("novo_" + strcampo).style.display = "none";
      //document.getElementById(strcampo).disabled = false;
      document.getElementById("novo_" + strcampo).disabled = true;
      document.getElementById("img_"+strcampo).name = "off";
      document.getElementById(strcampo).getElementsByTagName('option')[0].selected = 'selected'
    }
  }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  function validaSkill() {

    var radios = document.getElementsByName('rdskill');
    var serv = ['','10.221.236.101','','172.27.203.111','192.167.235.76'];

    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            solicitacao = radios[i].value;
            break;
        }
    }

      var xmlhttp = objXml()

      xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
          resposta = xmlhttp.responseText;
        }
      }

      var servidor = document.getElementById("servidor").options[document.getElementById("servidor").selectedIndex].value;
      var dac = document.getElementById("dac").options[document.getElementById("dac").selectedIndex].value;
      var skill = document.getElementById("novo_skill").value.split(";");

      for (var i = 0; i <= skill.length; i++) {
        skillTemp = skill[i];

        if(servidor != '' && dac != '' && skillTemp !=''){
           xmlhttp.open("GET","node/php/skills_func.php?cs=" + Math.round((new Date()).getTime() / 1000) + 
                                   "&&skill=" + skillTemp + "&&dac=" + dac + "&&servidor=" + servidor + "&&solicitacao="+solicitacao + "&&acao=valida_skill",false);
           xmlhttp.send();

            if(solicitacao == 2 && resposta == 0 || solicitacao == 3 && resposta == 0){
              alert('Verificar skill ' + skillTemp + ' não existente no servidor '+serv[servidor]+' e dac ' + dac);
              document.getElementById('enviar').disabled = true;
              document.getElementById('enviar').type = 'button';
              document.getElementById('avisoEnviar').style.display = '';
              document.getElementById('avisoEnviar').innerHTML = '*Verificar skill ' + skillTemp + ' não existente no servidor '+serv[servidor]+' e dac ' + dac;
              break;
            }
            else if(solicitacao == 1 && resposta == 1){
              alert('Verificar skill ' + skillTemp + 'servidor '+serv[servidor]+' e dac ' + dac +' já existe na tabela');
              document.getElementById('enviar').disabled = true;
              document.getElementById('enviar').type = 'button';
              document.getElementById('avisoEnviar').style.display = '';
              document.getElementById('avisoEnviar').innerHTML = '*Verificar skill ' + skillTemp + 'servidor '+serv[servidor]+' e dac ' + dac +' já existe na tabela';
              break;
            }
            else{
              document.getElementById('enviar').disabled = false;
              document.getElementById('enviar').type = 'submit';
              document.getElementById('avisoEnviar').style.display = 'none';
            }

        }        

      };

    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  function filtraNomenclatura(){

    var ul = document.getElementById("busca");
    var items = ul.getElementsByTagName("li");
    var filtro = document.getElementById("campoBusca").value.toLowerCase();
    for (i = 0; i < items.length; ++i) {
      var nomeAtual = items[i].innerHTML.toLowerCase()
      if(nomeAtual.match(filtro) == null){
        items[i].style.display = 'none';
      }
      else{
        items[i].style.display = '';
      }
    }


  }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  function consultaAjuda(i){

    document.getElementById('ajuda').style.display = '';
    var xmlhttp = objXml()

    for (j = 1; j <= 3; j++) {
      if(j != i){
        document.getElementById("ajuda" + j).style.display = "none";
      }
    };

    xmlhttp.onreadystatechange=function() {
      if (xmlhttp.readyState==4 && xmlhttp.status==200) {
        if (document.getElementById("ajuda" + i).style.display == "none") {
          document.getElementById("ajuda" + i).style.display = "inline-flex";
        }
        else{
          fechaAjuda(i);
        }
        document.getElementById("ajuda" + i).innerHTML = xmlhttp.responseText;
      }
    }

    xmlhttp.open("GET","node/php/skills_func.php?acao=ajuda&&campo=" + i,true);

    xmlhttp.send();

  }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  function fechaAjuda(i){

    document.getElementById('ajuda' + i).style.display = 'none';
  }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  function consultaNomenclatura(){

    document.getElementById('mais_controles').style.display = '';
    document.getElementById("corpo_solicitacao").innerHTML = '<center><img src="node/img/loading.gif"></img></center>';
    var xmlhttp = objXml()

    xmlhttp.onreadystatechange=function() {
      if (xmlhttp.readyState==4 && xmlhttp.status==200) {
        document.getElementById("corpo_solicitacao").innerHTML = xmlhttp.responseText;
      }
    }

    xmlhttp.open("GET","node/php/skills_func.php?acao=nome",true);

    xmlhttp.send();
  }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  function consultaFormulario(){

    document.getElementById('enviar').disabled = false;
    document.getElementById('enviar').type = 'submit';
    document.getElementById('avisoEnviar').style.display = 'none';
    document.getElementById('mais_controles').style.display = '';
    document.getElementById("corpo_solicitacao").innerHTML = '<center><img src="node/img/loading.gif"></img></center>';
    var xmlhttp = objXml()

    xmlhttp.onreadystatechange=function() {
      if (xmlhttp.readyState==4 && xmlhttp.status==200) {
        document.getElementById("corpo_solicitacao").innerHTML = xmlhttp.responseText;
      }
    }

    xmlhttp.open("GET","node/php/skills_func.php?acao=form",true);

    xmlhttp.send();

  }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  function consultaCategoria(tabela){

    document.getElementById("nome_altera").innerHTML = '<center><img src="node/img/loading.gif"></img></center>';
    var xmlhttp = objXml()

    xmlhttp.onreadystatechange=function() {
      if (xmlhttp.readyState==4 && xmlhttp.status==200) {
        document.getElementById("nome_altera").innerHTML = xmlhttp.responseText;
      }
    }

    xmlhttp.open("GET","node/php/skills_func.php?acao=nome_cat&&cat="+tabela,true);

    xmlhttp.send();

  }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  function buscaDac(){

    var servidor = document.getElementById("servidor").options[document.getElementById("servidor").selectedIndex].value;

    document.getElementById("dac").disabled = false;

    switch(servidor)
    {
    case '3':
      document.getElementById("dac").innerHTML = '<option>Dac</option><option value=1>Porto Seguro</option><option value=2>Azul_Seguros</option><option value=3>Core Regionais</option>';
    break;
    case '1':
      document.getElementById("dac").innerHTML = '<option>Dac</option><option value=2>DAC06_TSM</option>';
    break;
    case '4':
      document.getElementById("dac").innerHTML = '<option>Dac</option><option value=1>CM_CON_SB_DL360</option>';
    break;
    }

  }

  
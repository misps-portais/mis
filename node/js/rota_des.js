//OBS: O PARAMETRO CS - Vulgo Cache Slayer E UTILIZADO PARA EVITAR QUE AS PAGINAS FIQUEM EM CACHE
//UTILIZANDO O TIMESTAMP DO ACESSO, GERANDO ASSIM CADA VEZ UM NUMERO DIFERENTE

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function objxml (){
		var objxml;
		if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		var objxml =new XMLHttpRequest();
		}
		else {
		// code for IE6, IE5
		var objxml =new ActiveXObject("Microsoft.XMLHTTP");
		}
		return objxml
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function data_inicio( turma,acao) {
		var xmlhttp = objxml()
		xmlhttp.onreadystatechange=function() {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				document.getElementById("inicio_dt").innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","node/php/escola_atd.php?cs=" + Math.round((new Date()).getTime() / 1000) +
														"&&acao="+ acao + 
														"&&turma=" + turma,true);
		xmlhttp.send();
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	function atualiza() {

		var xmlhttp = objxml()

		xmlhttp.onreadystatechange=function() {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				document.getElementById("dados").innerHTML = xmlhttp.responseText;
				sorttable.makeSortable(document.getElementById("myTable02"));
			}
		}
		
		var gerente = document.getElementById("gerente").options[document.getElementById("gerente").selectedIndex].value

		if (gerente == 'SONIA RICA'){

			gerente = 'CARLA MACIEIRA';
		};

		var ccusto = document.getElementById("ccusto").options[document.getElementById("ccusto").selectedIndex].value;
		var mes = document.getElementById("mes").options[document.getElementById("mes").selectedIndex].value;
		
		xmlhttp.open("GET","node/php/rota.php?cs=" + Math.round((new Date()).getTime() / 1000) +
														"&&acao=consulta&&gerente=" + gerente + 
														"&&ccusto=" + ccusto + "&&mes=" + mes,true);
		xmlhttp.send();
		
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	function filtros() {

		var xmlhttp = objxml()

		xmlhttp.onreadystatechange=function() {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				document.getElementById("consulta").innerHTML = xmlhttp.responseText;
				sorttable.makeSortable(document.getElementById("myTable02"));
			}
		}
		
		var gerente = document.getElementById("gerente").options[document.getElementById("gerente").selectedIndex].value
		
		if (gerente == 'SONIA RICA'){

			gerente = 'CARLA MACIEIRA';
		};

		var ccusto = document.getElementById("ccusto").options[document.getElementById("ccusto").selectedIndex].value;
		var mes = document.getElementById("mes").options[document.getElementById("mes").selectedIndex].value;

		xmlhttp.open("GET","node/php/rota.php?cs=" + Math.round((new Date()).getTime() / 1000) +
														"&&acao=filtro_cc&&gerente=" + gerente + 
														"&&ccusto=" + ccusto+ "&&mes=" + mes,true);
		xmlhttp.send();
		
	}
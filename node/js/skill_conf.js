//OBS: O PARAMETRO CS E UTILIZADO PARA EVITAR QUE AS PAGINAS FIQUEM EM CACHE
//UTILIZANDO O TIMESTAMP DO ACESSO, GERANDO ASSIM CADA VEZ UM NUMERO DIFERENTE

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function objxml (){
		var objxml;
		if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		var objxml =new XMLHttpRequest();
		}
		else {
		// code for IE6, IE5
		var objxml =new ActiveXObject("Microsoft.XMLHTTP");
		}
		return objxml
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function consulta(id,offset) {
		
		var xmlhttp = objxml()
		
		xmlhttp.onreadystatechange=function() {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				document.getElementById("conteudo").innerHTML = xmlhttp.responseText;
			}
		}
		
		xmlhttp.open("GET","node/php/skill_conf.php?cs=" + Math.round((new Date()).getTime() / 1000) + "&&acao=consulta&&id=" + id +"&&offset=" + offset,true);
		
		xmlhttp.send();
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function consulta2(id,offset) {
		
		var xmlhttp = objxml()
		
		xmlhttp.onreadystatechange=function() {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				document.getElementById("conteudo").innerHTML = xmlhttp.responseText;
			}
		}
		
		xmlhttp.open("GET","node/php/skill_acom.php?cs=" + Math.round((new Date()).getTime() / 1000) + "&&acao=consulta&&id=" + id +"&&offset=" + offset ,true);
		
		xmlhttp.send();
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	function ler(id) {
		var xmlhttp = objxml()
		xmlhttp.onreadystatechange=function() {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				document.getElementById("conteudo").innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","node/php/skill_conf.php?cs=" + Math.round((new Date()).getTime() / 1000) + "&&id=" + id ,true);
		xmlhttp.send();
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function resp(acao,id) {

		var xmlhttp = objxml()
		xmlhttp.onreadystatechange=function() {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				document.getElementById("conteudo").innerHTML = xmlhttp.responseText;
			}
		}
		
		xmlhttp.open("GET","node/php/skill_conf.php?cs=" + Math.round((new Date()).getTime() / 1000) +
													"&&acao="+ acao +"&&id=" + id,true);
		xmlhttp.send();

	}	

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  function marcaTodos(){

    var tabela = document.getElementById("myTable02");
    var items = tabela.getElementsByTagName("input");
    for (i = 0; i < items.length; ++i) {
		items[i].checked = document.getElementById("marcatudo").checked;
    }

  }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function seletorAcao(){

	    var tabela = document.getElementById("myTable02");
	    var items = tabela.getElementsByTagName("input");
	    for (i = 0; i < items.length; ++i) {
			if(items[i].checked){
				resp(document.getElementById("acao").options[document.getElementById("acao").selectedIndex].value,items[i].id);
			}
	    }

	}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function buscaSolicitacao(id,offset){

		var xmlhttp = objxml()
		
		xmlhttp.onreadystatechange=function() {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				document.getElementById("conteudo").innerHTML = xmlhttp.responseText;
			}
		}
		
		var id_sol = document.getElementById("campoBusca").value;

		xmlhttp.open("GET","node/php/skill_conf.php?cs=" + Math.round((new Date()).getTime() / 1000) + "&&acao=busca&&id=" + id +"&&offset=" + offset + "&&id_sol=" + id_sol,true);
		
		xmlhttp.send();

	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function vazio(id){

		document.getElementById(id).value = "";

	}
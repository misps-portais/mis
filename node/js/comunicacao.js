﻿//OBS: O PARAMETRO CS E UTILIZADO PARA EVITAR QUE AS PAGINAS FIQUEM EM CACHE
//UTILIZANDO O TIMESTAMP DO ACESSO, GERANDO ASSIM CADA VEZ UM NUMERO DIFERENTE

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function objxml (){
		var objxml;
		if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		var objxml =new XMLHttpRequest();
		}
		else {
		// code for IE6, IE5
		var objxml =new ActiveXObject("Microsoft.XMLHTTP");
		}
		return objxml
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function enviar(nome, dest, assunto, sub, titulo, mensagem,acao) {
		var xmlhttp = objxml()
		xmlhttp.onreadystatechange=function() {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				document.getElementById("conteudo").innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","node/php/comunicacao.php?cs=" + Math.round((new Date()).getTime() / 1000) +				
													"&&acao=" + acao + 
													"&&usuario=" + nome + 
													"&&dest=" + dest + 
													"&&assunto=" + assunto + 
													"&&sub=" + sub + 
													"&&titulo=" + titulo + 
													"&&mensagem=" + mensagem,true);
		xmlhttp.send();
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function consulta( usuario, acao,offset) {
		var xmlhttp = objxml()
		xmlhttp.onreadystatechange=function() {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				document.getElementById("conteudo").innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","node/php/comunicacao.php?cs=" + Math.round((new Date()).getTime() / 1000) +				
													"&&acao=" + acao + 
													"&&usuario=" + usuario +
													"&&offset=" +  offset,true);
		xmlhttp.send();
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	function ler(usuario,acao,id,tipo) {
		var xmlhttp = objxml()
		xmlhttp.onreadystatechange=function() {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				document.getElementById("conteudo").innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET","node/php/comunicacao.php?cs=" + Math.round((new Date()).getTime() / 1000) +	
													"&&acao=" + acao + 
													"&&id=" + id +
													"&&usuario=" + usuario +
													"&&tipo=" + tipo
													,true);
		xmlhttp.send();
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function resp() {
		var xmlhttp = objxml()
		xmlhttp.onreadystatechange=function() {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				document.getElementById("conteudo").innerHTML = xmlhttp.responseText;
			}
		}
		
		rt = document.getElementById("remetente").innerHTML;
		dt = document.getElementById("destinatario").innerHTML;
		id_ = document.getElementById("id").innerHTML;
		ms = document.getElementById("msg").value;

		xmlhttp.open("GET","node/php/comunicacao.php?cs=" + Math.round((new Date()).getTime() / 1000) +				
													"&&cs=" + Math.round((new Date()).getTime() / 1000) +				
													"&&acao=resposta&&id=" + id_ +
													"&&remetente=" + rt +
													"&&mensagem=" + ms +
													"&&destinatario=" + dt
													,true);
		xmlhttp.send();
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//FUNCAO NAO UTILIZADA
	// function excluir(id,tipo) {
		// decisao = confirm("Deseja remover todos os dados dessa conversa, incluindo as mensagem enviadas e recebidas?");
		// if(decisao){
			// var xmlhttp = objxml()
			// xmlhttp.onreadystatechange=function() {
			  // if (xmlhttp.readyState==4 && xmlhttp.status==200) {
					// document.getElementById("conteudo").innerHTML = xmlhttp.responseText;
				// }
			// }

			// xmlhttp.open("GET","node/php/comunicacao.php?cs=" + Math.round((new Date()).getTime() / 1000) +				
														// "&&cs=" + Math.round((new Date()).getTime() / 1000) +				
														// "&&id=" + id +
														// "&&tipo=" + tipo +
														// "&&acao=excluir"
														// ,true);
			// xmlhttp.send();
		// }
		// else{
		// }
	// }
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////